   Raspberry Pi LPC1114 I/O Processor Expansion Board MUNTS-0004

   This is an LPC1114 ARM Cortex-M0 microcontroller based I/O processor
   and expansion board for the [1]Raspberry Pi, with 8 GPIO pins from the
   microcontroller brought out to the terminal block. Each of the GPIO
   pins can be configured for a variety of functions, including A/D input,
   PWM output, GPIO output, etc. The microcontroller UART, SPI, and I2C
   ports are connected to the Raspberry Pi expansion bus. See the [2]User
   Guide for more information. Includes 3" (75 mm) ribbon cable to connect
   to the Raspberry Pi P1 expansion header.

   See the articles in [3]The MagPi Issues 14, 17, 22, and 30.

   [4]Quick setup script

   [5]Cabling options for 40-pin Raspberry Pi Models

News

     * 25 January 2018 -- Added C++ classes and test programs for Unix,
       Arduino, and Mbed OS 5.
     * 16 May 2018 -- Switched from the old deprecated GPIO sysfs API to
       the new GPIO descriptor API. Moved Raspberry Pi device and GPIO pin
       assignments from expansion_lpc1114.h to configuration file
       /usr/local/etc/expansion_lpc1114.config.
     * 6 June 2018 -- Added support for [6]Microsoft Small Basic client
       programs.
     * 30 August 2018 -- Lots of C# housecleaning. .Net Core test program
       projects have been added and .Net Framework test program projects
       have been removed. Removed the XML-RPC .Net Framework library.
     * 23 January 2019 -- Added a [7]Remote I/O Protocol server. Added Ada
       transport packages necessary for clients to use the Remote I/O
       Protocol via Raw HID over [8]libhidapi, [9]libsimpleio, and
       [10]libusb 1.0.
     * 17 May 2019 -- Some minor housecleaning: Renamed a lot of
       identifiers from SPIAGENT_ to LPC1114_. This is only a source code
       change, so no firmware or libraries were rebuilt.
     * 18 May 2019 -- Modified the LEGO Power Functions Remote Control
       support in the SPI Agent Firmware to improve timing conformance to
       the protocol specification. The SPI Agent Firmware version number
       is now 17284. You should upgrade your unit's firmware using
       /usr/local/libexec/expansion_lpc1114_flash after upgrading to the
       latest Debian package for Raspbian.
     * 1 February 2020 -- The original MUNTS-0004 production run has now
       been used up. Stay tuned, though, because some software compatible
       follow on boards are in the design stage.
   _______________________________________________________________________

   Questions or comments to Philip Munts [11]phil@munts.net

References

   1. http://www.raspberrypi.org/
   2. http://git.munts.com/rpi-mcu/expansion/LPC1114/doc/UserGuide.pdf
   3. http://www.themagpi.com/
   4. http://git.munts.com/rpi-mcu/expansion/LPC1114/src/scripts/expansion_lpc1114_setup
   5. http://git.munts.com/rpi-mcu/expansion/LPC1114/FortyPins.html
   6. https://blogs.msdn.microsoft.com/smallbasic
   7. http://git.munts.com/libsimpleio/doc/RemoteIOProtocol.pdf
   8. https://github.com/signal11/hidapi
   9. http://git.munts.com/libsimpleio
  10. https://libusb.info/
  11. mailto:phil@munts.net
