// LPC11xx microcontroller pin definitions

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

namespace lpc11xx
{
    /// <summary>
    /// These are the LPC1114 I/O pins.
    /// Not all of them are bonded out on any particular device.
    /// </summary>
    public enum Pins
    {
        PIO0_0,
        PIO0_1,
        PIO0_2,
        PIO0_3,
        PIO0_4,
        PIO0_5,
        PIO0_6,
        PIO0_7,
        PIO0_8,
        PIO0_9,
        PIO0_10,
        PIO0_11,
        PIO1_0,
        PIO1_1,
        PIO1_2,
        PIO1_3,
        PIO1_4,
        PIO1_5,
        PIO1_6,
        PIO1_7,
        PIO1_8,
        PIO1_9,
        PIO1_10,
        PIO1_11,
        PIO2_0,
        PIO2_1,
        PIO2_2,
        PIO2_3,
        PIO2_4,
        PIO2_5,
        PIO2_6,
        PIO2_7,
        PIO2_8,
        PIO2_9,
        PIO2_10,
        PIO2_11,
        PIO3_0,
        PIO3_1,
        PIO3_2,
        PIO3_3,
        PIO3_4,
        PIO3_5,
    }
}
