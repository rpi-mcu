// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// transport services using ONC-RPC

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Net;
using org.acplt.oncrpc;

namespace SPIAgent
{
    /// <summary>
    /// The Transport class provides transport services to a specified Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware ONC-RPC server.
    /// </summary>
    public class Transport : ITransport
    {
        private spi_agent_oncrpcClient handle = null;

        /// <summary>
        /// Transport object constructor.  You must also call Open() to establish the connection to the server.
        /// </summary>
        public Transport()
        {
        }

        /// <summary>
        /// Transport object constructor.  This constructor opens a connection to the specified server.
        /// </summary>
        /// <param name="servername">Server domain name or IP address.</param>
        public Transport(String servername)
        {
            Open(servername);
        }

        /// <summary>
        /// Open connection to the specified server.
        /// </summary>
        /// <param name="servername">Server domain name or IP address.</param>
        public void Open(String servername)
        {
            IPAddress serveraddr;
            SPIAGENT_COMMAND_MSG_t cmd;
            SPIAGENT_RESPONSE_MSG_t resp;

            // Check for existing connection

            if (handle != null)
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport client handle is already open");
            }

            // Check for invalid parameter

            if (Uri.CheckHostName(servername) == UriHostNameType.Unknown)
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport server name is invalid");
            }

            // Resolve the server IP address

            try
            {
                serveraddr = Dns.GetHostAddresses(servername)[0];
            }
            catch
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport server name cannot be resolved");
            }


            try
            {
                // Create RPC client object

                handle = new spi_agent_oncrpcClient(serveraddr, OncRpcProtocols.ONCRPC_UDP);

                // Reduce timeout to 2 seconds

                OncRpcUdpClient c = (OncRpcUdpClient) handle.GetClient();

                c.setTimeout(2000);
                c.setRetransmissionTimeout(500);

                // Ping the RPC server

                cmd = new SPIAGENT_COMMAND_MSG_t();
                resp = new SPIAGENT_RESPONSE_MSG_t();

                cmd.command = 0;
                cmd.pin = 0;
                cmd.data = 0;

                Command(cmd, ref resp);

                if ((errno)resp.error != errno.EOK)
                {
                    throw new SPIAgent_Exception("SPI Agent Firmware returned error " + ((errno)resp.error).ToString());
                }
            }
            catch
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport transaction failed");
            }
        }

        /// <summary>
        /// Close the current server connection.
        /// </summary>
        public void Close()
        {
            // Check for existing connection

            if (handle == null)
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport client handle is not open");
            }

            // Discard the existing connection

            handle = null;
        }

        /// <summary>
        /// Issue a command to the current server.
        /// </summary>
        /// <param name="cmd">Command message structure.</param>
        /// <param name="resp">Response message structure.</param>
        public void Command(SPIAGENT_COMMAND_MSG_t cmd, ref SPIAGENT_RESPONSE_MSG_t resp)
        {
            // Check for existing connection

            if (handle == null)
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport client handle is not open");
            }

            // Dispatch the command

            try
            {
                resp = handle.spi_transaction_1(cmd);
            }
            catch
            {
                throw new SPIAgent_Transport_Exception("SPI Agent Firmware transport transaction failed");
            }
        }
    }
}
