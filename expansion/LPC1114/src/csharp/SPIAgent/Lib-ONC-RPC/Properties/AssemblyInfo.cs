using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Raspberry Pi LPC1114 I/O Processor SPI Agent Firmware ONC-RPC client library")]
[assembly: AssemblyDescription("Library for .Net Framework 4.5")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Munts Technologies")]
[assembly: AssemblyProduct("SPIAgent-ONC-RPC")]
[assembly: AssemblyCopyright("Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp dba Munts Technologies")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3772d8db-d515-423b-916f-c61f15a45530")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.*")]
