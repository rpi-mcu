// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// Version Test

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using SPIAgent;

namespace test_version
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            if (File.Exists("C:\\ProgramData\\MuntsTechnologies\\tablet.txt"))
            {
                // Resize for Windows 8 tablet

                this.WindowState = System.Windows.WindowState.Maximized;
                app_info.FontSize = 48;
                app_info.Margin = new Thickness(50);
                serverprompt.FontSize = 36;
                servername.FontSize = 36;
                servername.Margin = new Thickness(0, 0, 50, 0);
                app_output.FontSize = 30;
                app_output.Margin = new Thickness(50);
            }

            // Process server name on command line

            String[] args = Environment.GetCommandLineArgs();

            if (args.Length == 2)
            {
                servername.Text = args[1];
                servername.IsEnabled = false;
                ThreadPool.QueueUserWorkItem(o => ExecuteVersionQuery());
            }
        }

        private void servername_KeyDown(object sender, KeyEventArgs e)
        {
            if (servername.IsEnabled && (e.Key == Key.Return))
            {
                servername.IsEnabled = false;
                ThreadPool.QueueUserWorkItem(o => ExecuteVersionQuery());
            }
        }

        private void servername_TextChanged(object sender, TextChangedEventArgs e)
        {
            app_output.Text = "";
        }

        // Synchronized access to servername.Text

        private String SERVERNAME
        {
            get
            {
                String s = null;
                this.Dispatcher.Invoke(delegate { s = servername.Text; });
                return s;
            }
        }

        // Synchronized access to servername.IsEditable

        private bool SERVERNAMELOCK
        {
            get
            {
                bool l = false;
                this.Dispatcher.Invoke(delegate { l = !servername.IsEnabled; });
                return l;
            }

            set
            {
                this.Dispatcher.Invoke(delegate { servername.IsEnabled = !value; servername.Focus(); });
            }
        }

        // Synchronized access to app_output.Text

        private String OUTPUTBOX
        {
            get
            {
                String s = null;
                this.Dispatcher.Invoke(delegate { s = app_output.Text; });
                return s;
            }

            set
            {
                this.Dispatcher.Invoke(delegate { app_output.Text = value; });
            }
        }

        // Worker thread

        private void ExecuteVersionQuery()
        {
            try
            {
                // Open connection to the server

                OUTPUTBOX = "Connecting to " + SERVERNAME + "...";

                SPIAgent.Transport spiagent = new SPIAgent.Transport(SERVERNAME);

                // Query firmware version and microcontroller device ID

                SPIAGENT_COMMAND_MSG_t cmd = new SPIAGENT_COMMAND_MSG_t();
                SPIAGENT_RESPONSE_MSG_t resp = new SPIAGENT_RESPONSE_MSG_t();

                cmd.command = (int)Commands.SPIAGENT_CMD_NOP;
                cmd.pin = 0;
                cmd.data = 0;

                spiagent.Command(cmd, ref resp);

                OUTPUTBOX = "LPC1114 firmware version is " + resp.data.ToString();

                cmd.command = (int)Commands.SPIAGENT_CMD_GET_SFR;
                cmd.pin = (int)SFR.LPC1114_DEVICE_ID;
                cmd.data = 0;

                spiagent.Command(cmd, ref resp);

                OUTPUTBOX += "\nLPC1114 device ID is " + resp.data.ToString("X");
            }
            catch(Exception ex)
            {
                OUTPUTBOX = ex.Message;
            }
            finally
            {
                SERVERNAMELOCK = false;
            }
        }
    }
}
