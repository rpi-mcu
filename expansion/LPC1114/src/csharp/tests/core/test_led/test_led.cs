// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// LED test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Threading;

using SPIAgent;

namespace test_led
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nLED Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_led <hostname>");
                Environment.Exit(1);
            }

            ITransport spiagent = new Transport(args[0]);

            GPIO LED = new GPIO(spiagent, Pins.LPC1114_LED, GPIO.DIRECTION.OUTPUT, true);

            Console.WriteLine("Press any key to exit program\n");

            while(!Console.KeyAvailable)
            {
                if (LED.state)
                    Console.WriteLine("Turning LED OFF...");
                else
                    Console.WriteLine("Turning LED ON...");

                LED.state = !LED.state;
                Thread.Sleep(1000);
            }

            Environment.Exit(0);
        }
    }
}
