// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// frequency counter test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Threading;

using SPIAgent;

namespace test_frequency_counter
{
    class Program
    {
        static void Main(string[] args)
        {
            UInt32 count;
            double frequency;

            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nFrequency Counter Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_frequency_counter <hostname>");
                Environment.Exit(1);
            }

            ITransport spiagent = new Transport(args[0]);

            // Configure LPC1114 timer CT32B0 to measure time between rising edges on CAP0 input

            SPIAgent.Timer Timer0 = new SPIAgent.Timer(spiagent, SPIAgent.Timer.ID.CT32B0);

            Timer0.ConfigureCapture(SPIAgent.Timer.CAPTURE_EDGE.CAP0_RISING);

            Timer0.ConfigureMode(SPIAgent.Timer.MODE.PCLK);

            // Display frequency once a second

            Console.WriteLine("Press any key to exit program\n");

            while (!Console.KeyAvailable)
            {
                count = Timer0.capture_delta;

                if (count < 1920)
                    frequency = 0.0;
                else
                    frequency = (double)SPIAgent.Timer.PCLK_FREQUENCY / count;

                Console.Write("\rFrequency: " + frequency.ToString("F") + " Hz        ");

                Thread.Sleep(1000);
            }

            Environment.Exit(0);
        }
    }
}
