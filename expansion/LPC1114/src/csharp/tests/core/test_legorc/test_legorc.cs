// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// LEGO(R) Power Functions Remote Control output test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.Threading;

using SPIAgent;

namespace test_legorc
{
    class Program
    {
        static void Main(string[] args)
        {
            int channel;
            LEGORC.MOTOR motor;
            LEGORC.DIRECTION direction;
            int speed;

            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nLEGO(R) Power Functions Remote Control Test\n");

            if (args.Length != 5)
            {
                Console.WriteLine("Usage: test_legorc <hostname> <channel:1-4> <motor:0-2> <direction:0-1> <speed:0-7>");
                Environment.Exit(1);
            }

            // Connect to the SPIAgent Firmware RPC server

            ITransport spiagent = new Transport(args[0]);

            // Configure the LEGO(R) RC infrared emitter output

            LEGORC IRED = new LEGORC(spiagent, Pins.LPC1114_GPIO7);

            // Process command line arguments

            channel = int.Parse(args[1]);
            motor = (LEGORC.MOTOR)Enum.Parse(typeof(LEGORC.MOTOR), args[2]);
            direction = (LEGORC.DIRECTION)Enum.Parse(typeof(LEGORC.DIRECTION), args[3]);
            speed = int.Parse(args[4]);

            // Issue the command

            IRED.Command(channel, motor, direction, speed);

            Environment.Exit(0);
        }
    }
}
