// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// Pulse Width Modulated output test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;

using SPIAgent;

namespace test_pwm
{
    class Program
    {
        static void Main(string[] args)
        {
            int frequency;
            int channel;
            float dutycycle;

            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nPulse Width Modulated Output Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_pwm <hostname>");
                Environment.Exit(1);
            }

            // Connect to the SPIAgent Firmware RPC server

            ITransport spiagent = new Transport(args[0]);

            // Get the PWM pulse frequency (common to all PWM channels)

            Console.Write("Enter PWM frequency:              ");
            frequency = int.Parse(Console.ReadLine());

            // Configure the PWM outputs, and save them in a Dictionary object

            Dictionary<int, PWM> PWMchannels = new Dictionary<int, PWM>();

            PWMchannels.Add(1, new PWM(spiagent, Pins.LPC1114_PWM1, frequency));
            PWMchannels.Add(2, new PWM(spiagent, Pins.LPC1114_PWM2, frequency));
            PWMchannels.Add(3, new PWM(spiagent, Pins.LPC1114_PWM3, frequency));
            PWMchannels.Add(4, new PWM(spiagent, Pins.LPC1114_PWM4, frequency));

            for (;;)
            {
                Console.Write("Enter PWM channel number (1-4):   ");

                try
                {
                    channel = int.Parse(Console.ReadLine());
                    if (channel == 0) break;
                    if ((channel < 1) || (channel > 4)) continue;
                }
                catch
                {
                    continue;
                }

                Console.Write("Enter PWM duty cycle % (0-100.0): ");

                try
                {
                    dutycycle = float.Parse(Console.ReadLine());
                    if (dutycycle < PWM.MIN_DUTYCYCLE) continue;
                    if (dutycycle > PWM.MAX_DUTYCYCLE) continue;
                }
                catch
                {
                    continue;
                }

                PWM p;
                PWMchannels.TryGetValue(channel, out p);
                p.dutycycle = dutycycle;
            }

            Environment.Exit(0);
        }
    }
}
