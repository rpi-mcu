// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// analog input test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Threading;

using SPIAgent;

namespace test_analog
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;

            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.Write("Raspberry Pi LPC1114 I/O Processor Expansion Board Analog Input Test");

            if (args.Length != 1)
            {
                Console.SetCursorPosition(0, 2);
                Console.WriteLine("Usage: test_analog <hostname>");
                Environment.Exit(1);
            }

            Console.SetCursorPosition(0, 8);
            Console.WriteLine("Press any key to exit program\n");

            ITransport spiagent = new Transport(args[0]);

            // Configure analog inputs

            ADC[] ADCs = new ADC[5];

            for (i = 0; i < 5; i++)
            {
                ADCs[i] = new ADC(spiagent, Pins.LPC1114_AD1 + i);
            }

            // Main program loop--Display analog inputs

            while (!Console.KeyAvailable)
            {
                for (i = 0; i < 5; i++ )
                {
                    Console.SetCursorPosition(0, i + 2);
                    Console.Write("Analog Input " + (i+1).ToString() + " is " + ADCs[i].voltage.ToString("F2") + "V");
                }

                Thread.Sleep(1000);
            }

            Console.SetCursorPosition(0, 10);
            Environment.Exit(0);
        }
    }
}
