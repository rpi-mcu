// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// version test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using SPIAgent;

namespace test_sfr
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nSpecial Function Register Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_sfr <hostname>");
                Environment.Exit(1);
            }

            // Initialize the SPI Agent transport library

            ITransport spiagent = new Transport(args[0]);

            // Instantiate some SFR objects

            SFR DEVICE_ID = new SFR(spiagent, SFR.LPC1114_DEVICE_ID);
            SFR U0SCR = new SFR(spiagent, SFR.LPC1114_U0SCR);

            // Read the LPC1114 device ID register

            Console.WriteLine("The LPC1114 device ID is                 " + DEVICE_ID.data.ToString("X"));

            // Write to the LPC1114 UART scratch pad register

            U0SCR.data = 0x22;

            // Read from the LPC1114 UART scratch pad register

            Console.WriteLine("The LPC1114 UART scratch pad register is " + U0SCR.data.ToString("X"));

            Environment.Exit(0);
        }
    }
}
