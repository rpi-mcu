// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// sonar ranging test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// LPC1114 GPIO0 -- Echo pulse input
// LPC1114 GPIO1 -- Trigger pulse output

// NOTE: This program is written for and tested with 4-pin ultrasonic
// modules like the HC-SR04.  To use a 3-pin module like the Parallax
// Ping, connect GPIO1 to GPIO0 through a Schottky signal diode like the
// BAT43, anode to GPIO1 and cathode to GPIO0.

using System;
using System.Threading;

using SPIAgent;

namespace test_sonar
{
    class Program
    {
        static void Main(string[] args)
        {
            UInt32 count;
            int distance;

            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nSonar Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_sonar <hostname>");
                Environment.Exit(1);
            }

            ITransport spiagent = new Transport(args[0]);

            // Configure LPC1114 timer CT32B1 to measure time between rising edges on CAP0 input

            SPIAgent.Timer Timer1 = new SPIAgent.Timer(spiagent, SPIAgent.Timer.ID.CT32B1);

            Timer1.ConfigureCapture(SPIAgent.Timer.CAPTURE_EDGE.CAP0_BOTH);

            Timer1.ConfigureMode(SPIAgent.Timer.MODE.PCLK);

            // Configure the trigger pulse output

            GPIO Trigger = new GPIO(spiagent, Pins.LPC1114_GPIO1, GPIO.DIRECTION.OUTPUT, false);

            // Send ranging pulse once a second

            Console.WriteLine("Press any key to exit program\n");

            while (!Console.KeyAvailable)
            {
                Trigger.state = true;
                Trigger.state = false;

                Thread.Sleep(100);

                count = Timer1.capture_delta;
                distance = (int)(count / 282.35);

                if (distance < 2000)
                    Console.Write("Detection! Range is " + distance.ToString() + " mm\r");
                else
                    Console.Write("Probing...                  \r");

                Thread.Sleep(900);
            }

            Environment.Exit(0);
        }
    }
}
