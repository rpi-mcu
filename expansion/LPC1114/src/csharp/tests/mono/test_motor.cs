// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// H-bridge DC motor driver output test program

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;

using SPIAgent;

namespace test_motor
{
    class Program
    {
        static void Main(string[] args)
        {
            int frequency;
            bool ExitFlag = false;

            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nDC Motor Test\n");

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: test_motor <hostname>");
                Environment.Exit(1);
            }

            // Connect to the SPIAgent Firmware RPC server

            ITransport spiagent = new Transport(args[0]);

            // Get the PWM pulse frequency (common to all PWM channels)

            Console.Write("Enter PWM frequency:              ");
            frequency = int.Parse(Console.ReadLine());

            // Configure the motor driver outputs

            Motor M1 = new Motor(spiagent, Pins.LPC1114_PWM1, Pins.LPC1114_GPIO0, frequency);

            // Exit gracefully on CONTROL-C

            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                ExitFlag = false;

            };

            while (!ExitFlag)
            {
                try
                {
                    Console.Write("Enter motor speed (-1.0 to +1.0): ");
                    M1.speed = float.Parse(Console.ReadLine());
                }
                catch
                {
                    continue;
                }
            }

            Environment.Exit(0);
        }
    }
}
