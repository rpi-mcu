// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// square wave output test

// Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Threading;

using SPIAgent;

namespace test_square_wave
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nRaspberry Pi LPC1114 I/O Processor Expansion Board\nSquare Wave Output Test\n");

            // Process command line arguments

            if (args.Length != 2)
            {
                Console.WriteLine("Usage: test_square_wave <hostname> <frequency>");
                Environment.Exit(1);
            }

            ITransport spiagent = new Transport(args[0]);

            UInt32 frequency = UInt32.Parse(args[1]);
            UInt32 count = SPIAgent.Timer.PCLK_FREQUENCY / frequency / 2;

            // Configure LPC1114 timer CT32B1 to generate a square wave on the MAT0 output

            SPIAgent.Timer Timer1 = new SPIAgent.Timer(spiagent, SPIAgent.Timer.ID.CT32B1);

            Timer1.ConfigureMatch(SPIAgent.Timer.MATCH_REGISTER.MATCH0, count,
                SPIAgent.Timer.MATCH_OUTPUT.TOGGLE, SPIAgent.Timer.FEATURES.RESET);

            Timer1.ConfigureMode(SPIAgent.Timer.MODE.PCLK);

            Environment.Exit(0);
        }
    }
}
