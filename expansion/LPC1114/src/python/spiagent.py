#!/usr/bin/python3

"""
Python3 library for the Raspberry Pi LPC1114 I/O Processor Expansion Board

See http://git.munts.com/rpi-mcu/expansion/LPC1114 for more information.
"""

# Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

__author__	= 'Philip Munts <phil@munts.net>'

import math

# LPC1114 GPIO pins

LPC1114_INT				= 3
LPC1114_READY				= 11
LPC1114_LED				= 7
LPC1114_GPIO0				= 12
LPC1114_GPIO1				= 13
LPC1114_GPIO2				= 14
LPC1114_GPIO3				= 15
LPC1114_GPIO4				= 16
LPC1114_GPIO5				= 17
LPC1114_GPIO6				= 20
LPC1114_GPIO7				= 21

# LPC1114 GPIO special function pins

LPC1114_AD1				= LPC1114_GPIO0
LPC1114_AD2				= LPC1114_GPIO1
LPC1114_AD3				= LPC1114_GPIO2
LPC1114_AD4				= LPC1114_GPIO3
LPC1114_AD5				= LPC1114_GPIO4

LPC1114_PWM1				= LPC1114_GPIO1
LPC1114_PWM2				= LPC1114_GPIO2
LPC1114_PWM3				= LPC1114_GPIO3
LPC1114_PWM4				= LPC1114_GPIO7

LPC1114_CT32B1_CAP0			= LPC1114_GPIO0
LPC1114_CT32B1_MAT0			= LPC1114_GPIO1
LPC1114_CT32B1_MAT1			= LPC1114_GPIO2
LPC1114_CT32B1_MAT2			= LPC1114_GPIO3
LPC1114_CT32B1_MAT3			= LPC1114_GPIO4
LPC1114_CT32B0_CAP0			= LPC1114_GPIO5

# SPI Agent commands (from include/spi-agent.h)

CMD_NOP					= 0
CMD_LOOPBACK				= 1
CMD_CONFIGURE_ANALOG_INPUT		= 2
CMD_CONFIGURE_GPIO_INPUT		= 3
CMD_CONFIGURE_GPIO_OUTPUT		= 4
CMD_CONFIGURE_PWM_OUTPUT		= 5
CMD_GET_ANALOG				= 6
CMD_GET_GPIO				= 7
CMD_PUT_GPIO				= 8
CMD_PUT_PWM				= 9
CMD_CONFIGURE_GPIO_INTERRUPT		= 10
CMD_CONFIGURE_GPIO			= 11
CMD_PUT_LEGORC				= 12
CMD_GET_SFR				= 13
CMD_PUT_SFR				= 14
CMD_CONFIGURE_TIMER_MODE		= 15
CMD_CONFIGURE_TIMER_PRESCALER		= 16
CMD_CONFIGURE_TIMER_CAPTURE		= 17
CMD_CONFIGURE_TIMER_MATCH0		= 18
CMD_CONFIGURE_TIMER_MATCH1		= 19
CMD_CONFIGURE_TIMER_MATCH2		= 20
CMD_CONFIGURE_TIMER_MATCH3		= 21
CMD_CONFIGURE_TIMER_MATCH0_VALUE	= 22
CMD_CONFIGURE_TIMER_MATCH1_VALUE	= 23
CMD_CONFIGURE_TIMER_MATCH2_VALUE	= 24
CMD_CONFIGURE_TIMER_MATCH3_VALUE	= 25
CMD_GET_TIMER_VALUE			= 26
CMD_GET_TIMER_CAPTURE			= 27
CMD_GET_TIMER_CAPTURE_DELTA		= 28
CMD_INIT_TIMER				= 29

# Analog input constants

ANALOG_PINS				= (LPC1114_AD1, LPC1114_AD2, LPC1114_AD3, LPC1114_AD4, LPC1114_AD5)
ANALOG_MIN_CHANNEL			= 1
ANALOG_MAX_CHANNEL			= 5
ANALOG_SPAN				= 3.3
ANALOG_BITS				= 10
ANALOG_STEPS				= 1024
ANALOG_STEPSIZE				= 0.00322265625

# Valid GPIO pins

GPIO_PINS				= (LPC1114_GPIO0, LPC1114_GPIO1, LPC1114_GPIO2, LPC1114_GPIO3, LPC1114_GPIO4, LPC1114_GPIO5, LPC1114_GPIO6, LPC1114_GPIO7)

# GPIO resistor settings (GPIO_RESISTOR_t)

GPIO_PULLDOWN				= 0
GPIO_PULLUP				= 1

# GPIO pin mode settings (GPIO_MODE_T)

GPIO_MODE_INPUT				= 0
GPIO_MODE_INPUT_PULLDOWN		= 1
GPIO_MODE_INPUT_PULLUP			= 2
GPIO_MODE_OUTPUT			= 3
GPIO_MODE_OUTPUT_OPENDRAIN		= 4

# GPIO input interrupt configurations (GPIO_INTERRUPT_CONFIG_t)

GPIO_INTERRUPT_DISABLED			= 0
GPIO_INTERRUPT_FALLING			= 1
GPIO_INTERRUPT_RISING			= 2
GPIO_INTERRUPT_BOTH			= 3

# LEGO(R) Power Functions Remote Control limits

LEGORC_CHANNELS				= 4
LEGORC_MAXSPEED				= 255

# LEGO(R) Power Functions Remote Control motor identifiers

LEGORC_ALLSTOP				= 0
LEGORC_MOTORA				= 1
LEGORC_MOTORB				= 2
LEGORC_COMBODIRECT			= 3
LEGORC_COMBOPWM				= 4

# LEGO(R) Power Functions Remote Control direction identifiers

LEGORC_REVERSE				= 0
LEGORC_FORWARD				= 1

# Pulse Width Modulation limits

PWM_PINS				= (LPC1114_PWM1, LPC1114_PWM2, LPC1114_PWM3, LPC1114_PWM4)
PWM_MIN_CHANNEL				= 1
PWM_MAX_CHANNEL				= 4
PWM_MIN_FREQUENCY			= 50
PWM_MAX_FREQUENCY			= 50000
PWM_MIN_DUTYCYCLE			= 0.0
PWM_MAX_DUTYCYCLE			= 100.0
SERVO_MIN_POSITION			= -1.0
SERVO_MAX_POSITION			= 1.0

# LPC1114 timers (spiagent_timer_id_t)

CT32B0					= 0
CT32B1					= 1

# LPC1114 timer modes (spiagent_timer_mode_t)

TIMER_MODE_DISABLED			= 0
TIMER_MODE_RESET			= 1
TIMER_MODE_PCLK				= 2
TIMER_MODE_CAP0_RISING			= 3
TIMER_MODE_CAP0_FALLING			= 4
TIMER_MODE_CAP0_BOTH			= 5

# LPC1114 timer capture input edge settings (spiagent_timer_capture_edget_t)

TIMER_CAPTURE_EDGE_DISABLED		= 0
TIMER_CAPTURE_EDGE_CAP0_RISING		= 1
TIMER_CAPTURE_EDGE_CAP0_FALLING		= 2
TIMER_CAPTURE_EDGE_CAP0_BOTH		= 3

# LPC1114 timer capture flags

TIMER_CAPTURE_INTERRUPT			= 0x10

# LPC1114 timer match output actions (spiagent_timer_match_output_action_t)

TIMER_MATCH_OUTPUT_DISABLED		= 0
TIMER_MATCH_OUTPUT_CLEAR		= 1
TIMER_MATCH_OUTPUT_SET			= 2
TIMER_MATCH_OUTPUT_TOGGLE		= 3

# LPC1114 timer match flags

TIMER_MATCH_INTERRUPT			= 0x10
TIMER_MATCH_RESET			= 0x20
IMTER_MATCH_STOP			= 0x40

# Define some LPC1114 special function registers

LPC1114_DEVICE_ID			= 0x400483F4
LPC1114_GPIO1DATA			= 0x50010CFC
LPC1114_U0SCR				= 0x4000801C

# Define some other constants

PCLK_FREQUENCY				= 48000000

###############################################################################

# LPC1114 SPI Agent firmware transport library wrapper class

class Transport:
  """
  The Transport class provides services for dispatching commands to a
  Raspberry Pi running the LPC1114 I/O Processor Expansion Board SPI Agent
  Firmware server.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Send a NOP command to the LPC1114 SPI Agent Firmware
  resp=[]
  t.command((spiagent.CMD_NOP, 0,  0), resp)
  print(resp)

  t.close()
  """

  __handler__ = None

  def __init__(self, server = None, lib = None):
    """
    Constructor function.

    Parameter server (string or None) indicates the domain name or IP address
    of the server.  If server is provided, call the open() method to
    connect to the specified server.

    Parameter lib (string: "auto", "libspiagent", "xml-rpc" or None) indicates
    which underlying transport library to use.  If lib is provided, use the
    specified transport library for communicating with the server.
    """

    self.error = 0

# Select transport library automatically

    if lib == None or lib == 'auto':
      try:
# Try libspiagent first...
        self.__handler__ = __import__('spiagent-libspiagent')
      except:
# ...Then fall back to XML-RPC
        self.__handler__ = __import__('spiagent-xmlrpc')

# Always use libspiagent

    elif lib == 'libspiagent':
      self.__handler__ = __import__('spiagent-libspiagent')

# Always use XML-RPC

    elif lib == 'xml-rpc':
      self.__handler__ = __import__('spiagent-xmlrpc')

# Unknown transport, raise exception and give up.  Could add HTTP, SOAP, etc.
# transport libraries here sometime in the future.

    else:
      raise IOError('Invalid transport identifier')

# Open connection to the server, if specified

    if server != None:
      self.open(server)

# Open connection to the server

  def open(self, server):
    """
    Open a connection to the specified server.

    Parameter server (string) indicates the domain name or IP address of the
    server.
    """

    self.error = self.__handler__.open(server)
    if self.error != 0:
      raise IOError('Transport.open() failed, error=' + str(self.error))

# Issue command to the server

  def command(self, cmd, resp):
    """
    Dispatch a command to the SPI Agent Firmware running on the LPC1114
    microcontroller.

    Parameter cmd must be a list or tuple of three integers (command, pin, data).

    Parameter resp must be an empty list.  It will be filled with 4 integers
    from the SPI Agent Firmware (command, pin, data, error).
    """

    self.error = self.__handler__.command(cmd, resp)
    if self.error != 0:
      raise IOError('Transport.command() failed, error=' + str(self.error))
    if resp[3] != 0:
      raise IOError('SPI Agent firmware returned error=' + str(resp[3]))

# Close connection to the server

  def close(self):
    """
    Close the connection to the server.
    """

    self.error = self.__handler__.close()
    if self.error != 0:
      raise IOError('Transport.close() failed, error=' + str(self.error))

###############################################################################

# LPC1114 analog input class

class ADC:
  """
  The ADC class configures an LPC1114 GPIO pin for analog input.

  WARNING: LPC1114 Analog inputs are NOT 5V tolerant!
  Do NOT exceed 3.3V on any analog input.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Read the analog input voltage on LPC1114_ADC1
  ADC1 = spiagent.ADC(t, spiagent.LPC1114_ADC1)
  print(ADC1.voltage)
  """

# Analog input object constructor

  def __init__(self, transport, pin):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter pin must be spiagent.LPC1114_ADC1 through spiagent.LPC1114_ADC5.
    """

    resp = []
    transport.command((CMD_CONFIGURE_ANALOG_INPUT, pin, 0), resp)
    self.__mytransport = transport
    self.__mypin = pin

# Analog input voltage property (read only)

  voltage = property()

  @voltage.getter
  def voltage(self):
    """
    Analog input voltage (read only).
    Returns float 0.0 through 3.3 when read.
    """

    resp = []
    self.__mytransport.command((CMD_GET_ANALOG, self.__mypin, 0), resp)
    return ANALOG_STEPSIZE*resp[2]

###############################################################################

# LPC1114 GPIO pin class

class GPIO:
  """
  The GPIO class configures an LPC1114 GPIO pin for General Purpose
  Input/Output.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Turn on the LED
  LED = spiagent.GPIO(t, spiagent.LPC1114_LED, spiagent.GPIO_MODE_OUTPUT)
  LED.state = 1
  print(LED.state)
  """

# GPIO pin object constructor

  def __init__(self, transport, pin, mode):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter pin must be spiagent.LPC1114_GPIO0 through spiagent.LPC1114_GPIO7,
    spiagent.LPC1114_INT, or spiagent.LPC1114_LED.

    Parameter mode must be an integer from spiagent.GPIO_MODE_INPUT (0) through
    spiagent.GPIO_MODE_OUTPUT_OPENDRAIN (4).
    """

    resp = []
    transport.command((CMD_CONFIGURE_GPIO, pin, mode), resp)
    self.__mytransport = transport
    self.__mypin = pin

# GPIO pin state property (read/write)

  state = property()

  @state.getter
  def state(self):
    """
    GPIO pin state (read/write).
    Returns integer 0 or 1 when read.
    Allowed write values are 0, 1, False, or True.
    """

    resp = []
    self.__mytransport.command((CMD_GET_GPIO, self.__mypin, 0), resp)
    return resp[2]

  @state.setter
  def state(self, value):
    resp = []
    self.__mytransport.command((CMD_PUT_GPIO, self.__mypin, int(value)), resp)

###############################################################################

# LPC1114 PWM output class

class PWM:
  """
  The PWM class configures an LPC1114 GPIO pin for Pulse Width Modulated output.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Set the output duty cycle on LPC1114_PWM1 to 50%
  PWM1 = spiagent.PWM(spiagent.LPC1114_PWM1, 50)
  PWM1.dutycycle = 50.0
  """

# PWM object constructor

  def __init__(self, transport, pin, frequency=50):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter pin must be spiagent.LPC1114_PWM1 through spiagent.LPC1114_PWM4.

    Parameter frequency must be an integer from value 50 through 50000 (Hertz).
    Optional, default value is 50.

    NOTE: PWM outputs PWM1, PWM2, and PWM3 share the same frequency generator.
    The pulse frequency will be the same for all of these PWM outputs, the last
    frequency configured for any of them.
    """

    resp = []
    transport.command((CMD_CONFIGURE_PWM_OUTPUT, pin, frequency), resp)
    self.__mytransport = transport
    self.__mypin = pin
    self.__myfrequency = frequency

# PWM output duty cycle property (write only)

  dutycycle = property(doc='''PWM output duty cycle (write only).
Allowed write values are float 0.0 through 100.0 (percent duty cycle).''')

  @dutycycle.setter
  def dutycycle(self, duty):
    if (duty < PWM_MIN_DUTYCYCLE) or (duty > PWM_MAX_DUTYCYCLE):
      raise ValueError('Invalid PWM output duty cycle')

    resp = []
    self.__mytransport.command((CMD_PUT_PWM, self.__mypin, math.floor(655.35*duty + 0.5)), resp)

###############################################################################

# LPC1114 servo output class

class Servo(PWM):
  """
  The Servo class configures an LPC1114 GPIO pin as an output to drive a
  standard RC (Radio Control) servo.  The pulse width is limited to the
  range 1.0 to 2.0 milliseconds, with the neutral/null/zero position at 1.5
  milliseconds.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Set the servo connected to LPC1114_PWM2 to the fully extended position
  S1 = spiagent.Servo(spiagent.LPC1114_PWM2)
  S1.position = 1.0
  """

# Servo object constructor

  def __init__(self, transport, pin, frequency=50):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter pin must be spiagent.LPC1114_PWM1 through spiagent.LPC1114_PWM4.

    Parameter frequency must be an integer from value 50 through 50000 (Hertz).
    Optional, default value is 50.  Standard servos operate at 50 Hz.  Some
    newer digital servos can operate as high as 400 Hz.

    NOTE: PWM outputs PWM1, PWM2, and PWM3 share the same frequency generator.
    The pulse frequency will be the same for all of these PWM outputs, the last
    frequency configured for any of them.
    """

    if frequency > 400:
      raise ValueError('PWM frequency is too high for servos')

    PWM.__init__(self, transport, pin, frequency)

# Servo position property (write only)

  position = property(doc='''RC servo position (write only).
Allowed write values are float -1.0 through +1.0 (normalized deflection from
the neutral position).''')

  @position.setter
  def position(self, p):
    if (p < SERVO_MIN_POSITION) or (p > SERVO_MAX_POSITION):
      raise ValueError('Invalid servo position')

    self.dutycycle = (0.5E-1*p + 1.5E-1)*self._PWM__myfrequency

###############################################################################

# H-bridge DC motor driver output class

class Motor(PWM, GPIO):
  """
  The Motor class configures a pair of LPC1114 GPIO pins as outputs to drive
  an H-bridge DC motor driver chip or module.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  M1 = spiagent.Motor(t, spiagent.LPC1114_PWM1, spiagent.LPC1114_GPIO0, 1000)
  M1.speed = 0.5
  """

# H-bridge DC motor driver output object constructor

  def __init__(self, transport, PWMpin, DIRpin, frequency=50):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter PWMpin must be spiagent.LPC1114_PWM1 through spiagent.LPC1114_PWM4.

    Parameter DIRpin must be spiagent.LPC1114_GPIO0 through spiagent.LPC1114_GPIO7.

    Parameter frequency must be an integer from value 50 through 50000 (Hertz).
    Optional, default value is 50.  Small hobby motors seem to work best at 50 Hz.
    Larger industrial motors may work better at higher frequencies.

    NOTE: PWM outputs PWM1, PWM2, and PWM3 share the same frequency generator.
    The pulse frequency will be the same for all of these PWM outputs, the last
    frequency configured for any of them.
    """

# Validate parameters

    if PWMpin == DIRpin:
      raise ValueError('PWM and direction pins cannot be the same')

# Configure pins

    PWM.__init__(self, transport, PWMpin, frequency)
    GPIO.__init__(self, transport, DIRpin, GPIO_MODE_OUTPUT)

# H-bridge DC motor driver output speed property (write only)

  speed = property(doc='''Motor speed property (write only).
Allowed write values are float -1.0 through +1.0 (normalized speed).''')

  @speed.setter
  def speed(self, s):
    if (s >= -1.0) and (s < 0.0):
      self.state = 0
      self.dutycycle = -s*100.0
    elif (s >= 0.0) and (s <= 1.0):
      self.state = 1
      self.dutycycle = s*100.0
    else:
      raise ValueError('Invalid motor speed')

###############################################################################

# LPC1114 LEGO(R) Power Functions Remote Control output class

class LegoRC:
  """
  The LegoRC class configures an LPC1114 GPIO pin for LEGO(R) Power Functions
  Remote Control output.

  NOTE: You will need to drive the infrared emitter diode (IRED) with a high
  current driver such as the ULN2003A.  The LPC1114 GPIO pins are not capable
  of driving an IRED without current amplification.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Set Motor A speed to forward, 5
  IRED = spiagent.LegoRC(t, spiagent.GPIO7)
  IRED.SendCommand(1, spiagent.LEGORC_MOTORA, spiagent.LEGORC_FORWARD, 5)
  """

# LEGO(R) RC output object constructor

  def __init__(self, transport, pin):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter pin must be spiagent.LPC1114_GPIO0 through spiagent.LPC1114_GPIO7.
    """

    resp = []
    transport.command((CMD_CONFIGURE_GPIO_OUTPUT, pin, 0), resp)
    self.__mytransport = transport
    self.__mypin = pin

# Send LEGO(R) RC command

  def SendCommand(self, channel, motor, direction, speed):
    """
    Transmit a LEGO(R) Power Functions Remote Control command.

    Parameter channel must be an integer from 1 through 4.

    Parameter motor must be an integer from spiagent.LEGORC_ALLSTOP (0)
    through spiagent.LEGORC.COMBOPWM (4).

    Parameter direction must be an integer from spiagent.LEGORC_REVERSE (0)
    through spiagent.LEGORC_FORWARD (1).

    Parameter speed must be an integer from 0 through 255.

    NOTE: Some motor settings restrict the speed to smaller ranges.
    """

    if (channel <= 0) or (channel > LEGORC_CHANNELS):
      raise ValueError('Invalid channel number')

    if (motor < LEGORC_ALLSTOP) or (motor > LEGORC_COMBOPWM):
      raise ValueError('Invalid motor identifier')

    if (direction < LEGORC_REVERSE) or (direction > LEGORC_FORWARD):
      raise ValueError('Invalid direction')

    if (speed < 0) or (speed > LEGORC_MAXSPEED):
      raise ValueError('Invalid speed')

    resp = []
    self.__mytransport.command((CMD_PUT_LEGORC, self.__mypin, (channel << 24) | (motor << 16) | (direction << 8) | speed), resp)

###############################################################################

# LPC1114 32-bit timer class

class Timer:
  """
  The Timer class configures an LPC1114 32-bit timer/counter.

  EXAMPLE:

  import spiagent

  t = spiagent.Transport("bogus.com")

  # Output 1000 Hz square wave on pin LPC1114_CT32B1_MAT0
  T1 = spiagent.Timer(t, spiagent.CT32B1)
  T1.ConfigurePrescaler(1)
  T1.ConfigureMatch(0, spiagent.PCLK_FREQUENCY//1000//2, spiagent.TIMER_MATCH_OUTPUT_TOGGLE, spiagent.TIMER_MATCH_RESET)
  T1.ConfigureMode(spiagent.TIMER_MODE_PCLK)
  """

# Timer object constructor

  def __init__(self, transport, timer, mode = TIMER_MODE_DISABLED, prescaler = 1):
    """
    Constructor function.

    Parameter transport must be a Transport instance with an open connection.

    Parameter timer must be an integer from spiagent.CT32B0 (0) through
    spiagent.CT32B1 (1).

    Parameter mode must be an integer spiagent.TIMER_MODE_DISABLED (0)
    through spiagent.TIMER_MODE_CAP0_BOTH (5).

    Parameter prescaler must be a 32-bit integer.
    """

    resp = []
    transport.command((CMD_INIT_TIMER, timer, 0), resp)

    resp = []
    transport.command((CMD_CONFIGURE_TIMER_PRESCALER, timer, prescaler), resp)

    resp = []
    transport.command((CMD_CONFIGURE_TIMER_MODE, timer, mode), resp)

    self.__mytransport = transport
    self.__mytimerid = timer

# Configure timer mode

  def ConfigureMode(self, mode):
    """
    Configure the timer mode.

    Parameter mode must be an integer from spiagent.TIMER_MODE_DISABLED (0)i
    through spiagent.TIMER_MODE_CAP0_BOTH (5).
    """

    resp = []
    self.__mytransport.command((CMD_CONFIGURE_TIMER_MODE, self.__mytimerid, mode), resp)

# Configure timer prescaler

  def ConfigurePrescaler(self, prescaler):
    """
    Configure the timer prescaler.

    Parameter prescaler must be a 32-bit integer.
    """

    resp = []
    self.__mytransport.command((CMD_CONFIGURE_TIMER_PRESCALER, self.__mytimerid, prescaler), resp)

# Configure timer capture

  def ConfigureCapture(self, edge, features = 0):
    """
    Configure the CAP0 input capture edge.

    Parameter edge must be an integer from spiagent.TIMER_CAPTURE_EDGE_DISABLED (0)
    through spiagent.TIMER_CAPTURE_EDGE_CAP0_BOTH (3).

    Parameter features must be 0 or spiagent.TIMER_CAPTURE_INTERRUPT (16).
    """
    resp = []
    self.__mytransport.command((CMD_CONFIGURE_TIMER_CAPTURE, self.__mytimerid, edge|features), resp)

# Configure timer match

  def ConfigureMatch(self, match, value, action, features):
    """
    Configure a match register.

    Parameter match must be an integer from 0 through 3.

    Parameter value must be a 32-bit integer.

    Parameter action must be an integer from spigent.TIMER_MATCH_OUTPUT_DISABLED (0)
    through spiagent.TIMER_MATCH_OUTPUT_TOGGLE (3).

    Parameter features must 0 or one or more of spiagent.TIMER_MATCH_INTERRUPT (16)
    OR'ed with spiagent.TIMER_MATCH_RESET (32).
    """

    resp = []
    self.__mytransport.command((CMD_CONFIGURE_TIMER_MATCH0 + match, self.__mytimerid, action|features), resp)

    resp = []
    self.__mytransport.command((CMD_CONFIGURE_TIMER_MATCH0_VALUE + match, self.__mytimerid, value), resp)

# Read-only counter register property

  counter = property()

  @counter.getter
  def counter(self):
    """
    Counter register (read only).
    Returns a 32-bit integer value.
    """

    resp = []
    self.__mytransport.command((CMD_GET_TIMER_VALUE, self.__mytimerid, 0), resp)
    return resp[2]

# Read-only capture register property

  capture = property()

  @capture.getter
  def capture(self):
    """
    Capture register (read only).
    Returns a 32-bit integer value.
    """

    resp = []
    self.__mytransport.command((CMD_GET_TIMER_CAPTURE, self.__mytimerid, 0), resp)
    return resp[2]

# Read-only capture delta property

  capture_delta = property()

  @capture_delta.getter
  def capture_delta(self):
    """
    Capture delta (difference between last two capture values) (read only).
    Returns a 32-bit integer value.
    """

    resp = []
    self.__mytransport.command((CMD_GET_TIMER_CAPTURE_DELTA, self.__mytimerid, 0), resp)
    return resp[2]
