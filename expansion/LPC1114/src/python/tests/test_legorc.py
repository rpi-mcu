#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# LEGO Power Functions Remote Control test

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board LEGO Power Functions RC Test\n')

# Validate parameters

if len(sys.argv) == 5:
  server = 'localhost'
  transport = 'auto'
  channel = int(sys.argv[1])
  motor = int(sys.argv[2])
  direction = int(sys.argv[3])
  speed = int(sys.argv[4])
elif len(sys.argv) == 6:
  server = sys.argv[1]
  transport = 'auto'
  channel = int(sys.argv[2])
  motor = int(sys.argv[3])
  direction = int(sys.argv[4])
  speed = int(sys.argv[5])
elif len(sys.argv) == 7:
  server = sys.argv[1]
  transport = sys.argv[2]
  channel = int(sys.argv[3])
  motor = int(sys.argv[4])
  direction = int(sys.argv[5])
  speed = int(sys.argv[6])
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc] <channel:1-4> <motor:0-2> <forward:0-1> <speed:0-7>')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Configure LEGO(R) Power Functions Remote Control output pin

GPIO7 = spiagent.LegoRC(t, spiagent.LPC1114_GPIO7)

# Issue LEGO Power Functions RC command to SPI Agent Firmware

GPIO7.SendCommand(channel, motor, direction, speed)

# Graceful shutdown

t.close()
sys.exit(0)
