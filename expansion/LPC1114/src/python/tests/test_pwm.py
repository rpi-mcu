#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# PWM output test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board PWM Output Test\n')

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Get PWM frequency

while True:
  try:
    frequency = int(input('Enter PWM frequency:                        '))
  except:
    print('ERROR: ' + str(sys.exc_info()[1]))
    continue

  if frequency < spiagent.PWM_MIN_FREQUENCY or frequency > spiagent.PWM_MAX_FREQUENCY:
    print('ERROR: Invalid PWM frequency')
    continue
  else:
    break

# Initialize PWM outputs

PWMOutputs = {}

for channel in range(spiagent.PWM_MIN_CHANNEL, spiagent.PWM_MAX_CHANNEL+1):
  PWMOutputs[channel] = spiagent.PWM(t, spiagent.PWM_PINS[channel-1], frequency)
  PWMOutputs[channel].dutycycle = 0.0

# Main program loop--Let operator set PWM output duty cycles

while True:

# Get channel number

  try:
    channel = int(input('Enter PWM channel number (1-4):             '))
  except:
    print('ERROR: ' + str(sys.exc_info()[1]))
    continue

# Exit on channel zero

  if channel == 0:
    t.close();
    sys.exit(0)

# Validate channel number

  if not channel in PWMOutputs:
    print('ERROR: Invalid PWM channel number')
    continue

# Set PWM output duty cycle

  try:
    PWMOutputs[channel].dutycycle = float(input('Enter PWM output duty cycle (0.0 to 100.0): '))
  except:
    print('ERROR: ' + str(sys.exc_info()[1]))
