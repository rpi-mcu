#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board
# LEGO Power Functions RC car test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

################################################################################

# This program requires a Parallax 2-Axis Joystick (item 27800 or Radio Shack
# 275-030) or equivalent connected to GPIO0 (horizontal axis) and GPIO1
# (vertical axis) analog inputs.  Connect the center tap of the horizontal
# potentiometer to GPIO0 and the center tap of the vertical potentiometer to
# GPIO1.  Connect the top terminal of both potentiometers to 3V3 and the
# bottom terminals to ground.

################################################################################

import math
import spiagent
import sys
import time

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board')
print('LEGO Power Functions Remote Control Remotely Piloted Vehicle Test\n')

################################################################################

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
  channel = 1
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
  channel = 1
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
  chanel = 1
elif len(sys.argv) == 4:
  server = sys.argv[1]
  transport = sys.argv[2]
  channel = int(sys.argv[3])
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc] [channel]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

################################################################################

# Configure analog inputs

AD1 = spiagent.ADC(t, spiagent.LPC1114_AD1)
AD2 = spiagent.ADC(t, spiagent.LPC1114_AD2)

# Configure IRED output

IRED = spiagent.LegoRC(t, spiagent.LPC1114_GPIO7)

################################################################################

print('Press CONTROL-C to quit\n')

xbias = 0.015625
ybias = -0.017578125

steerlimit = 3
speedlimit = 7

try:
  while True:

# Sample joystick position, normalized to -1.0 to +1.0 in both X and Y dimensions

    x = max(min(2.0*AD1.voltage/spiagent.ANALOG_SPAN - 1.0 + xbias, 1.0), -1.0)
    y = max(min(2.0*AD2.voltage/spiagent.ANALOG_SPAN - 1.0 + ybias, 1.0), -1.0)

# Calculate steerage and motor speeds

    steer = max(min(math.floor(x*steerlimit + 0.5), steerlimit), -steerlimit)
    speed1 = max(min(math.floor(y*speedlimit + 0.5) - steer, speedlimit), -speedlimit)
    speed2 = max(min(math.floor(y*speedlimit + 0.5) + steer, speedlimit), -speedlimit)

    print('Left motor speed:%2d' % speed2, end='')
    print('  Right motor speed:%2d' % speed1, end='')
    print('\r', end='')

# Calculate directions for each motor

    if speed1 < 0:
      direction1 = 0
    else:
      direction1 = 1

    if speed2 > 0:
      direction2 = 0
    else:
      direction2 = 1

# Send motor commands

    IRED.SendCommand(channel, spiagent.LEGORC_MOTORA, direction1, abs(speed1))
    IRED.SendCommand(channel, spiagent.LEGORC_MOTORB, direction2, abs(speed2))
    time.sleep(0.1)

# Handle CONTROL-C

except KeyboardInterrupt:

# Graceful shutdown

  print('')
  IRED.SendCommand(channel, spiagent.LEGORC_ALLSTOP, 0, 0)
  t.close()
  sys.exit()
