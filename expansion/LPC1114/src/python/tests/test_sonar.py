#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# sonar ranging test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# LPC1114 GPIO0 -- Echo pulse input
# LPC1114 GPIO1 -- Trigger pulse output

# NOTE: This program is written for and tested with 4-pin ultrasonic
# modules like the HC-SR04.  To use a 3-pin module like the Parallax
# Ping, connect GPIO1 to GPIO0 through a Schottky signal diode like the
# BAT43, anode to GPIO1 and cathode to GPIO0.

import sys
import time
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board Sonar Test\n')

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Configure LPC1114 hardware timer CT32B1

T1 = spiagent.Timer(t, spiagent.CT32B1)
T1.ConfigurePrescaler(1)
T1.ConfigureCapture(spiagent.TIMER_CAPTURE_EDGE_CAP0_BOTH)
T1.ConfigureMode(spiagent.TIMER_MODE_PCLK)

# Configure GPIO1 as trigger pulse output

trigger = spiagent.GPIO(t, spiagent.LPC1114_GPIO1, spiagent.GPIO_MODE_OUTPUT)
trigger.state = 0

print('Press CONTROL-C to quit\n')

try:
  while True:
    # Set trigger output high

    trigger.state = 1

    # Set trigger output low

    trigger.state = 0

    # Wait for echo

    time.sleep(0.1)

    # Calculate distance from echo time

    distance = round(T1.capture_delta/282.35, 0)

    if distance < 2000:
      print('Detection! Range is ' + str(distance) + ' mm          ', end='\r')
    else:
      print('Probing...                        ', end='\r')

    # Probe once a second

    time.sleep(0.9)

# Handle CONTROL-C

except KeyboardInterrupt:

# Graceful shutdown

  print('')
  t.close()
  sys.exit(0)
