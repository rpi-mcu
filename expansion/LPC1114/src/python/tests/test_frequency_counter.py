#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# frequency counter test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import time
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board Frequency Counter Test\n')

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Initialize Timer 0

T0 = spiagent.Timer(t, spiagent.CT32B0)

# Configure Timer 0 prescaler

T0.ConfigurePrescaler(1)

# Configure Timer 0 capture input

T0.ConfigureCapture(spiagent.TIMER_CAPTURE_EDGE_CAP0_RISING)

# Configure Timer 0 mode

T0.ConfigureMode(spiagent.TIMER_MODE_PCLK)

print('Press CONTROL-C to quit\n')

try:
  while True:
    time.sleep(1)

    # Calculate the frequency

    count = T0.capture_delta

    if count < 1920:
      freq = 0.0
    else:
      freq = round(48000000.0/count, 1)

    # Print the frequency

    print('Frequency is ' + str(freq) + ' Hz          ', end='\r')

# Handle CONTROL-C

except KeyboardInterrupt:

# Graceful shutdown

  print('')
  t.close()
  sys.exit(0)
