#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board analog joystick test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

################################################################################

# This program requires a Parallax 2-Axis Joystick (item 27800 or Radio Shack
# 275-030) or equivalent connected to GPIO0 (horizontal axis) and GPIO1
# (vertical axis) analog inputs.  Connect the center tap of the horizontal
# potentiometer to GPIO0 and the center tap of the vertical potentiometer to
# GPIO1.  Connect the top terminal of both potentiometers to 3V3 and the
# bottom terminals to ground.

################################################################################

import math
import spiagent
import sys
import time
import turtle

################################################################################

# Press SPACEBAR to quit the program

ExitFlag = False

# Exit key handler

def KeyHandler():
  global ExitFlag
  ExitFlag = True

################################################################################

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Configure analog inputs

AD1 = spiagent.ADC(t, spiagent.LPC1114_AD1)
AD2 = spiagent.ADC(t, spiagent.LPC1114_AD2)

# Configure turtle graphics

MyTurtle = turtle.Turtle()
MyTurtle.getscreen().setup(500, 500)
MyTurtle.getscreen().onkey(KeyHandler, 'space')
MyTurtle.getscreen().listen()
MyTurtle.shape('circle')
MyTurtle.penup()
MyTurtle.hideturtle()
MyTurtle.goto(-100, 200)
MyTurtle.write('Joystick Test', font=("Arial", 24, "bold"))
MyTurtle.goto(-140, -225)
MyTurtle.write('Press SPACEBAR to quit', font=("Arial", 18, "normal"))
MyTurtle.goto(0, 0)
MyTurtle.showturtle()

################################################################################

# Move the turtle around with the joystick

while not ExitFlag:
  x = math.floor(500*(AD1.voltage/spiagent.ANALOG_SPAN - 0.5) + 0.5)
  y = math.floor(500*(AD2.voltage/spiagent.ANALOG_SPAN - 0.5) + 0.5)

  MyTurtle.goto(x, y)
  time.sleep(0.01)

# Graceful shutdown

t.close()
sys.exit(0)
