#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# square wave output test program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board Square Wave Output Test\n')

# Validate parameters

if len(sys.argv) == 1:
  server = 'localhost'
  transport = 'auto'
  freq = 1000
elif len(sys.argv) == 2:
  server = sys.argv[1]
  transport = 'auto'
  freq = 1000
elif len(sys.argv) == 3:
  server = sys.argv[1]
  transport = sys.argv[2]
  freq = 1000
elif len(sys.argv) == 4:
  server = sys.argv[1]
  transport = sys.argv[2]
  freq = int(sys.argv[3])
else:
  print('Usage: ' + sys.argv[0] + ' [<hostname>] [auto|libspiagent|xml-rpc] [frequency]')
  sys.exit(1)

# Open connection to the server

try:
  t = spiagent.Transport(server, transport)
except:
  print('ERROR: ' + str(sys.exc_info()[1]))
  sys.exit(1)

# Configure LPC1114 32-bit timer 1 to output square wave on MAT0 aka GPIO1

T1 = spiagent.Timer(t, spiagent.CT32B1)
T1.ConfigurePrescaler(1)
T1.ConfigureMatch(0, spiagent.PCLK_FREQUENCY//freq//2, spiagent.TIMER_MATCH_OUTPUT_TOGGLE, spiagent.TIMER_MATCH_RESET)
T1.ConfigureMode(spiagent.TIMER_MODE_PCLK)

# Graceful shutdown

t.close()
sys.exit(0)
