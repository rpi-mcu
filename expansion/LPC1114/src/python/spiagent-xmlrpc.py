#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# transport module using XML-RPC

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import errno
import socket
import xmlrpc.client

library_name = 'xmlrpc.client'

# Initialize server proxy object to known invalid server
#   servername is a string of the form:
#     [xmlrpc://]<host name>

_server = xmlrpc.client.ServerProxy('http://bogus.munts.net:55555/RPC2')

# Open a connection to the specified server

def open(servername):
  if servername.startswith('xmlrpc://'):
    servername = servername[9:]

  global _server

  try:
    servername = socket.gethostbyname(servername)
  except:
    return errno.EIO

  try:
    portnumber = socket.getservbyname('spiagent-xmlrpc', 'tcp')
  except:
    portnumber = 8080

  try:
    _server = xmlrpc.client.ServerProxy('http://' + servername + ':' + str(portnumber) + '/RPC2')
    resp = _server.spi.agent.transaction(0, 0, 0)
    return 0
  except:
    return errno.EIO

# Dispatch a command
#   cmd must be passed in as a list or tuple of 3 integers (command, pin, data)
#   resp must be passed in as an empty list and will be updated as
#   a list of 4 integers (command, pin, data, error)

def command(cmd, resp):
  try:
    resp += _server.spi.agent.transaction(cmd[0], cmd[1], cmd[2])
    return 0
  except:
    return errno.EIO

# Close the server connection

def close():
  global _server
  _server = xmlrpc.client.ServerProxy('http://bogus.munts.net:55555/RPC2')
  return 0
