// Raspberry Pi LPC1114 I/O Processor Expansion Board Firmware Version Test

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import java.util.Arrays;

import static SPIAgent.commands.*;
import static SPIAgent.libspiagent.*;

class test_version
{
  public static final String NUL = "\000";
  public static final String DEFAULTSERVER = "localhost" + NUL;

  public static void main(String args[])
  {
    byte[] servername;
    int[] cmd = new int[3];
    int[] resp = new int[4];
    int[] error = new int[1];

    System.out.println("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Firmware Version Test\n");

    switch (args.length)
    {
      case 0 :
        servername = DEFAULTSERVER.getBytes();
        break;

      case 1 :
        servername = (args[0] + NUL).getBytes();
        break;

      default :
        servername = null;
        System.out.println("Usage: java -jar test_version.jar [hostname]\n");
        System.exit(1);
        break;
    }

    spiagent_open(servername, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_open() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    Arrays.fill(cmd, 0);
    Arrays.fill(resp, 0);

    cmd[0] = SPIAGENT_CMD_NOP.ordinal();

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("The SPI Agent Firmware version number is " + Integer.toString(resp[2]));

    Arrays.fill(cmd, 0);
    Arrays.fill(resp, 0);

    cmd[0] = SPIAGENT_CMD_GET_SFR.ordinal();
    cmd[1] = 0x400483F4;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("The LPC1114 device ID is 0x" + Integer.toHexString(resp[2]) + "\n");

    spiagent_close(error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_close() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }
  }
}
