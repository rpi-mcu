// Java binding for the Raspberry PI LPC1114 I/O Processor Expansion Board
// SPI Agent firmware

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package SPIAgent;

public final class pins
{

// Raspberry Pi LPC1114 I/O Processor Expansion Board GPIO pins

  public static final int LPC1114_INT		= lpc11xx.PIO0_3;
  public static final int LPC1114_READY		= lpc11xx.PIO0_11;
  public static final int LPC1114_LED		= lpc11xx.PIO0_7;
  public static final int LPC1114_GPIO0		= lpc11xx.PIO1_0;
  public static final int LPC1114_GPIO1		= lpc11xx.PIO1_1;
  public static final int LPC1114_GPIO2		= lpc11xx.PIO1_2;
  public static final int LPC1114_GPIO3		= lpc11xx.PIO1_3;
  public static final int LPC1114_GPIO4		= lpc11xx.PIO1_4;
  public static final int LPC1114_GPIO5		= lpc11xx.PIO1_5;
  public static final int LPC1114_GPIO6		= lpc11xx.PIO1_8;
  public static final int LPC1114_GPIO7		= lpc11xx.PIO1_9;

// Raspberry Pi LPC1114 I/O Processor Expansion Board GPIO special function pin aliases

  public static final int LPC1114_AD1		= LPC1114_GPIO0;
  public static final int LPC1114_AD2		= LPC1114_GPIO1;
  public static final int LPC1114_AD3		= LPC1114_GPIO2;
  public static final int LPC1114_AD4		= LPC1114_GPIO3;
  public static final int LPC1114_AD5		= LPC1114_GPIO4;

  public static final int LPC1114_PWM1		= LPC1114_GPIO1;
  public static final int LPC1114_PWM2		= LPC1114_GPIO2;
  public static final int LPC1114_PWM3		= LPC1114_GPIO3;
  public static final int LPC1114_PWM4		= LPC1114_GPIO7;

  public static final int LPC1114_CT32B1_CAP0	= LPC1114_GPIO0;
  public static final int LPC1114_CT32B1_MAT0	= LPC1114_GPIO1;
  public static final int LPC1114_CT32B1_MAT1	= LPC1114_GPIO2;
  public static final int LPC1114_CT32B1_MAT2	= LPC1114_GPIO3;
  public static final int LPC1114_CT32B1_MAT3	= LPC1114_GPIO4;
  public static final int LPC1114_CT32B0_CAP0	= LPC1114_GPIO5;
}
