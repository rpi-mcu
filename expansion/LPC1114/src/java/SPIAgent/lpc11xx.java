// LPC11xx ARM Cortex-M0 Microntroller Hardware Definitions

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package SPIAgent;

public final class lpc11xx
{

// LPC11xx GPIO pin definitions (extracted from arm-mcu/lpc11xx/gpio.h)

  public static final int PIO0_0  = 0;
  public static final int PIO0_1  = 1;
  public static final int PIO0_2  = 2;
  public static final int PIO0_3  = 3;
  public static final int PIO0_4  = 4;
  public static final int PIO0_5  = 5;
  public static final int PIO0_6  = 6;
  public static final int PIO0_7  = 7;
  public static final int PIO0_8  = 8;
  public static final int PIO0_9  = 9;
  public static final int PIO0_10 = 10;
  public static final int PIO0_11 = 11;
  public static final int PIO1_0  = 12;
  public static final int PIO1_1  = 13;
  public static final int PIO1_2  = 14;
  public static final int PIO1_3  = 15;
  public static final int PIO1_4  = 16;
  public static final int PIO1_5  = 17;
  public static final int PIO1_6  = 18;
  public static final int PIO1_7  = 19;
  public static final int PIO1_8  = 20;
  public static final int PIO1_9  = 21;
  public static final int PIO1_10 = 22;
  public static final int PIO1_11 = 23;
  public static final int PIO2_0  = 24;
  public static final int PIO2_1  = 25;
  public static final int PIO2_2  = 26;
  public static final int PIO2_3  = 27;
  public static final int PIO2_4  = 28;
  public static final int PIO2_5  = 29;
  public static final int PIO2_6  = 30;
  public static final int PIO2_7  = 31;
  public static final int PIO2_8  = 32;
  public static final int PIO2_9  = 33;
  public static final int PIO2_10 = 34;
  public static final int PIO2_11 = 35;
  public static final int PIO3_0  = 36;
  public static final int PIO3_1  = 37;
  public static final int PIO3_2  = 38;
  public static final int PIO3_3  = 39;
  public static final int PIO3_4  = 40;
  public static final int PIO3_5  = 41;
}
