// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent firmware services over XML-RPC

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "spi_agent_oncrpc.h"
#include <spi-agent.h>

static CLIENT *clienthandle = NULL;

void spiagent_command_oncrpc(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  if (clienthandle == NULL)
  {
    *error = EBADF;
    return;
  }

#ifdef ONCRPCTHREADUNSAFE
  SPIAGENT_RESPONSE_MSG_t *r = spi_transaction_1(*cmd, clienthandle);

  if (r == NULL)
#else
  if (spi_transaction_1(*cmd, resp, clienthandle))
#endif
  {
#ifdef DEBUG
    clnt_perror(clienthandle, "ERROR: spi_transaction_1() failed");
#endif

    *error = EIO;
    return;
  }

#ifdef ONCRPCTHREADUNSAFE
  memcpy(resp, r, sizeof(SPIAGENT_RESPONSE_MSG_t));
#endif

  *error = 0;
}

void spiagent_open_oncrpc(const char *servername, int32_t *error)
{
  struct timeval tv;
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Check whether we already have an open connection

  if (clienthandle != NULL)
  {
    *error = EBUSY;
    return;
  }

  // NULL servername is not allowed

  if (servername == NULL)
  {
    *error = EINVAL;
    return;
  }

  // Create client handle

  clienthandle = clnt_create(servername, SPI_AGENT_ONCRPC, SPI_AGENT_ONCRPC_VERS, "udp");
  if (clienthandle == NULL)
  {
#ifdef DEBUG
    clnt_pcreateerror("ERROR: clnt_create() failed");
#endif
    *error = EIO;
    return;
  }

  // Reduce the timeout

  tv.tv_sec = 2;
  tv.tv_usec = 0;
  clnt_control(clienthandle, CLSET_TIMEOUT, (char *) &tv);

  tv.tv_sec = 0;
  tv.tv_usec = 500000;
  clnt_control(clienthandle, CLSET_RETRY_TIMEOUT, (char *) &tv);

  // Ping the server

  memset(&cmd, 0, sizeof(cmd));
  cmd.command = SPIAGENT_CMD_NOP;

  memset(&resp, 0, sizeof(resp));

  spiagent_command_oncrpc(&cmd, &resp, error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_command_oncrpc() failed, error=%d\n", *error);
#endif

    clnt_destroy(clienthandle);
    clienthandle = NULL;
    *error = EIO;
    return;
  }

#ifdef DEBUG
  fprintf(stderr, "DEBUG: SPI Agent Firmware version is %d\n", resp.data);
#endif

  *error = 0;
}

void spiagent_close_oncrpc(int32_t *error)
{
  if (clienthandle == NULL)
  {
    *error = EBADF;
    return;
  }

  clnt_destroy(clienthandle);
  clienthandle = NULL;
  *error = 0;
}
