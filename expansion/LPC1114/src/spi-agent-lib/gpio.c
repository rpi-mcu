// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// helper functions for C

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


#include <spi-agent.h>

void spiagent_gpio_configure(uint32_t pin, uint32_t direction, uint32_t state, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  if (direction)
    cmd.command = SPIAGENT_CMD_CONFIGURE_GPIO_OUTPUT;
  else
    cmd.command = SPIAGENT_CMD_CONFIGURE_GPIO_INPUT;

  cmd.pin = pin;
  cmd.data = state;

  spiagent_command(&cmd, &resp, error);

  if (!*error && resp.error)
    *error = resp.error;
}

void spiagent_gpio_configure_mode(uint32_t pin, uint32_t mode, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  cmd.command = SPIAGENT_CMD_CONFIGURE_GPIO;
  cmd.pin = pin;
  cmd.data = mode;

  spiagent_command(&cmd, &resp, error);

  if (!*error && resp.error)
    *error = resp.error;
}

void spiagent_gpio_configure_interrupt(uint32_t pin, uint32_t intconfig, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  cmd.command = SPIAGENT_CMD_CONFIGURE_GPIO_INTERRUPT;
  cmd.pin = pin;
  cmd.data = intconfig;

  spiagent_command(&cmd, &resp, error);

  if (!*error && resp.error)
    *error = resp.error;
}

void spiagent_gpio_get(uint32_t pin, uint32_t *state, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  cmd.command = SPIAGENT_CMD_GET_GPIO;
  cmd.pin = pin;
  cmd.data = 0;

  spiagent_command(&cmd, &resp, error);

  if (!*error && resp.error)
    *error = resp.error;

  *state = resp.data;
}

void spiagent_gpio_put(uint32_t pin, uint32_t state, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  cmd.command = SPIAGENT_CMD_PUT_GPIO;
  cmd.pin = pin;
  cmd.data = state;

  spiagent_command(&cmd, &resp, error);

  if (!*error && resp.error)
    *error = resp.error;
}
