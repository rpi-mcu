// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent firmware services over Linux SPI ioctl()

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <libsimpleio/libspi.h>

#include <expansion_lpc1114.h>
#include <spi-agent.h>

static int fd = -1;

// Open the Raspberry Pi LPC1114 I/O Processor Expansion Board SPI device

void spiagent_open_spi(const char *devname, int32_t *error)
{
  // Check whether the SPI device is open

  if (fd != -1)
  {
    *error = EBUSY;
    return;
  }

  SPI_open(devname, LPC1114_SPI_MODE, LPC1114_SPI_WORDSIZE,
    LPC1114_SPI_SPEED, &fd, error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: SPI_open() for %s failed, %s\n",
      LPC1114_SPI_DEV, strerror(*error));
#endif
  }
}

void spiagent_command_spi(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  // Check whether the SPI device is open

  if (fd == -1)
  {
    *error = EBADF;
    return;
  }

  SPI_transaction(fd, SPI_CS_AUTO, cmd, sizeof(SPIAGENT_COMMAND_MSG_t),
    (cmd->command == SPIAGENT_CMD_PUT_LEGORC ? 20000 : 100), resp,
    sizeof(SPIAGENT_RESPONSE_MSG_t), error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: SPI_transaction() failed, %s\n", strerror(*error));
#endif
    memset(resp, 0, sizeof(SPIAGENT_RESPONSE_MSG_t));
  }
  else if ((resp->command != cmd->command) || (resp->pin != cmd->pin))
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: command and pin echo in response do not match command\n");
    fprintf(stderr, "Sent:     command:%d pin:%d\n", cmd->command, cmd->pin);
    fprintf(stderr, "Received: command:%d pin:%d\n", resp->command, resp->pin);
#endif
    *error = EIO;
  }
}

void spiagent_close_spi(int32_t *error)
{
  // Check whether the SPI device is open

  if (fd == -1)
  {
    *error = EBADF;
    return;
  }

  SPI_close(fd, error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: SPI_close() failed, %s\n", strerror(*error));
#endif
  }

  fd = -1;
}
