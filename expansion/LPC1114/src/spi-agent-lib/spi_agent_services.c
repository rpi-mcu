// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// transport services

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#include "resolve.h"

#ifdef ENABLE_IOCTL
extern void spiagent_open_ioctl(const char *servername, int32_t *error);
extern void spiagent_command_ioctl(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
extern void spiagent_close_ioctl(int32_t *error);
#endif

#ifdef ENABLE_HTTP
extern void spiagent_open_http(const char *servername, int32_t *error);
extern void spiagent_command_http(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
extern void spiagent_close_http(int32_t *error);
#endif

#ifdef ENABLE_ONCRPC
extern void spiagent_open_oncrpc(const char *servername, int32_t *error);
extern void spiagent_command_oncrpc(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
extern void spiagent_close_oncrpc(int32_t *error);
#endif

#ifdef ENABLE_XMLRPC
extern void spiagent_open_xmlrpc(const char *servername, int32_t *error);
extern void spiagent_command_xmlrpc(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
extern void spiagent_close_xmlrpc(int32_t *error);
#endif

typedef void (openfn_t)(const char *servername, int32_t *error);
typedef void (commandfn_t)(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
typedef void (closefn_t)(int32_t *error);

typedef struct
{
  int		len;
  char		*scheme;
  openfn_t	*open;
  commandfn_t	*command;
  closefn_t	*close;
} ServiceTransport_t;

static const ServiceTransport_t ServiceTransportTable[] =
{
#ifdef ENABLE_IOCTL
  { 8, "ioctl://",  spiagent_open_ioctl,  spiagent_command_ioctl,  spiagent_close_ioctl  },
#endif
#ifdef ENABLE_ONCRPC
  { 9, "oncrpc://", spiagent_open_oncrpc, spiagent_command_oncrpc, spiagent_close_oncrpc },
#endif
#ifdef ENABLE_XMLRPC
  { 9, "xmlrpc://", spiagent_open_xmlrpc, spiagent_command_xmlrpc, spiagent_close_xmlrpc },
#endif
#ifdef ENABLE_HTTP
  { 7, "http://", spiagent_open_http, spiagent_command_http, spiagent_close_http },
#endif
  { 0, NULL, NULL, NULL, NULL }
};

static const ServiceTransport_t *CurrentTransport = NULL;

void spiagent_open(const char *servername, int32_t *error)
{
  ServiceTransport_t const *p;
  char serveraddr[256];

  // NULL servername is not allowed

  if (servername == NULL)
  {
    *error = EINVAL;
    return;
  }

  // If no scheme is provided, default to the first entry in the table

  if (strstr(servername, "://") == NULL)
  {
    p = ServiceTransportTable;

#ifdef ENABLE_IOCTL
    if (!strcmp(p->scheme, "ioctl://"))
    {
      if (!strcmp(servername, "localhost"))
      {
        p->open(NULL, error);
        if (*error == 0) CurrentTransport = p;
        return;
      }
      else
      {
        p++;	// Don't try to use ioctl() with a remote host
      }
    }
#endif

    if (resolve(servername, serveraddr, sizeof(serveraddr)))
    {
      *error = errno;
      return;
    }

    p->open(serveraddr, error);
    if (*error == 0) CurrentTransport = p;
    return;
  }

  // Search the service transport table for a matching scheme

  for (p = ServiceTransportTable; p->len; p++)
  {
    if (!strncasecmp(servername, p->scheme, p->len))
    {
#ifdef ENABLE_IOCTL
      if (!strcmp(p->scheme, "ioctl://"))
      {
        if (strcmp(servername + p->len, "localhost"))
        {
          *error = EINVAL;
          return;
        }

        p->open(NULL, error);
        if (*error == 0) CurrentTransport = p;
        return;
      }
#endif

      if (resolve(servername + p->len, serveraddr, sizeof(serveraddr)))
      {
        *error = errno;
        return;
      }

      p->open(serveraddr, error);
      if (*error == 0) CurrentTransport = p;
      return;
    }
  }

  // No matching scheme found

  CurrentTransport = NULL;
  *error = EINVAL;
}

void spiagent_command(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  if (CurrentTransport == NULL)
    *error = EINVAL;
  else
    CurrentTransport->command(cmd, resp, error);
}

void spiagent_close(int32_t *error)
{
  if (CurrentTransport == NULL)
    *error = EINVAL;
  else
  {
    CurrentTransport->close(error);
    CurrentTransport = NULL;
  }
}
