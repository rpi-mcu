// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent firmware services over Linux SPI or I2C ioctl()

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <expansion_lpc1114.h>
#include <spi-agent.h>

extern void spiagent_open_i2c(const char *devname, int32_t *error);

extern void spiagent_command_i2c(SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);

extern void spiagent_close_i2c(int32_t *error);

extern void spiagent_open_spi(const char *devname, int32_t *error);

extern void spiagent_command_spi(SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);

extern void spiagent_close_spi(int32_t *error);

// Default command handler function

static void spiagent_command_default(SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  *error = EIO;
}

// Default close handler function

static void spiagent_close_default(int32_t *error)
{
  *error = EIO;
}

// Cache the selected command handler function

static void (*spiagent_command_fn)(SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error) = spiagent_command_default;

// Cache the selected close handler function

static void (*spiagent_close_fn)(int32_t *error) = spiagent_close_default;

// Open the transport device

void spiagent_open_ioctl(const char *servername, int32_t *error)
{
  char devname[256] = "/dev/nonexistent";

  if (spiagent_command_fn != spiagent_command_default)
  {
    *error = EBUSY;
    return;
  }

  // Server name must be NULL

  if (servername != NULL)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: server name for ioctl() services must be NULL\n");
#endif
    *error = EINVAL;
    return;
  }

  // Open the configuration file

  spiagent_config_open(error);
  if (*error) return;

  // Try SPI (Serial Peripheral Interconnect) first

  spiagent_config_get("LPC1114_SPI", devname, sizeof(devname), error);

  if ((*error == 0) && !access(devname, R_OK|W_OK))
  {
    spiagent_open_spi(devname, error);
    if (*error) return;

    spiagent_command_fn = spiagent_command_spi;
    spiagent_close_fn = spiagent_close_spi;

    spiagent_config_close(error);
    *error = 0;
    return;
  }

  // Then try I2C (Inter-Integrated Circuit) next

  spiagent_config_get("LPC1114_I2C", devname, sizeof(devname), error);

  if ((*error == 0) && !access(devname, R_OK|W_OK))
  {
    spiagent_open_i2c(devname, error);
    if (*error) return;
    spiagent_command_fn = spiagent_command_i2c;
    spiagent_close_fn = spiagent_close_i2c;

    spiagent_config_close(error);
    *error = 0;
    return;
  }

  // No transport device found

  spiagent_config_close(error);
  *error = ENODEV;
}

// Pass a command on to the command executor

void spiagent_command_ioctl(SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  spiagent_command_fn(cmd, resp, error);
}

// Close the transport device

void spiagent_close_ioctl(int32_t *error)
{
  spiagent_close_fn(error);
  spiagent_command_fn = spiagent_command_default;
  spiagent_close_fn = spiagent_close_default;
}
