// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// helper functions for C

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>

#include <spi-agent.h>

void spiagent_timer_init(uint32_t timer, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_INIT_TIMER;
  cmd.pin = timer;
  cmd.data = 0;

  spiagent_command(&cmd, &resp, error);

  // Handle errors

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }
}

void spiagent_timer_configure_mode(uint32_t timer,
  uint32_t mode, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  if (mode >= LPC1114_TIMER_MODE_SENTINEL)
  {
    *error = EINVAL;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_CONFIGURE_TIMER_MODE;
  cmd.pin = timer;
  cmd.data = mode;

  spiagent_command(&cmd, &resp, error);

  if (*error)
  {
    return;
  }

  if (resp.error)
  {
    *error = resp.error;
    return;
  }
}

void spiagent_timer_configure_prescaler(uint32_t timer,
  uint32_t divisor, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  if (divisor == 0)
  {
    *error = ENODEV;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_CONFIGURE_TIMER_PRESCALER;
  cmd.pin = timer;
  cmd.data = divisor;

  spiagent_command(&cmd, &resp, error);

  if (*error)
  {
    return;
  }

  if (resp.error)
  {
    *error = resp.error;
    return;
  }
}

void spiagent_timer_configure_capture(uint32_t timer,
  uint32_t edge, uint32_t intr, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  if (edge >= LPC1114_TIMER_CAPTURE_EDGE_SENTINEL)
  {
    *error = EINVAL;
    return;
  }

  if (intr > 1)
  {
    *error = EINVAL;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_CONFIGURE_TIMER_CAPTURE;
  cmd.pin = timer;
  cmd.data = edge | ((intr & 0x01) << 4);

  spiagent_command(&cmd, &resp, error);

  if (*error)
  {
    return;
  }

  if (resp.error)
  {
    *error = resp.error;
    return;
  }
}

void spiagent_timer_configure_match(uint32_t timer, uint32_t match,
  uint32_t value, uint32_t action, uint32_t intr, uint32_t reset, uint32_t stop,
  int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  if (match > 3)
  {
    *error = EINVAL;
    return;
  }

  if (action >= LPC1114_TIMER_MATCH_OUTPUT_SENTINEL)
  {
    *error = EINVAL;
    return;
  }

  if (intr > 1)
  {
    *error = EINVAL;
    return;
  }

  if (reset > 1)
  {
    *error = EINVAL;
    return;
  }

  if (stop > 1)
  {
    *error = EINVAL;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0 + match;
  cmd.pin = timer;
  cmd.data = action | (intr << 4) | (reset << 5) | (stop << 6);

  spiagent_command(&cmd, &resp, error);

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0_VALUE + match;
  cmd.pin = timer;
  cmd.data = value;

  spiagent_command(&cmd, &resp, error);

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }
}

static const uint32_t CounterAddresses[LPC1114_TIMER_ID_SENTINEL] =
{
  0x40014008,
  0x40018008,
};

void spiagent_timer_get(uint32_t timer, uint32_t *count, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_GET_SFR;
  cmd.pin = CounterAddresses[timer];
  cmd.data = 0;

  spiagent_command(&cmd, &resp, error);

  // Handle errors

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }

  // Copy the result

  *count = resp.data;
}

static const uint32_t CaptureAddresses[LPC1114_TIMER_ID_SENTINEL] =
{
  0x4001402C,
  0x4001802C,
};

void spiagent_timer_get_capture(uint32_t timer, uint32_t *count, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_GET_SFR;
  cmd.pin = CaptureAddresses[timer];
  cmd.data = 0;

  spiagent_command(&cmd, &resp, error);

  // Handle errors

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }

  // Copy the result

  *count = resp.data;
}

void spiagent_timer_get_capture_delta(uint32_t timer, uint32_t *count, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (timer >= LPC1114_TIMER_ID_SENTINEL)
  {
    *error = ENODEV;
    return;
  }

  // Dispatch the command

  cmd.command = SPIAGENT_CMD_GET_TIMER_CAPTURE_DELTA;
  cmd.pin = timer;
  cmd.data = 0;

  spiagent_command(&cmd, &resp, error);

  // Handle errors

  if (*error)
  {
    return;
  }

  if (!*error && resp.error)
  {
    *error = resp.error;
    return;
  }

  // Copy the result

  *count = resp.data;
}
