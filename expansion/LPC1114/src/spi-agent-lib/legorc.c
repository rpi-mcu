// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// helper functions for C

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>

#include <spi-agent.h>

void spiagent_legorc_put(uint32_t pin, uint32_t channel, uint32_t motor,
  uint32_t direction, uint32_t speed, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if (!IS_GPIO(pin))
  {
    *error = EINVAL;
    return;
  }

  if ((channel < 1) || (channel > 4))
  {
    *error = EINVAL;
    return;
  }

  if (motor >= LEGORC_MOTOR_SENTINEL)
  {
    *error = EINVAL;
    return;
  }

  if (direction >= LEGORC_DIRECTION_SENTINEL)
  {
    *error = EINVAL;
    return;
  }

  if (speed > 255)
  {
    *error = EINVAL;
    return;
  }

  // Build the command structure

  cmd.command = SPIAGENT_CMD_PUT_LEGORC;
  cmd.pin = pin;
  cmd.data = speed | (direction << 8) | (motor << 16) | (channel << 24);

  // Issue the command

  spiagent_command(&cmd, &resp, error);

  // Handle errors

  if (!*error && resp.error)
    *error = resp.error;
}
