// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent firmware services over XML-RPC

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <xmlrpc.h>
#include <xmlrpc_client.h>

#include <spi-agent.h>

#define CLIENTNAME	"Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent XML-RPC Client"
#define CLIENTVERSION	"1.0"
#define SERVICENAME	"spiagent-xmlrpc"

static char URL[256];
static xmlrpc_env env;

void spiagent_open_xmlrpc(const char *servername, int32_t *error)
{
  struct servent *service;
  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;
  xmlrpc_value *results;

  // Check for open connection

  if (URL[0] != 0)
  {
    *error = EBUSY;
    return;
  }

  // NULL servername is not allowed

  if (servername == NULL)
  {
    *error = EINVAL;
    return;
  }

  // Look up the service

  service = getservbyname(SERVICENAME, "tcp");
  if (service == NULL)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: getservbyname() failed, unknown service\n");
#endif
    *error = EIO;
    return;
  }

  // Build the XML-RPC server URL

  memset(URL, 0, sizeof(URL));
  snprintf(URL, sizeof(URL), "http://%s:%d/RPC2", servername, ntohs(service->s_port));

  // Initialize the XML-RPC client library

  xmlrpc_env_init(&env);

  // Initialize the client connection

  xmlrpc_client_init2(&env, XMLRPC_CLIENT_NO_FLAGS, CLIENTNAME, CLIENTVERSION, NULL, 0);

  if (env.fault_occurred)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: xmlrpc_client_init2() failed, error=%d (%s)\n",
      env.fault_code, env.fault_string);
#endif

    xmlrpc_env_clean(&env);
    xmlrpc_client_cleanup();
    *error = EIO;
    return;
  }

  // Ping the server

  memset(&cmd, 0, sizeof(cmd));
  memset(&resp, 0, sizeof(resp));

  results = xmlrpc_client_call(&env, URL, "spi.agent.transaction", "(iii)",
    cmd.command, cmd.pin, cmd.data);

  if (env.fault_occurred)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: xmlrpc_client_call() to %s failed, error=%d (%s)\n",
      URL, env.fault_code, env.fault_string);
#endif

    *error = EIO;
    return;
  }

  xmlrpc_decompose_value(&env, results, "(iiii)",
    &resp.command, &resp.pin, &resp.data, &resp.error);

  if (env.fault_occurred)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: xmlrpc_decompose_value() failed, error=%d (%s)\n",
      env.fault_code, env.fault_string);
#endif

    xmlrpc_DECREF(results);
    *error = EIO;
    return;
  }

#ifdef DEBUG
  fprintf(stderr, "DEBUG: SPI Agent Firmware version is %d\n", resp.data);
#endif

  xmlrpc_DECREF(results);
  *error = 0;
}

void spiagent_command_xmlrpc(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  xmlrpc_value *results;

  // Check for open connection

  if (URL[0] == 0)
  {
    *error = EBADF;
    return;
  }

  results = xmlrpc_client_call(&env, URL, "spi.agent.transaction", "(iii)",
    cmd->command, cmd->pin, cmd->data);

  if (env.fault_occurred)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: xmlrpc_client_call() to %s failed, error=%d (%s)\n",
      URL, env.fault_code, env.fault_string);
#endif

    *error = EIO;
    return;
  }

  xmlrpc_decompose_value(&env, results, "(iiii)",
    &resp->command, &resp->pin, &resp->data, &resp->error);

  if (env.fault_occurred)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: xmlrpc_decompose_value() failed, error=%d (%s)\n",
      env.fault_code, env.fault_string);
#endif

    xmlrpc_DECREF(results);
    *error = EIO;
    return;
  }

  xmlrpc_DECREF(results);
  *error = 0;
}

void spiagent_close_xmlrpc(int32_t *error)
{
  // Check for open connection

  if (URL[0] == 0)
  {
    *error = EBADF;
    return;
  }

  xmlrpc_env_clean(&env);
  xmlrpc_client_cleanup();

  memset(&env, 0, sizeof(env));
  memset(URL, 0, sizeof(URL));
  *error = 0;
}
