#!/usr/bin/python3

# Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
# loopback test program using libspiagent.so

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import time
import spiagent

print('\nRaspberry Pi LPC1114 I/O Processor Expansion Board Firmware Test\n')

# Validate parameters

if len(sys.argv) == 1:
  servername = 'localhost'
  ITERATIONS = 50000
elif len(sys.argv) == 2:
  servername = sys.argv[1]
  ITERATIONS = 50000
elif len(sys.argv) == 3:
  servername = sys.argv[1]
  ITERATIONS = int(sys.argv[2])
else:
  print('Usage: ' + sys.argv[0] + ' [hostname]')
  sys.exit(1)

# Import transport module

transport = __import__('spiagent-libspiagent')

# Open connection to the server

error = transport.open(servername)
if error != 0:
  print('ERROR: transport.open() failed, error=' + str(error))
  sys.exit(1)

print('Issuing some SPI transactions...\n')

# Issue NOP command to the LPC1114

cmd = (spiagent.CMD_NOP, 0,  0)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

# Display results

print('Response: command:' + str(resp[0]) + ' pin:' + str(resp[1]) +
  ' data:' + str(resp[2]) + ' error:' + str(resp[3]))

# Issue LOOPBACK command to the LPC1114

cmd = (spiagent.CMD_LOOPBACK, 2,  3)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

# Display results

print('Response: command:' + str(resp[0]) + ' pin:' + str(resp[1]) +
  ' data:' + str(resp[2]) + ' error:' + str(resp[3]))

# Issue command with illegal pin value to the LPC1114

cmd = (spiagent.CMD_GET_GPIO, 99,  3)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

# Display results

print('Response: command:' + str(resp[0]) + ' pin:' + str(resp[1]) +
  ' data:' + str(resp[2]) + ' error:' + str(resp[3]))

# Issue illegal command to the LPC1114

cmd = (99, 2,  3)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

# Display results

print('Response: command:' + str(resp[0]) + ' pin:' + str(resp[1]) +
  ' data:' + str(resp[2]) + ' error:' + str(resp[3]) + '\n')

# Issue NOP command to the LPC1114

cmd = (spiagent.CMD_NOP, 0,  0)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

if resp[3] != 0:
  print('ERROR: SPI Agent Firmware returned error=' + str(resp[3]))
  sys.exit(1)

# Display result

print('The LPC1114 firmware version number is ' + str(resp[2]))

# Issue GET_SFR command to the LPC1114

cmd = (spiagent.CMD_GET_SFR, 0x400483F4,  0)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

if resp[3] != 0:
  print('ERROR: SPI Agent Firmware returned error=' + str(resp[3]))
  sys.exit(1)

# Display result

print('The LPC1114 device ID is               ' + hex(resp[2]))

# Issue GET_GPIO command to the LPC1114

cmd = (spiagent.CMD_GET_GPIO, spiagent.LPC1114_LED,  0)
resp = []
error = transport.command(cmd, resp)

# Handle errors

if error != 0:
  print('ERROR: transport.command() failed, error=' + str(error))
  sys.exit(1)

if resp[3] != 0:
  print('ERROR: SPI Agent Firmware returned error=' + str(resp[3]))
  sys.exit(1)

# Display result

if resp[2]:
  print('The expansion board LED is             ON\n')
else:
  print('The expansion board LED is             OFF\n')

# Do loopback torture test

print('Starting %d SPI agent loopback test transactions...' % (ITERATIONS))

start = time.time()

for i in range(ITERATIONS):
  cmd = (spiagent.CMD_LOOPBACK, i*17, i*19)
  resp = []
  error = transport.command(cmd, resp)

  if error != 0:
    print('ERROR: transport.command() failed, error=' + str(error))
    sys.exit(1)

  if ((resp[0] != cmd[0]) or (resp[1] != cmd[1]) or
    (resp[2] != cmd[2]) or (resp[3] != 0)):
    print('Iteration:' + str(i) + ' Response: command:' + str(resp[0]) +
      ' pin:' + str(resp[1]) + ' data:' + str(resp[2]) + ' error:' +
      str(resp[3]))

finish = time.time()

# Print statistics

deltat = finish - start

print('\nPerformed %d loopback tests in %1.1f seconds' % (ITERATIONS, deltat))
print('  %1.1f iterations per second' % (ITERATIONS/deltat,))
print('  %1.1f microseconds per iteration' % (1000000/ITERATIONS*deltat,))

# Graceful shutdown

transport.close()
sys.exit(0)
