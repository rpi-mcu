// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// loopback test program

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import static SPIAgent.commands.*;
import static SPIAgent.pins.*;
import static SPIAgent.libspiagent.*;

class spi_agent_test
{
  public static final String NUL = "\000";
  public static final String DEFAULTSERVER = "localhost" + NUL;
  public static final int DEFAULTITERATIONS = 50000;

  public static void main(String args[])
  {
    byte[] servername;
    int iterations = 0;
    int[] cmd = new int[3];
    int[] resp = new int[4];
    int[] error = new int[1];
    int i;

    System.out.println("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

    switch (args.length)
    {
      case 0 :
        servername = DEFAULTSERVER.getBytes();
        iterations = DEFAULTITERATIONS;
        break;

      case 1 :
        servername = (args[0] + NUL).getBytes();
        iterations = DEFAULTITERATIONS;
        break;

      case 2 :
        servername = (args[0] + NUL).getBytes();
        iterations = Integer.parseInt(args[1]);
        break;

      default :
        servername = null;
        System.out.println("Usage: java -jar spi_agent_test.jar [hostname] [iterations]\n");
        System.exit(1);
        break;
    }

    spiagent_open(servername, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_open() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("Issuing some SPI transactions...\n");

    // Issue NOP command

    cmd[0] = SPIAGENT_CMD_NOP.ordinal();
    cmd[1] = 0;
    cmd[2] = 0;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("Response: command:" + Integer.toString(resp[0]) + " pin:" +
      Integer.toString(resp[1]) + " data:" + Integer.toString(resp[2]) + " error:" +
      Integer.toString(resp[3]));

    // Issue LOOPBACK command

    cmd[0] = SPIAGENT_CMD_LOOPBACK.ordinal();
    cmd[1] = 2;
    cmd[2] = 3;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("Response: command:" + Integer.toString(resp[0]) + " pin:" +
      Integer.toString(resp[1]) + " data:" + Integer.toString(resp[2]) + " error:" +
      Integer.toString(resp[3]));

    // Issue bad pin command

    cmd[0] = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd[1] = 99;
    cmd[2] = 3;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("Response: command:" + Integer.toString(resp[0]) + " pin:" +
      Integer.toString(resp[1]) + " data:" + Integer.toString(resp[2]) + " error:" +
      Integer.toString(resp[3]));

    // Issue illegal command

    cmd[0] = 99;
    cmd[1] = 2;
    cmd[2] = 3;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("Response: command:" + Integer.toString(resp[0]) + " pin:" +
      Integer.toString(resp[1]) + " data:" + Integer.toString(resp[2]) + " error:" +
      Integer.toString(resp[3]) + "\n");

    // Query the SPI Agent Firmware version number

    cmd[0] = SPIAGENT_CMD_NOP.ordinal();
    cmd[1] = 0;
    cmd[2] = 0;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    if (resp[3] != 0)
    {
      System.out.println("ERROR: The SPI Agent Firmware returned error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("The LPC1114 firmware version is " + Integer.toString(resp[2]));

    // Query the LPC1114 chip ID

    cmd[0] = SPIAGENT_CMD_GET_SFR.ordinal();
    cmd[1] = 0x400483F4;
    cmd[2] = 0;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    if (resp[3] != 0)
    {
      System.out.println("ERROR: The SPI Agent Firmware returned error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("The LPC1114 device ID is        " + Integer.toHexString(resp[2]));

    // Query the LED state

    cmd[0] = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd[1] = LPC1114_LED;
    cmd[2] = 0;

    spiagent_command(cmd, resp, error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    if (resp[3] != 0)
    {
      System.out.println("ERROR: The SPI Agent Firmware returned error=" + Integer.toString(error[0]));
      System.exit(1);
    }

    System.out.println("The expansion board LED is      " + ((resp[2] != 0) ? "ON" : "OFF") + "\n");

    // Perform the loopback torture test

    System.out.println("Starting " + Integer.toString(iterations) + " SPI agent loopback test transactions...\n");

    long starttime = System.nanoTime();

    for (i = 1; i <= iterations; i++)
    {
      cmd[0] = SPIAGENT_CMD_LOOPBACK.ordinal();
      cmd[1] = i*17;
      cmd[2] = i*19;

      spiagent_command(cmd, resp, error);

      if (error[0] != 0)
      {
        System.out.println("ERROR: spiagent_command() failed, error=" + Integer.toString(error[0]));
        System.exit(1);
      }

      if ((resp[0] != cmd[0]) || (resp[1] != cmd[1]) || (resp[2] != cmd[2]) || (resp[3] != 0))
      {
        System.out.println("Iteration:" + Integer.toString(i) + " Response: command:" +
        Integer.toString(resp[0]) + " pin:" + Integer.toString(resp[1]) + " data:" +
        Integer.toString(resp[2]) + " error:" + Integer.toString(resp[3]));
      }
    }

    long endtime = System.nanoTime();

    // Display statistics

    double deltat = (endtime - starttime)*1.0e-9;
    double rate = iterations/deltat;
    double cycletime = deltat/iterations;

    System.out.println("Performed " + Integer.toString(iterations) + " loopback tests in " +
      String.format("%1.1f", deltat) + " seconds");

    System.out.println("  " + String.format("%1.1f", rate) + " iterations per second");

    System.out.println("  " + String.format("%1.1f", cycletime*1.0E6) + " microseconds per iteration" + "\n");

    spiagent_close(error);

    if (error[0] != 0)
    {
      System.out.println("ERROR: spiagent_close() failed, error=" + Integer.toString(error[0]));
      System.exit(1);
    }
  }
}
