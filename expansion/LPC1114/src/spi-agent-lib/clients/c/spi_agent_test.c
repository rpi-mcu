// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// loopback test program

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc >= 2) ? argv[1] : "localhost")
#define ITERATIONS	((argc == 3) ? atoi(argv[2]) : 50000)

static volatile sig_atomic_t breakflag = 0;

static void breakhandler(int sig)
{
  breakflag = 1;
  signal(sig, breakhandler);
}

int main(int argc, char *argv[])
{
  int32_t error;
  time_t start, finish;
  SPIAGENT_COMMAND_MSG_t command;
  SPIAGENT_RESPONSE_MSG_t response;
  int i;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

  // Initialize the transport library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() for %s failed, %s\n",
      SERVERNAME, strerror(error));
    exit(1);
  }

  // Install signal handlers

  if (signal(SIGINT, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGINT failed, %s\n", strerror(errno));
    exit(1);
  }

  if (signal(SIGTERM, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGTERM failed, %s\n", strerror(errno));
    exit(1);
  }

  // Issue some SPI transactions

  puts("Issuing some SPI transactions...\n");

  // Build command message, with NOP command

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_NOP;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%-4d pin:%-4d data:%-5d error:%-4d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with valid fields

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_LOOPBACK;
  command.pin = 2;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%-4d pin:%-4d data:%-5d error:%-4d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with illegal pin number

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = 99;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%-4d pin:%-4d data:%-5d error:%-4d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with illegal command field

  memset(&command, 0, sizeof(command));
  command.command = 99;
  command.pin = 2;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%-4d pin:%-4d data:%-5d error:%-4d\n\n",
    response.command, response.pin, response.data, response.error);

  // Query the LPC1114 firmware version

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_NOP;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: NOP operation failed, %s\n", strerror(response.error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("The LPC1114 firmware version is %d\n", response.data);

  // Query the LPC1114 device ID

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_SFR;
  command.pin = 0x400483F4; // LPC1114 Device ID register

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: GET_SFR operation for register %08X failed, %s\n",
      response.pin, strerror(response.error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("The LPC1114 device ID is        %08X\n", response.data);

  // Query the expansion board LED state

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = LPC1114_LED;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: GET_GPIO operation for pin %d failed, %s\n",
      response.pin, strerror(response.error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("The expansion board LED is      %s\n\n", response.data ? "ON" : "OFF");

  // Perform SPI loopback commands as fast as possible to stress test the SPI
  // interface

  printf("Starting %d SPI agent loopback test transactions...\n", ITERATIONS);
  fflush(stdout);

  time(&start);

  for (i = 0; i < ITERATIONS; i++)
  {
    if (breakflag) break;

    // Build the command message

    memset(&command, 0, sizeof(command));
    command.command = SPIAGENT_CMD_LOOPBACK;
    command.pin = i*17;
    command.data = i*19;

    memset(&response, 0, sizeof(response));

    // Perform the SPI transfers

    spiagent_command(&command, &response, &error);

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
      break;
    }

    // Display the response from the slave MCU

    if ((response.command != command.command) ||
        (response.pin != command.pin) ||
        (response.data != command.data) ||
        (response.error != 0))
    {
      printf("Iteration:%-6d Response: command:%d pin:%d data:%d error:%d\n",
        i, response.command, response.pin, response.data, response.error);
    }
  }

  time(&finish);

  // Display statistics

  printf("\nPerformed %d loopback tests in %ld seconds\n", i, finish - start);

  double deltat = finish - start;
  double rate = i / deltat;

  printf("  %1.1f iterations per second\n", rate);

  double cycletime = deltat / i;

  printf("  %1.1f microseconds per iteration\n", cycletime*1.0E6);

  // Cleanup

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
