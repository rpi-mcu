-- Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
-- loopback test program

-- Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

WITH Ada.Command_Line;      USE Ada.Command_Line;
WITH Ada.Real_Time;         USE Ada.Real_Time;
WITH Ada.Strings.Unbounded; USE Ada.Strings.Unbounded;
WITH Ada.Text_IO;           USE Ada.Text_IO;
WITH Interfaces;            USE Interfaces;

WITH lpc11xx;               USE lpc11xx;
WITH spi_agent.commands;    USE spi_agent.commands;
WITH spi_agent.exceptions;  USE spi_agent.exceptions;
WITH spi_agent.messages;    USE spi_agent.messages;
WITH spi_agent.pins;        USE spi_agent.pins;
WITH spi_agent.transport;   USE spi_agent.transport;

PROCEDURE spi_agent_test IS

  servername : Unbounded_String;
  iterations : Integer := 50000;
  error      : Integer;
  cmd        : SPIAGENT_COMMAND_MSG_t;
  resp       : SPIAGENT_RESPONSE_MSG_t;
  starttime  : Time;
  stoptime   : Time;
  elapsed    : Duration;
  rate       : Duration;

  PACKAGE int_io IS NEW Integer_IO(Integer);
  USE int_io;

  PACKAGE duration_io IS NEW Fixed_IO(Duration);
  USE duration_io;

BEGIN
  New_Line;
  Put_Line("Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test");
  New_Line;

  -- Analyze command line parameters

  CASE Argument_Count IS
    WHEN 0 =>
      servername := To_Unbounded_String("localhost");

    WHEN 1 =>
      servername := To_Unbounded_String(Argument(1));

    WHEN 2 =>
      servername := To_Unbounded_String(Argument(1));
      iterations := Integer'VALUE(Argument(2));

    WHEN OTHERS =>
      Put_Line("Usage: " & Command_Name & " [hostname]");
      RETURN;
  END CASE;

  -- Initialize the SPI Agent Firmware transport library

  spi_agent.transport.open(To_String(servername) & ASCII.NUL, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.open() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  -- Issue a NOP command

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_NOP);
  cmd.pin     := 0;
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  Put("Response: ");
  Put("command:");
  Put(Integer(resp.command), 0, 10);
  Put(" pin:");
  Put(Integer(resp.pin), 0, 10);
  Put(" data:");
  Put(Integer(resp.data), 0, 10);
  Put(" error:");
  Put(Integer(resp.error), 0, 10);
  New_Line(1);

  -- Issue a LOOPBACK command

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_LOOPBACK);
  cmd.pin     := 2;
  cmd.data    := 3;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  Put("Response: ");
  Put("command:");
  Put(Integer(resp.command), 0, 10);
  Put(" pin:");
  Put(Integer(resp.pin), 0, 10);
  Put(" data:");
  Put(Integer(resp.data), 0, 10);
  Put(" error:");
  Put(Integer(resp.error), 0, 10);
  New_Line(1);

  -- Issue an illegal GET_GPIO command

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_GET_GPIO);
  cmd.pin     := 99;
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  Put("Response: ");
  Put("command:");
  Put(Integer(resp.command), 0, 10);
  Put(" pin:");
  Put(Integer(resp.pin), 0, 10);
  Put(" data:");
  Put(Integer(resp.data), 0, 10);
  Put(" error:");
  Put(Integer(resp.error), 0, 10);
  New_Line(1);

  -- Issue an illegal command

  cmd.command := 99;
  cmd.pin     := 0;
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  Put("Response: ");
  Put("command:");
  Put(Integer(resp.command), 0, 10);
  Put(" pin:");
  Put(Integer(resp.pin), 0, 10);
  Put(" data:");
  Put(Integer(resp.data), 0, 10);
  Put(" error:");
  Put(Integer(resp.error), 0, 10);
  New_Line(2);

  -- Query the LPC1114 firmware version

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_NOP);
  cmd.pin     := 0;
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  IF resp.error /= 0 THEN
    RAISE SpiAgentError WITH "SPI Agent Firmware returned error:" &
      Unsigned_32'IMAGE(resp.error);
  END IF;

  Put("The LPC1114 firmware version is ");
  Put(Integer(resp.data), 1, 10);
  New_Line(1);

  -- Query the LPC1114 device ID

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_GET_SFR);
  cmd.pin     := 16#400483F4#;
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  IF resp.error /= 0 THEN
    RAISE SpiAgentError WITH "SPI Agent Firmware returned error:" &
      Unsigned_32'IMAGE(resp.error);
  END IF;

  Put("The LPC1114 device ID is        ");
  Put(Integer(resp.data), 1, 16);
  New_Line(1);

  -- Query the LED state

  cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_GET_GPIO);
  cmd.pin     := pin_id_t'POS(LPC1114_LED);
  cmd.data    := 0;

  spi_agent.transport.command(cmd, resp, error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
      Integer'IMAGE(error);
  END IF;

  IF resp.error /= 0 THEN
    RAISE SpiAgentError WITH "SPI Agent Firmware returned error:" &
      Unsigned_32'IMAGE(resp.error);
  END IF;

  Put("The expansion board LED is      ");

  CASE Integer(resp.data) IS
    WHEN 0 =>
      Put("OFF");

    WHEN 1 =>
      Put("ON");

    WHEN OTHERS =>
      RAISE SpiAgentError WITH "Invalid value for GPIO state";
  END CASE;

  New_Line(2);

  -- Perform loopback test

  Put("Starting" & Integer'IMAGE(iterations) & " SPI agent loopback test transactions...");
  New_Line(2);

  starttime := Clock;

  FOR i IN 1 .. iterations LOOP
    cmd.command := SPIAGENT_COMMAND_t'POS(SPIAGENT_CMD_LOOPBACK);
    cmd.pin     := Unsigned_32(i*17);
    cmd.data    := Unsigned_32(i*19);

    spi_agent.transport.command(cmd, resp, error);

    IF error /= 0 THEN
      RAISE SpiAgentError WITH "spi_agent.transport.command() failed, error:" &
        Integer'IMAGE(error);
    END IF;

    IF (resp.command /= cmd.command) OR (resp.pin /= cmd.pin) OR
       (resp.data /= cmd.data) OR (resp.error /= 0) THEN
      Put("Iteration:");
      Put(i, 0, 10);
      Put(" Response: ");
      Put("command:");
      Put(Integer(resp.command), 0, 10);
      Put(" pin:");
      Put(Integer(resp.pin), 0, 10);
      Put(" data:");
      Put(Integer(resp.data), 0, 10);
      Put(" error:");
      Put(Integer(resp.error), 0, 10);
      New_Line(1);
    END IF;
  END LOOP;

  stoptime := Clock;

  -- Display statistics

  elapsed := To_Duration(stoptime - starttime);
  rate    := Duration(iterations) / elapsed;

  Put("Performed" & Integer'IMAGE(iterations) & " loopback tests in ");
  Put(elapsed, 0, 1);
  Put_Line(" seconds");

  Put("  ");
  Put(rate, 0, 1);
  Put_Line(" iterations per second");

  Put("  ");
  Put(1000000.0 / rate, 0, 1);
  Put_Line(" microseconds per iteration");

  -- Deinitialize the SPI Agent Firmware transport library

  spi_agent.transport.close(error);

  IF error /= 0 THEN
    RAISE SpiAgentError WITH "spi_agent.transport.close() failed, error:" &
      Integer'IMAGE(error);
  END IF;
END spi_agent_test;
