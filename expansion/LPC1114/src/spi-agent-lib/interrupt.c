// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent firmware services over Linux SPI ioctl()

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <libsimpleio/libgpio.h>

#include <spi-agent.h>

static int32_t INT_fd  = -1;
static int32_t INT_pin = -1;

void spiagent_interrupt_enable(int32_t *error)
{
  char value[256];

  // Make sure we have not previously called spiagent_interrupt_enable()

  if (INT_fd != -1)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_interrupt_enable() has already been called\n");
#endif
    *error = EBUSY;
    return;
  }

  // Read pin assignment (if any) from the configuration file

  spiagent_config_open(error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_config_open() failed, %s\n",
      strerror(*error));
#endif
    return;
  }

  spiagent_config_get("LPC1114_INT", value, sizeof(value), error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_config_get() failed, %s\n",
      strerror(*error));
#endif
    return;
  }

  spiagent_config_close(error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_config_close() failed, %s\n",
      strerror(*error));
#endif
    return;
  }

  // Open the interrupt input GPIO

  GPIO_line_open(0, atoi(value), GPIOHANDLE_REQUEST_INPUT,
    GPIOEVENT_REQUEST_RISING_EDGE, 0, &INT_fd, error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: GPIO_line_open() failed, %s\n", strerror(*error));
#endif
    INT_fd = -1;
    return;
  }

  INT_pin = atoi(value);
  *error = 0;
}

void spiagent_interrupt_disable(int32_t *error)
{
  // Make sure we have previously called spiagent_interrupt_enable()

  if (INT_fd == -1)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_interrupt_enable() has not been called\n");
#endif
    *error = EBADF;
    return;
  }

  // Close the interrupt input GPIO

  GPIO_line_close(INT_fd, error);

  INT_fd  = -1;
  INT_pin = -1;

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: GPIO_line_close() failed, %s\n", strerror(*error));
#endif
    return;
  }

  *error = 0;
}

void spiagent_interrupt_wait(uint32_t timeout, uint32_t *pin, int32_t *error)
{
  int32_t state;

  // Make sure we have previously called spiagent_interrupt_enable()

  if (INT_fd == -1)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: spiagent_interrupt_enable() has not been called\n");
#endif
    *error = EBADF;
    return;
  }

  GPIO_line_event(INT_fd, &state, error);

  if (*error)
  {
#ifdef DEBUG
    fprintf(stderr, "ERROR: GPIO_line_event() failed, %s\n", strerror(*error));
#endif
    return;
  }

  *pin = INT_pin;
  *error = 0;
}
