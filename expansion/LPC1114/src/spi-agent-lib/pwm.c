// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// helper functions for C

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <math.h>

#include <spi-agent.h>

static uint32_t frequency = 50;

void spiagent_pwm_set_frequency(uint32_t freq, int32_t *error)
{
  if ((freq < LPC1114_PWM_FREQ_MIN) || (freq > LPC1114_PWM_FREQ_MAX))
  {
    *error = EINVAL;
    return;
  }

  frequency = freq;
  *error = 0;
}

void spiagent_pwm_configure(uint32_t pin, int32_t *error)
{
  if (!IS_PWM(pin))
  {
    *error = EINVAL;
    return;
  }

  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Issue command

  cmd.command = SPIAGENT_CMD_CONFIGURE_PWM_OUTPUT;
  cmd.pin = pin;
  cmd.data = frequency;

  spiagent_command(&cmd, &resp, error);

  // Process response

  if (!*error && resp.error)
    *error = resp.error;
}

void spiagent_pwm_put(uint32_t pin, float dutycycle, int32_t *error)
{
  if (!IS_PWM(pin))
  {
    *error = EINVAL;
    return;
  }

  SPIAGENT_COMMAND_MSG_t cmd;
  SPIAGENT_RESPONSE_MSG_t resp;

  // Validate parameters

  if ((dutycycle < LPC1114_PWM_DUTY_MIN) || (dutycycle > LPC1114_PWM_DUTY_MAX))
  {
    *error = EINVAL;
    return;
  }

  // Issue command

  cmd.command = SPIAGENT_CMD_PUT_PWM;
  cmd.pin = pin;
  cmd.data = lroundf(655.35*dutycycle);

  spiagent_command(&cmd, &resp, error);

  // Process response

  if (!*error && resp.error)
    *error = resp.error;
}

void spiagent_servo_configure(uint32_t pin, int32_t *error)
{
  if (!IS_PWM(pin))
  {
    *error = EINVAL;
    return;
  }

  if (frequency > 400)
  {
    *error = EINVAL;
    return;
  }

  spiagent_pwm_configure(pin, error);
}

void spiagent_servo_put(uint32_t pin, float position, int32_t *error)
{
  if (!IS_PWM(pin))
  {
    *error = EINVAL;
    return;
  }

  if ((position < LPC1114_SERVO_MIN) || (position > LPC1114_SERVO_MAX))
  {
    *error = EINVAL;
    return;
  }

  spiagent_pwm_put(pin, (0.5E-1*position + 1.5E-1)*frequency, error);
}

void spiagent_motor_configure(uint32_t pwmpin, uint32_t dirpin, int32_t *error)
{
  if (!IS_PWM(pwmpin))
  {
    *error = EINVAL;
    return;
  }

  if (!IS_GPIO(dirpin))
  {
    *error = EINVAL;
    return;
  }

  if (pwmpin == dirpin)
  {
    *error = EINVAL;
    return;
  }

  spiagent_pwm_configure(pwmpin, error);
  if (*error) return;
  spiagent_gpio_configure_mode(dirpin, LPC1114_GPIO_MODE_OUTPUT, error);
  if (*error) return;
}

void spiagent_motor_put(uint32_t pwmpin, uint32_t dirpin, float speed, int32_t *error)
{
  if (!IS_PWM(pwmpin))
  {
    *error = EINVAL;
    return;
  }

  if (!IS_GPIO(dirpin))
  {
    *error = EINVAL;
    return;
  }

  if (pwmpin == dirpin)
  {
    *error = EINVAL;
    return;
  }

  if ((speed < LPC1114_MOTOR_SPEED_MIN) || (speed > LPC1114_MOTOR_SPEED_MAX))
  {
    *error = EINVAL;
    return;
  }

  if (speed < 0)
  {
    spiagent_gpio_put(dirpin, 0, error);
    if (*error) return;
    spiagent_pwm_put(pwmpin, -100.0*speed, error);
    if (*error) return;
  }
  else
  {
    spiagent_gpio_put(dirpin, 1, error);
    if (*error) return;
    spiagent_pwm_put(pwmpin, 100.0*speed, error);
    if (*error) return;
  }
}
