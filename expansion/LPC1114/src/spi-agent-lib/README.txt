  Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Shared Library

   The shared library libspiagent.so encapsulates abstract services for
   communicating with the Raspberry Pi LPC1114 I/O Processor Expansion
   Board SPI Agent firmware, using various local and network transport
   services.

   The following declarations, extracted from [1]spi-agent.h, specify the
   abstract services that are available:
// SPI Agent Firmware services

extern void spiagent_open(const char *servername, int32_t *error);

extern void spiagent_command(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_
t *resp, int32_t *error);

extern void spiagent_close(int32_t *error);

   The service transport scheme and server hostname or IP address are
   encoded in the servername parameter supplied to spiagent_open(), using
   a form of [2]URL notation. The following service transport schemes are
   defined:

   ioctl://localhost Linux [3]ioctl() SPI device API (Raspberry Pi Linux,
   local machine only)
   http://<servername> HTTP via [4]libcurl
   oncrpc://<servername> [5]ONC/RPC Remote Procedure Call (over UDP)
   xmlrpc://<servername> [6]XML-RPC Remote Procedure Call (over HTTP)

Operating System Notes

  Unix Derivatives (Linux, MacOS X, OpenBSD, etc.)

   Ada, C, [7]Free Pascal, Java, and Python3 all fully support
   libspiagent.so. Each of these programming languages has its own thin
   binding module.

   All of the service transports described above are included in the
   prebuilt library for the Raspberry Pi, and the ioctl() transport is
   default for best performance.

   For a Unix derivative network client, libspiagent.so can be built to
   include HTTP, ONC/RPC, and XML-RPC service transports. The default
   service transport is ONC/RPC over UDP for best performance.

  Microsoft Windows

   Microsoft Windows is problematic. It is possible to build
   libspiagent.dll using [8]Cygwin. That DLL can then be used for C and
   Python3 clients using Cygwin tools, but not with [9]GNAT Ada, nor
   [10]Free Pascal, nor [11]ActivePython for Windows nor the official
   [12]Python for Windows. The DLL also malfunctions with the Cygwin GNAT
   Ada compiler toolchain, causing hangs at program exit.

   The Cygwin libspiagent.dll can be built to include HTTP, ONC/RPC, and
   XML-RPC transports. The default service transport is ONC/RPC over UDP
   for best performance.
     __________________________________________________________________

   Questions or comments to Philip Munts [13]phil@munts.net

References

   1. http://git.munts.com/rpi-mcu/expansion/LPC1114/src/include/spi-agent.h
   2. http://en.wikipedia.org/wiki/URL
   3. https://www.kernel.org/doc/Documentation/spi/spidev
   4. http://curl.haxx.se/libcurl
   5. http://en.wikipedia.org/wiki/Open_Network_Computing_Remote_Procedure_Call
   6. http://www.xmlrpc.com/
   7. http://www.freepascal.org/
   8. http://www.cygwin.com/
   9. http://libre.adacore.com/
  10. http://www.freepascal.org/
  11. http://www.activestate.com/activepython
  12. https://www.python.org/downloads/windows
  13. mailto:phil@munts.net
