#!/bin/sh

# Script to perform all one-time setup for the LPC1114 I/O Processor Expansion Board

# Short URL:  http://goo.gl/7wtzx
# Invocation: wget -q -O - http://goo.gl/7wtzx | sh

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###############################################################################

# Transform named parameters to environment variables

for x ; do export ${x%=*}="${x#*=}"; done

# Set directory locations

if [ -z $RPISRC ]; then
  export RPISRC=$HOME/rpi-mcu
fi

# Extract board model from /proc/cpuinfo

REVISION=`awk '/Revision/ { print substr($3, length($3)-2, 2); }' /proc/cpuinfo`

###############################################################################

echo ""
echo "Performing initial one-time setup for the Munts Technologies"
echo "LPC1114 I/O Processor Expansion Board MUNTS-0004"
echo ""

# Check for some problems

if [ `id -u` = 0 ]; then
  echo "ERROR: This script must NOT be run as root!"
  echo ""
  exit 1
fi

if [ ! -c /dev/spidev0.0 ]; then
  echo "ERROR: You must enable the SPI interface in raspi-config"
  echo "before running this script!"
  echo ""
  exit 1
fi

if [ ! -c /dev/ttyAMA0 ]; then
  echo "ERROR: You must enable the serial port interface in raspi-config"
  echo "before running this script!"
  echo ""
  exit 1
fi

# Check for Raspberry Pi 3 Model B

if [ "$REVISION" = "08" ]; then
  egrep -q "^dtoverlay=pi3-disable-bt" /boot/config.txt
  if [ $? -ne 0 ]; then
    echo "ERROR: You must disable the internal bluetooth interface"
    echo "by adding 'dtoverlay=pi3-disable-bt' to /boot/config.txt"
    echo "before running this script!"
    echo ""
    exit 1
  fi
fi

# Add repository http://repo.munts.com

if [ ! -f /etc/apt/sources.list.d/repo.munts.com.list ]; then
  echo "Adding package repository http://repo.munts.com"
  echo ""

  grep -q -i stretch /etc/os-release
  if [ $? -eq 0 ]; then
    wget -nv -P /tmp http://repo.munts.com/debian10/raspbian/munts-repo.deb
    sudo dpkg -i /tmp/munts-repo.deb
    rm /tmp/munts-repo.deb
  fi
fi

# Install some necessary packages

echo "Installing packages"
echo ""

sudo apt-get -q -y update
echo ""
sudo apt-get -q -y install git libcurl4-openssl-dev libncurses5-dev libxmlrpc-core-c3-dev libzmq3-dev
echo ""
sudo apt-get -q -y install munts-libsimpleio munts-expansion-lpc1114
echo ""

# Checkout source code

if [ ! -d $RPISRC ]; then
  git clone http://git.munts.com/rpi-mcu.git $RPISRC
  echo ""
fi

echo "If you wish to compile firmware for the LPC1114 microcontroller,"
echo "you must also run the following commands:"
echo ""
echo "sudo apt-get install arm-mcu-tools"
echo "git clone http://git.munts.com/arm-mcu.git"
echo ""
