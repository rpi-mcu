# Makefile for Raspberry Pi LPC1114 I/O Processor Expansion Board
# XML-RPC Java client program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Override the following macro to build out of tree

RASPBERRYPI_LPC1114_SRC ?= $(HOME)/rpi-mcu/expansion/LPC1114/src

JAVA_SRC		= $(RASPBERRYPI_LPC1114_SRC)/java

include $(JAVA_SRC)/include/java.mk

JAVAC_CLASSPATH		= .:$(JAVA_SRC)

JAR_COMPONENTS		+= SPIAgent org

default:
	jar xf $(JAVA_SRC)/lib/ws-commons-util-1.0.2.jar org/apache
	jar xf $(JAVA_SRC)/lib/xmlrpc-client-3.1.3.jar org/apache/xmlrpc
	jar xf $(JAVA_SRC)/lib/xmlrpc-common-3.1.3.jar org/apache/xmlrpc
	$(MAKE) spi_agent_xmlrpc_client.jar

clean: java_mk_clean
	rm -rf $(JAR_COMPONENTS)

reallyclean: clean

distclean: reallyclean
