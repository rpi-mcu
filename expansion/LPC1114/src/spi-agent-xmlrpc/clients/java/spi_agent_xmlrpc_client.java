// Java test client for the Raspberry Pi LPC1114 I/O Processor
// Expansion Board SPI agent firmware XML-RPC server

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import java.io.*;
import java.net.*;

import org.apache.xmlrpc.*;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import static SPIAgent.commands.*;
import static SPIAgent.pins.*;

public class spi_agent_xmlrpc_client
{
  public static void spiagent_command_xmlrpc(XmlRpcClient server, Integer cmd[], Integer resp[])
  {
    try
    {
      Object[] oresp = (Object[]) server.execute("spi.agent.transaction", cmd);
      resp[0] = (Integer) oresp[0];
      resp[1] = (Integer) oresp[1];
      resp[2] = (Integer) oresp[2];
      resp[3] = (Integer) oresp[3];
    }
    catch (XmlRpcException e)
    {
      System.out.println("ERROR: XML-RPC error: " + e.toString());
      System.exit(1);
    }
  }

  public static void main(String[] args)
  {
    String urlstring = new String();
    int iterations = 3000;
    XmlRpcClientConfigImpl config;
    XmlRpcClient server;
    Integer[] cmd = new Integer[3];
    Integer[] resp = new Integer[4];

    System.out.println("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

    // Build XML-RPC server URL

    switch (args.length)
    {
      case 0 :
        urlstring = "http://localhost:8080/RPC2";
        iterations = 3000;
        break;

      case 1 :
        urlstring = "http://" + args[0] + ":8080/RPC2";
        iterations = 3000;
        break;

      case 2 :
        urlstring = "http://" + args[0] + ":8080/RPC2";
        iterations = Integer.parseInt(args[1]);
        break;

      default :
        System.out.println("Usage: java -jar spi_agent_test_xmlrpc.jar [hostname] [iterations]");
        System.exit(1);
        break;
    }

    // Build XML-RPC client proxy object.  The client proxy object execute()
    // method accepts RPC parameters as Object[] and returns RPC results as
    // Object[] as well.

    config = new XmlRpcClientConfigImpl();

    try
    {
      config.setServerURL(new URL(urlstring));
    }
    catch (IOException e)
    {
      System.out.println("ERROR: I/O error: " + e.toString());
      System.exit(1);
    }

    // We call the XML-RPC client proxy object "server", because we use it as a
    // server handle

    server = new XmlRpcClient();
    server.setConfig(config);

    System.out.println("Issuing some SPI transactions...\n");

    cmd = new Integer[]{SPIAGENT_CMD_NOP.ordinal(), 0, 0};
    spiagent_command_xmlrpc(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd = new Integer[]{SPIAGENT_CMD_LOOPBACK.ordinal(), 2, 3};
    spiagent_command_xmlrpc(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd = new Integer[]{SPIAGENT_CMD_GET_GPIO.ordinal(), 99, 3};
    spiagent_command_xmlrpc(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd = new Integer[]{99, 2, 3};
    spiagent_command_xmlrpc(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd = new Integer[]{SPIAGENT_CMD_NOP.ordinal(), 0, 0};
    spiagent_command_xmlrpc(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("\nThe LPC1114 firmware version is " +
        String.format("%d", resp[2]));

    cmd = new Integer[]{SPIAGENT_CMD_GET_SFR.ordinal(), 0x400483F4, 0};
    spiagent_command_xmlrpc(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("The LPC1114 device ID is        " +
        String.format("%08X", resp[2]));

    cmd = new Integer[]{SPIAGENT_CMD_GET_GPIO.ordinal(), LPC1114_LED, 0};
    spiagent_command_xmlrpc(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("The expansion board LED is      " +
        ((resp[2] == 1) ? "ON" : "OFF"));

    long starttime = System.nanoTime();

    System.out.println("\nStarting " + Integer.toString(iterations) +
      " SPI agent loopback test transactions...\n");

    for (int i = 1; i <= iterations; i++)
    {
      cmd[0] = SPIAGENT_CMD_LOOPBACK.ordinal();
      cmd[1] = i*17;
      cmd[2] = i*19;
      spiagent_command_xmlrpc(server, cmd, resp);

      if ((resp[0].intValue() != cmd[0].intValue()) || (resp[1].intValue() != cmd[1].intValue()) ||
          (resp[2].intValue() != cmd[2].intValue()) || (resp[3].intValue() != 0))
      {
        System.out.println("Iteration:" + Integer.toString(i) + " Response: command:" +
        Integer.toString(resp[0]) + " pin:" + Integer.toString(resp[1]) + " data:" +
        Integer.toString(resp[2]) + " error:" + Integer.toString(resp[3]));
      }
    }

    long endtime = System.nanoTime();

    // Display statistics

    double deltat = (endtime - starttime)*1.0e-9;
    double rate = iterations/deltat;
    double cycletime = deltat/iterations;

    System.out.println("Performed " + Integer.toString(iterations) + " loopback tests in " +
      String.format("%1.1f", deltat) + " seconds");

    System.out.println("  " + String.format("%1.1f", rate) + " iterations per second");

    System.out.println("  " + String.format("%1.1f", cycletime*1.0E6) + " microseconds per iteration" + "\n");
  }
}
