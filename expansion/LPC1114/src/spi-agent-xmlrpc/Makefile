# Makefile for Raspberry Pi LPC1114 I/O Processor Expansion Board
# SPI Agent XMLRPC server program

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Override the following macro to build out of tree

RASPBERRYPI_LPC1114_SRC	?= $(HOME)/rpi-mcu/expansion/LPC1114/src

SOURCES		= spi_agent_xmlrpc_server.c

HOST_CC		= gcc
HOST_CFLAGS	+= -Wall -I$(RASPBERRYPI_LPC1114_SRC)/c/include
HOST_LDFLAGS	+= -L$(RASPBERRYPI_LPC1114_SRC)/spi-agent-lib
HOST_LDFLAGS	+= -lpthread -lxmlrpc -lxmlrpc_abyss -lxmlrpc_server
HOST_LDFLAGS	+= -lxmlrpc_server_abyss -lxmlrpc_util
HOST_LDFLAGS	+= -lsimpleio -lspiagent

default: spi_agent_xmlrpc_server

###############################################################################

# Compile the server program

spi_agent_xmlrpc_server: $(SOURCES)
	$(HOST_CC) $(HOST_CFLAGS) -o $@ $^ $(HOST_LDFLAGS)

###############################################################################

# Install the server program

install: spi_agent_xmlrpc_server
	install -csm 0555 $^ /usr/local/libexec

###############################################################################

# Remove working files

clean:
	-rm -f spi_agent_xmlrpc_server
