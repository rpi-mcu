; InnoSetup installer script for the LabView LINX Test Clients

; Copyright (C)2016-2018, Philip Munts, President, Munts AM Corp.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;
; * Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

[Setup]
AppName=LabView LPC1114 SPI Agent Tests using LINX
AppVersion={#APPVERSION}
AppPublisher=Munts Technologies
AppPublisherURL=http://tech.munts.com
DefaultDirName={pf}\Labview-LPC1114-SPI-Agent-Tests-LINX
DefaultGroupName={#SetupSetting("AppName")}
DirExistsWarning=no
Compression=lzma2
SolidCompression=yes
OutputBaseFilename=LabView-LPC1114-SPI-Agent-Tests-LINX-{#APPVERSION}-setup
OutputDir=.
UninstallFilesDir={app}/LabView-LPC1114-SPI-Agent-Tests-LINX-uninstall

[Files]
Source: "*.exe"; DestDir: "{app}"
Source: "*.pdf"; DestDir: "{app}"

[Icons]
Name: "{group}\GPIO Read Test";			Filename: "{app}\test_gpio_read.exe";		WorkingDir: "{app}"
Name: "{group}\GPIO Read Test.pdf";		Filename: "{app}\test_gpio_read.pdf";		WorkingDir: "{app}"
Name: "{group}\GPIO Write Test";		Filename: "{app}\test_gpio_write.exe";		WorkingDir: "{app}"
Name: "{group}\GPIO Write Test.pdf";		Filename: "{app}\test_gpio_write.pdf";		WorkingDir: "{app}"
Name: "{group}\Servo Test";			Filename: "{app}\test_servo.exe";		WorkingDir: "{app}"
Name: "{group}\Servo Test.pdf";			Filename: "{app}\test_servo.pdf";		WorkingDir: "{app}"
Name: "{group}\Firmware Version Test";		Filename: "{app}\test_version.exe";		WorkingDir: "{app}"
Name: "{group}\Firmware Version Test.pdf";	Filename: "{app}\test_version.pdf";		WorkingDir: "{app}"
