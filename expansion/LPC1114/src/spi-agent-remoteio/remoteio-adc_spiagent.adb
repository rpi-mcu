-- Remote I/O Server Dispatcher for Raspberry Pi LPC1114 I/O Processor
-- Expansion Board A/D converter commands

-- Copyright (C)2019-2021, Philip Munts, President, Munts AM Corp.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

WITH errno;
WITH Logging.libsimpleio;
WITH Messaging;
WITH Message64;
WITH RemoteIO.Dispatch;
WITH RemoteIO.Executive;

WITH Interfaces;
WITH SPI_Agent.Commands;
WITH SPI_Agent.Messages;
WITH SPI_Agent.Pins;

USE TYPE Interfaces.Integer_32;
USE TYPE Interfaces.Unsigned_32;
USE TYPE Messaging.Byte;

PACKAGE BODY RemoteIO.ADC_SPIAgent IS

  -- Minimal binding to libspiagent.so

  PRAGMA Link_With("-lspiagent");

  PROCEDURE open
   (server : String;
    error  : OUT Interfaces.Integer_32);

  PRAGMA Import(C, open, "spiagent_open");

  PROCEDURE command
   (command  : IN  SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    response : OUT SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error    : OUT Interfaces.Integer_32);

  PRAGMA Import(C, command, "spiagent_command");

  -- The LPC1114 A/D converter resolution is 10 bits

  Resolution : CONSTANT := 10;

  -- Query available LPC1114 A/D converter inputs

  PROCEDURE Present
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    byteindex : Natural;
    bitmask   : Messaging.Byte;

  BEGIN
    resp(0) := MessageTypes'Pos(ADC_PRESENT_RESPONSE);
    resp(1) := cmd(1);
    resp(2 .. 63) := (OTHERS => 0);

    FOR c IN ChannelNumber LOOP
      byteindex  := c/8;
      bitmask    := 2**(7 - (c MOD 8));

      IF Self.inputs(c).registered THEN
        resp(3 + byteindex) := resp(3 + byteindex) OR bitmask;
      END IF;
    END LOOP;
  END Present;

  -- Configure an LPC1114 A/D converter input

  PROCEDURE Configure
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    num   : RemoteIO.ChannelNumber;
    scmd  : SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    sresp : SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error : Interfaces.Integer_32;

  BEGIN
    resp(0) := MessageTypes'Pos(ADC_CONFIGURE_RESPONSE);
    resp(1) := cmd(1);
    resp(2 .. 63) := (OTHERS => 0);

    num := RemoteIO.ChannelNumber(cmd(2));

    IF NOT Self.inputs(num).registered THEN
      resp(2) := errno.ENODEV;
    END IF;

    IF Self.inputs(num).configured THEN
      resp(3) := Resolution;
      RETURN;
    END IF;

    scmd.Command :=
      Interfaces.Unsigned_32(SPI_Agent.Commands.SPIAGENT_COMMAND_t'Pos(
        SPI_Agent.Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT));

    scmd.Pin     :=
      Interfaces.Unsigned_32(LPC11xx.pin_id_t'Pos(Self.inputs(num).pin));

    scmd.Data    := 0;

    command(scmd, sresp, error);

    IF error /= 0 THEN
      resp(2) := Messaging.Byte(error);
      RETURN;
    END IF;

    IF sresp.Error /= 0 THEN
      resp(2) := Messaging.Byte(sresp.Error);
      RETURN;
    END IF;

    Self.inputs(num).configured := True;
    resp(3) := Resolution;
  END;

  -- Read an LPC1114 A/D converter input

  PROCEDURE Read
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    num   : RemoteIO.ChannelNumber;
    scmd  : SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    sresp : SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error : Interfaces.Integer_32;

  BEGIN
    resp(0) := MessageTypes'Pos(ADC_READ_RESPONSE);
    resp(1) := cmd(1);
    resp(2) := 0;
    resp(3 .. 63) := (OTHERS => 0);

    num := RemoteIO.ChannelNumber(cmd(2));

    IF NOT Self.inputs(num).registered THEN
      resp(2) := errno.ENODEV;
      RETURN;
    END IF;

    IF NOT Self.inputs(num).configured THEN
      resp(2) := errno.EIO;
    END IF;

    scmd.Command :=
      Interfaces.Unsigned_32(SPI_Agent.Commands.SPIAGENT_COMMAND_t'Pos(
        SPI_Agent.Commands.SPIAGENT_CMD_GET_ANALOG));

    scmd.Pin     :=
      Interfaces.Unsigned_32(LPC11xx.pin_id_t'Pos(Self.inputs(num).pin));

    scmd.Data    := 0;

    command(scmd, sresp, error);

    IF error /= 0 THEN
      resp(2) := Messaging.Byte(error);
      RETURN;
    END IF;

    IF sresp.Error /= 0 THEN
      resp(2) := Messaging.Byte(sresp.Error);
      RETURN;
    END IF;

    resp(3) := Messaging.Byte(sresp.Data / 16777216);
    resp(4) := Messaging.Byte(sresp.Data / 65536 MOD 256);
    resp(5) := Messaging.Byte(sresp.Data / 256 MOD 256);
    resp(6) := Messaging.Byte(sresp.Data MOD 256);
  END Read;

  -- Create Remote I/O Protocol command dispatcher object

  FUNCTION Create
   (executor : IN OUT RemoteIO.Executive.Executor) RETURN Dispatcher IS

    Self : Dispatcher;

  BEGIN
    Self := NEW DispatcherSubclass'(inputs => (OTHERS => Unused));

    executor.Register(ADC_PRESENT_REQUEST,   RemoteIO.Dispatch.Dispatcher(Self));
    executor.Register(ADC_CONFIGURE_REQUEST, RemoteIO.Dispatch.Dispatcher(Self));
    executor.Register(ADC_READ_REQUEST,      RemoteIO.Dispatch.Dispatcher(Self));

    RETURN Self;
  END Create;

  -- Dispatch Remote I/O Protocol commands

  PROCEDURE Dispatch
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.Message) IS

    msgtype : MessageTypes;

  BEGIN
    msgtype := MessageTypes'Val(cmd(0));

    CASE msgtype IS
      WHEN ADC_PRESENT_REQUEST =>
        Present(Self, cmd, resp);

      WHEN ADC_CONFIGURE_REQUEST =>
        Configure(Self, cmd, resp);

      WHEN ADC_Read_Request =>
        Read(Self, cmd, resp);

      WHEN OTHERS =>
        Logging.libsimpleio.Error("Unexected message type: " &
          MessageTypes'Image(msgtype));
    END CASE;
  END Dispatch;

  -- Register analog input

  PROCEDURE Register
   (Self : IN OUT DispatcherSubclass;
    num  : ChannelNumber;
    pin  : LPC11xx.pin_id_t) IS

  BEGIN
    IF Self.inputs(num).registered THEN
      RETURN;
    END IF;

    Self.inputs(num) := InputRec'(pin, True, False);
  END Register;

  error : Interfaces.Integer_32;

BEGIN
  open("ioctl://localhost", error);

  IF (error /= 0) AND (error /= errno.EBUSY) THEN
    RAISE Program_Error WITH "Cannot initialize libsimpleio";
  END IF;
END RemoteIO.ADC_SPIAgent;
