-- Remote I/O Server for the Raspberry Pi LPC1114 I/O Processor Expansion Board

-- Copyright (C)2019-2021, Philip Munts, President, Munts AM Corp.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

WITH Interfaces;
WITH RemoteIO.ADC_SPIAgent;
WITH RemoteIO.Device_SPIAgent;
WITH RemoteIO.Executive;
WITH RemoteIO.GPIO_SPIAgent;
WITH RemoteIO.PWM_SPIAgent;
WITH RemoteIO.Server.Dev;
WITH RemoteIO.Server.Foundation;
WITH RemoteIO.Server.Serial;
WITH RemoteIO.Server.UDP;
WITH RemoteIO.Server.ZeroMQ;
WITH SPI_Agent.Commands;
WITH SPI_Agent.Messages;
WITH SPI_Agent.Pins;

PROCEDURE spi_agent_remoteio_server IS

  title  : CONSTANT String := "SPI Agent Remote I/O Server";
  caps   : CONSTANT String := "ADC GPIO PWM DEVICE";
  exec   : RemoteIO.Executive.Executor;
  srvh   : RemoteIO.Server.Instance;
  srvs   : RemoteIO.Server.Instance;
  srvu   : RemoteIO.Server.Instance;
  srvz   : RemoteIO.Server.Instance;
  adc    : RemoteIO.ADC_SPIAgent.Dispatcher;
  dev    : RemoteIO.Device_SPIAgent.Dispatcher;
  gpio   : RemoteIO.GPIO_SPIAgent.Dispatcher;
  pwm    : RemoteIO.PWM_SPIAgent.Dispatcher;

BEGIN
  RemoteIO.Server.Foundation.Initialize(title, caps);
  exec   := RemoteIO.Server.Foundation.Executor;

  -- Initialize server subsystem tasks

  srvh := RemoteIO.Server.Dev.Create(exec, "Raw HID", "/dev/hidg0");
  srvs := RemoteIO.Server.Serial.Create(exec, "Serial Port", "/dev/ttyGS0");
  srvu := RemoteIO.Server.UDP.Create(exec, "UDP");
  srvz := RemoteIO.Server.ZeroMQ.Create(exec, "ZeroMQ");

  -- Create I/O subsystem objects

  adc  := RemoteIO.ADC_SPIAgent.Create(exec);
  dev  := RemoteIO.Device_SPIAgent.Create(exec);
  gpio := RemoteIO.GPIO_SPIAgent.Create(exec);
  pwm  := RemoteIO.PWM_SPIAgent.Create(exec);

  -- Register I/O resources

  adc.Register(1, SPI_Agent.Pins.LPC1114_AD1);
  adc.Register(2, SPI_Agent.Pins.LPC1114_AD2);
  adc.Register(3, SPI_Agent.Pins.LPC1114_AD3);
  adc.Register(4, SPI_Agent.Pins.LPC1114_AD4);
  adc.Register(5, SPI_Agent.Pins.LPC1114_AD5);

  gpio.Register(0, SPI_Agent.Pins.LPC1114_LED);
  gpio.Register(1, SPI_Agent.Pins.LPC1114_GPIO0);
  gpio.Register(2, SPI_Agent.Pins.LPC1114_GPIO1);
  gpio.Register(3, SPI_Agent.Pins.LPC1114_GPIO2);
  gpio.Register(4, SPI_Agent.Pins.LPC1114_GPIO3);
  gpio.Register(5, SPI_Agent.Pins.LPC1114_GPIO4);
  gpio.Register(6, SPI_Agent.Pins.LPC1114_GPIO5);
  gpio.Register(7, SPI_Agent.Pins.LPC1114_GPIO6);
  gpio.Register(8, SPI_Agent.Pins.LPC1114_GPIO7);

  pwm.Register(1, SPI_Agent.Pins.LPC1114_PWM1);
  pwm.Register(2, SPI_Agent.Pins.LPC1114_PWM2);
  pwm.Register(3, SPI_Agent.Pins.LPC1114_PWM3);
  pwm.Register(4, SPI_Agent.Pins.LPC1114_PWM4);

END spi_agent_remoteio_server;
