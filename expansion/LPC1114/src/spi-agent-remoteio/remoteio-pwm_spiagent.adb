-- Remote I/O Server Dispatcher for Raspberry Pi LPC1114 I/O Processor
-- Expansion Board PWM output commands

-- Copyright (C)2019-2021, Philip Munts, President, Munts AM Corp.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

WITH Interfaces;
WITH Logging.libsimpleio;
WITH Message64;
WITH Messaging;
WITH RemoteIO.Dispatch;
WITH RemoteIO.Executive;
WITH SPI_Agent.Commands;
WITH SPI_Agent.Messages;
WITH SPI_Agent.Pins;
WITH errno;

USE TYPE Interfaces.Integer_32;
USE TYPE Interfaces.Unsigned_32;
USE TYPE Messaging.Byte;

PACKAGE BODY RemoteIO.PWM_SPIAgent IS

  -- Minimal binding to libspiagent.so

  PRAGMA Link_With("-lspiagent");

  PROCEDURE open
   (server : String;
    error  : OUT Interfaces.Unsigned_32);

  PRAGMA Import(C, open, "spiagent_open");

  PROCEDURE command
   (command  : IN  SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    response : OUT SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error    : OUT Interfaces.Integer_32);

  PRAGMA Import(C, command, "spiagent_command");

  -- Query available LPC1114 PWM outputs

  PROCEDURE Present
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    byteindex : Natural;
    bitmask   : Messaging.Byte;

  BEGIN
    resp(0) := MessageTypes'Pos(PWM_PRESENT_RESPONSE);
    resp(1) := cmd(1);
    resp(2 .. 63) := (OTHERS => 0);

    FOR c IN ChannelNumber LOOP
      byteindex  := c/8;
      bitmask    := 2**(7 - (c MOD 8));

      IF Self.outputs(c).registered THEN
        resp(3 + byteindex) := resp(3 + byteindex) OR bitmask;
      END IF;
    END LOOP;
  END Present;

  -- Configure an LPC1114 PWM output

  PROCEDURE Configure
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    num    : Natural;
    period : Positive;
    freq   : Positive;
    scmd   : SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    sresp  : SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error  : Interfaces.Integer_32;

  BEGIN
    resp(0) := MessageTypes'Pos(PWM_CONFIGURE_RESPONSE);
    resp(1) := cmd(1);
    resp(2 .. 63) := (OTHERS => 0);

    -- Extract the channel number parameter

    num := RemoteIO.ChannelNumber(cmd(2));

    IF num >= RemoteIO.ChannelNumber'Last THEN
      resp(2) := errno.EINVAL;
      RETURN;
    END IF;

    IF NOT Self.outputs(num).registered THEN
      resp(2) := errno.ENODEV;
      RETURN;
    END IF;

    -- Extract the PWM period in nanoseconds parameter

    period :=
      Natural(cmd(3))*16777216 +
      Natural(cmd(4))*65536 +
      Natural(cmd(5))*256 +
      Natural(cmd(6));

    freq := 1000000000/period;

    -- Build SPIAgent command message

    scmd.Command :=
      Interfaces.Unsigned_32(SPI_Agent.Commands.SPIAGENT_COMMAND_t'Pos(
        SPI_Agent.Commands.SPIAGENT_CMD_CONFIGURE_PWM_OUTPUT));

    scmd.Pin     :=
      Interfaces.Unsigned_32(LPC11xx.pin_id_t'Pos(Self.outputs(num).pin));

    scmd.Data    := Interfaces.Unsigned_32(Freq);

    -- Dispatch SPIAgent command message

    command(scmd, sresp, error);

    -- Handle errors

    IF error /= 0 THEN
      resp(2) := Messaging.Byte(error);
      RETURN;
    END IF;

    IF sresp.Error /= 0 THEN
      resp(2) := Messaging.Byte(sresp.Error);
      RETURN;
    END IF;

    Self.outputs(num).period     := period;
    Self.outputs(num).configured := True;
  END;

  -- Write to an LPC1114 PWM output

  PROCEDURE Write
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.message) IS

    num    : Natural;
    ontime : Interfaces.Unsigned_32;
    scmd   : SPI_Agent.Messages.SPIAGENT_COMMAND_MSG_t;
    sresp  : SPI_Agent.Messages.SPIAGENT_RESPONSE_MSG_t;
    error  : Interfaces.Integer_32;

  BEGIN
    resp(0) := MessageTypes'Pos(PWM_WRITE_RESPONSE);
    resp(1) := cmd(1);
    resp(2 .. 63) := (OTHERS => 0);

    -- Extract the channel number parameter

    num := RemoteIO.ChannelNumber(cmd(2));

    IF num >= RemoteIO.ChannelNumber'Last THEN
      resp(2) := errno.EINVAL;
      RETURN;
    END IF;

    IF NOT Self.outputs(num).registered THEN
      resp(2) := errno.ENODEV;
      RETURN;
    END IF;

    IF NOT Self.outputs(num).configured THEN
      resp(2) := errno.ENODEV;
    END IF;

    -- Extract the on time in nanoseconds parameter

    ontime :=
      Interfaces.Unsigned_32(cmd(3))*16777216 +
      Interfaces.Unsigned_32(cmd(4))*65536 +
      Interfaces.Unsigned_32(cmd(5))*256 +
      Interfaces.Unsigned_32(cmd(6));

    -- Build SPIAgent command message

    scmd.Command :=
      Interfaces.Unsigned_32(SPI_Agent.Commands.SPIAGENT_COMMAND_t'Pos(
        SPI_Agent.Commands.SPIAGENT_CMD_PUT_PWM));

    scmd.Pin     :=
      Interfaces.Unsigned_32(LPC11xx.pin_id_t'Pos(Self.outputs(num).pin));

    scmd.Data    :=
      Interfaces.Unsigned_32(Float(ontime)/Float(Self.outputs(num).period)*65535.0);

    -- Dispatch SPIAgent command message

    command(scmd, sresp, error);

    -- Handle errors

    IF error /= 0 THEN
      resp(2) := Messaging.Byte(error);
      RETURN;
    END IF;

    IF sresp.Error /= 0 THEN
      resp(2) := Messaging.Byte(sresp.Error);
      RETURN;
    END IF;
  END Write;

  -- Create Remote I/O Protocol command dispatcher object

  FUNCTION Create
   (executor : IN OUT RemoteIO.Executive.Executor) RETURN Dispatcher IS

    Self : Dispatcher;

  BEGIN
    Self := NEW DispatcherSubclass'(outputs => (OTHERS => Unused));

    executor.Register(PWM_PRESENT_REQUEST,   RemoteIO.Dispatch.Dispatcher(Self));
    executor.Register(PWM_CONFIGURE_REQUEST, RemoteIO.Dispatch.Dispatcher(Self));
    executor.Register(PWM_WRITE_REQUEST,     RemoteIO.Dispatch.Dispatcher(Self));

    RETURN Self;
  END Create;

  -- Dispatch Remote I/O Protocol commands

  PROCEDURE Dispatch
   (Self : IN OUT DispatcherSubclass;
    cmd  : Message64.Message;
    resp : OUT Message64.Message) IS

    msgtype : MessageTypes;

  BEGIN
    msgtype := MessageTypes'Val(cmd(0));

    CASE msgtype IS
      WHEN PWM_PRESENT_REQUEST =>
        Present(Self, cmd, resp);

      WHEN PWM_CONFIGURE_REQUEST =>
        Configure(Self, cmd, resp);

      WHEN PWM_WRITE_REQUEST =>
        Write(Self, cmd, resp);

      WHEN OTHERS =>
        Logging.libsimpleio.Error("Unexected message type: " &
          MessageTypes'Image(msgtype));
    END CASE;
  END Dispatch;

  -- Register an LPC1114 PWM output

  PROCEDURE Register
   (Self : IN OUT DispatcherSubclass;
    num  : ChannelNumber;
    pin  : LPC11xx.pin_id_t) IS

  BEGIN
    IF Self.outputs(num).registered THEN
      RETURN;
    END IF;

    Self.outputs(num) := OutputRec'(pin, 0, True, False);
  END Register;

  error : Interfaces.Unsigned_32;

BEGIN
  open("ioctl://localhost", error);

  IF (error /= 0) AND (error /= errno.EBUSY) THEN
    RAISE Program_Error WITH "Cannot initialize libsimpleio";
  END IF;
END RemoteIO.PWM_SPIAgent;
