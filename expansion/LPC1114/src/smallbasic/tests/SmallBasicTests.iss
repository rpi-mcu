; InnoSetup installer script for the Raspberry Pi LPC1114 I/O Processor
; Expansion Board SPI Agent Firmware LED test

; Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;
; * Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

[Setup]
AppName=Raspberry Pi LPC1114 SPI Agent Firmware Small Basic Tests
AppVersion={#APPVERSION}
AppPublisher=Munts Technologies
AppPublisherURL=http://tech.munts.com
DefaultDirName={pf}\SPIAgent-SmallBasic-Tests
DefaultGroupName={#SetupSetting("AppName")}
DirExistsWarning=no
Compression=lzma2
SolidCompression=yes
InfoBeforeFile=infobefore.txt
OutputBaseFileName=SPIAgent-SmallBasic-Tests-{#APPVERSION}
outputDir=.
UninstallFilesDir={app}/uninstall

[Files]
Source: "SmallBasicLibrary.dll";	DestDir: "{app}"
Source: "LitDev.dll";			DestDir: "{app}"
Source: "SPIAgentExtension*.dll";	DestDir: "{app}"
Source: "*.exe";			DestDir: "{app}"

[Icons]
Name: "{group}\Analog Input Test";	Filename: "{app}\test_adc.exe";		WorkingDir: "{app}"
Name: "{group}\LED Test";		Filename: "{app}\test_led.exe";		WorkingDir: "{app}"
Name: "{group}\Performance Test";	Filename: "{app}\spi_agent_test.exe";	WorkingDir: "{app}"
Name: "{group}\PWM Output Test";	Filename: "{app}\test_pwm.exe";		WorkingDir: "{app}"
Name: "{group}\Servo Output Test";	Filename: "{app}\test_servo.exe";	WorkingDir: "{app}"
Name: "{group}\SFR Test";		Filename: "{app}\test_sfr.exe";		WorkingDir: "{app}"
Name: "{group}\Version Test";		Filename: "{app}\test_version.exe";	WorkingDir: "{app}"
