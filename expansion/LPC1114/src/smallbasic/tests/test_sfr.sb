' Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
' Special Function Register test program

' Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
'
' Redistribution and use in source and binary forms, with or without
' modification, are permitted provided that the following conditions are met:
'
' * Redistributions of source code must retain the above copyright notice,
'   this list of conditions and the following disclaimer.
'
' THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
' IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
' ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
' LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
' CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
' SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
' INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
' CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
' ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
' POSSIBILITY OF SUCH DAMAGE.

' Layout the GUI

IsTablet = LDFile.Exists("C:\ProgramData\MuntsTechnologies\tablet.txt")

GraphicsWindow.Title = "Raspberry Pi LPC1114 SFR Test"
GraphicsWindow.BrushColor = "black"

If IsTablet Then
  ' Layout the GUI for Windows tablet

  LDUtilities.GWState = 2
  W = GraphicsWindow.Width
  H = GraphicsWindow.Height

  GraphicsWindow.FontSize = 32
  GraphicsWindow.DrawText(W/2 - 300,  50, "Raspberry Pi LPC1114 I/O Processor")
  GraphicsWindow.DrawText(W/2 - 300, 100, "Expansion Board SPI Agent Firmware")
  GraphicsWindow.DrawText(W/2 - 300, 150, "Special Function Register Test")
  GraphicsWindow.FontSize = 30
  GraphicsWindow.DrawText(W/2 - 270, 250, "Server name:")
  TextBoxServerName = Controls.AddTextBox(W/2 - 60, 250)
  ButtonConnect = Controls.AddButton("Connect", W/2 + 120, 250)
  SFRLabelAddr = Shapes.AddText("SFR Addr:")
  SFRLabelData = Shapes.AddText("SFR Data:")
  Controls.Move(SFRLabelAddr, W/2 - 300, 350)
  Controls.Move(SFRLabelData, W/2 - 300, 420)
  SFRTextBoxAddr = Controls.AddTextBox(W/2 - 130, 350)
  SFRTextBoxData = Controls.AddTextBox(W/2 - 130, 420)
  Controls.SetSize(SFRTextBoxAddr, 300, 50)
  Controls.SetSize(SFRTextBoxData, 300, 50)
  ButtonRead = Controls.AddButton("Read ", W/2 + 190, 350)
  ButtonWrite = Controls.AddButton("Write", W/2 + 190, 420)
  Controls.HideControl(SFRLabelAddr)
  Controls.HideControl(SFRLabelData)
  Controls.HideControl(SFRTextBoxAddr)
  Controls.HideControl(SFRTextBoxData)
  Controls.HideControl(ButtonRead)
  Controls.HideControl(ButtonWrite)
Else
  ' Layout the GUI for Windows Desktop

  GraphicsWindow.Width = 400
  GraphicsWindow.Height = 210
  GraphicsWindow.FontSize = 18
  GraphicsWindow.DrawText(30, 10, "Raspberry Pi LPC1114 I/O Processor")
  GraphicsWindow.DrawText(30, 30, "Expansion Board SPI Agent Firmware")
  GraphicsWindow.DrawText(30, 50, "Special Function Register Test")
  GraphicsWindow.FontSize = 14
  GraphicsWindow.DrawText(30, 84, "Server name:")
  TextBoxServerName = Controls.AddTextBox(130, 80)
  ButtonConnect = Controls.AddButton("Connect", 300, 78)
  SFRLabelAddr = Shapes.AddText("SFR Addr:")
  SFRLabelData = Shapes.AddText("SFR Data:")
  Controls.Move(SFRLabelAddr, 50, 129)
  Controls.Move(SFRLabelData, 50, 169)
  SFRTextBoxAddr = Controls.AddTextBox(130, 130)
  SFRTextBoxData = Controls.AddTextBox(130, 170)
  ButtonRead = Controls.AddButton("Read ", 300, 128)
  ButtonWrite = Controls.AddButton("Write", 300, 168)
  Controls.HideControl(SFRLabelAddr)
  Controls.HideControl(SFRLabelData)
  Controls.HideControl(SFRTextBoxAddr)
  Controls.HideControl(SFRTextBoxData)
  Controls.HideControl(ButtonRead)
  Controls.HideControl(ButtonWrite)
  GraphicsWindow.Show()
Endif

' Preload server name from command line argument

If Program.ArgumentCount = 1 Then
  Controls.SetTextBoxText(TextBoxServerName, Program.GetArgument(1))
Endif

' Register the button click event handler

Controls.ButtonClicked = ButtonHandler

' This subroutine handles button presses

Sub ButtonHandler
  c = Controls.GetButtonCaption(Controls.LastClickedButton)

  ' Connect button pressed

  If c = "Connect" Then
    SPIAgent.Open(Controls.GetTextBoxText(TextBoxServerName))
    If SPIAgent.error <> 0 Then
      GraphicsWindow.ShowMessage("SPIAgent.Open() returned error " + SPIAgent.error, "ERROR:")
      Goto ButtonHandlerDone
    Endif

    Controls.ShowControl(SFRLabelAddr)
    Controls.ShowControl(SFRLabelData)
    Controls.ShowControl(SFRTextBoxAddr)
    Controls.ShowControl(SFRTextBoxData)
    Controls.ShowControl(ButtonRead)
    Controls.ShowControl(ButtonWrite)
    Controls.SetButtonCaption(ButtonConnect, "Disconnect")

  ' Disconnect button pressed

  ElseIf c = "Disconnect" Then
    SPIAgent.Close()
    If SPIAgent.error <> 0 Then
      GraphicsWindow.ShowMessage("SPIAgent.Close() returned error " + SPIAgent.error, "ERROR:")
      Goto ButtonHandlerDone
    Endif

    Controls.HideControl(SFRLabelAddr)
    Controls.HideControl(SFRLabelData)
    Controls.HideControl(SFRTextBoxAddr)
    Controls.HideControl(SFRTextBoxData)
    Controls.HideControl(ButtonRead)
    Controls.HideControl(ButtonWrite)
    Controls.SetButtonCaption(ButtonConnect, "Connect")

  ' Read button pressed

  ElseIF c = "Read " Then
    SPIAgent.Command("GET_SFR", Controls.GetTextBoxText(SFRTextBoxAddr), 0)
    If SPIAgent.error <> 0 Then
      Controls.SetTextBoxText(SFRTextBoxData, "")
      GraphicsWindow.ShowMessage("SPIAgent.Command() returned error " + SPIAgent.error, "ERROR:")
      Goto ButtonHandlerDone
    Endif

    Controls.SetTextBoxText(SFRTextBoxData, SPIAgent.datahex)

  ' Write button pressed

  ElseIF c = "Write" Then
    SPIAgent.Command("PUT_SFR", Controls.GetTextBoxText(SFRTextBoxAddr), Controls.GetTextBoxText(SFRTextBoxData))
    If SPIAgent.error <> 0 Then
      GraphicsWindow.ShowMessage("SPIAgent.Command() returned error " + SPIAgent.error, "ERROR:")
      Goto ButtonHandlerDone
    Endif
  EndIf

  ButtonHandlerDone:
EndSub
