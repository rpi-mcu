# Generic Makefile for building Small Basic applications

# Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Override the following macro to build out of tree

RASPBERRYPI_LPC1114_SRC	?= $(HOME)/rpi-mcu/expansion/LPC1114/src
SMALLBASICSRC		?= $(RASPBERRYPI_LPC1114_SRC)/smallbasic

TARGETS			= spi_agent_test.exe test_adc.exe test_led.exe
TARGETS			+= test_pwm.exe test_servo.exe test_sfr.exe
TARGETS			+= test_version.exe

APPVERSION		= 1.0

RESHACK			?= $(PROGRAMFILES)/Resource Hacker/ResourceHacker.exe
INNOSETUPCOMPILER	?= $(PROGRAMFILES)/Inno Setup 5/iscc.exe
INNOSETUPFLAGS		= /dAPPVERSION=$(APPVERSION)

# Compile test programs

default: $(TARGETS)

# Build installer

installer: $(TARGETS)
	for p in $^ ; do "$(RESHACK)" -add $$p,$$p,board.ico,icongroup,1, ; done
	"$(INNOSETUPCOMPILER)" $(INNOSETUPFLAGS) SmallBasicTests.iss

# Release installer

release: installer
	cp SPIAgent-SmallBasic-Tests*.exe prebuilt
	$(MAKE) clean

# Remove working files

clean: smallbasic_mk_clean

# Include subordinate makefiles

include $(SMALLBASICSRC)/SmallBasic.mk
