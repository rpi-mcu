using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Small Basic Extension for the Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware")]
[assembly: AssemblyDescription("Microsoft Small Basic 1.1 extension library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Munts Technologies")]
[assembly: AssemblyProduct("SPIAgentExtension")]
[assembly: AssemblyCopyright("Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("53d66405-e49b-4c51-ba8b-f59e094fd49e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.*")]
