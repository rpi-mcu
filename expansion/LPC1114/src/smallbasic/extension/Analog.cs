// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent Firmware Extension for Microsoft Small Basic

// Analog to Digital Converter input services

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.SmallBasic.Library;
using System;

namespace SPIAgent
{
    public static partial class SPIAgent
    {
        // Private state variables
        private static bool AD1_configured = false;
        private static bool AD2_configured = false;
        private static bool AD3_configured = false;
        private static bool AD4_configured = false;
        private static bool AD5_configured = false;

        private static void AnalogInitialize()
        {
            AD1_configured = false;
            AD2_configured = false;
            AD3_configured = false;
            AD4_configured = false;
            AD5_configured = false;
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board analog input AD1 (GPIO0 aka LPC1114 PIO1_0).
        /// The analog input range is 0.0-3.3V.
        /// WARNING: LPC1114 pins configured as analog inputs are NOT 5V tolerant!  Do not exceed 3.3V at any analog input!
        /// </summary>
        public static Primitive AD1
        {
            get
            {
                if (!AD1_configured)
                {
                    CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT, Pins.LPC1114_AD1, 0);
                    if (response_error != (int)errno.EOK) return 0.0F;
                    AD1_configured = true;
                }

                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_ANALOG, Pins.LPC1114_AD1, 0);
                if (response_error != (int)errno.EOK) return 0.0F;
                return response_data * 0.00322265625F;
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board analog input AD2 (GPIO1 aka LPC1114 PIO1_1).
        /// The analog input range is 0.0-3.3V.
        /// WARNING: LPC1114 pins configured as analog inputs are NOT 5V tolerant!  Do not exceed 3.3V at any analog input!
        /// </summary>
        public static Primitive AD2
        {
            get
            {
                if (!AD2_configured)
                {
                    CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT, Pins.LPC1114_AD2, 0);
                    if (response_error != (int)errno.EOK) return 0.0F;
                    AD2_configured = true;
                }

                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_ANALOG, Pins.LPC1114_AD2, 0);
                if (response_error != (int)errno.EOK) return 0.0F;
                return response_data * 0.00322265625F;
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board analog input AD3 (GPIO2 aka LPC1114 PIO1_2).
        /// The analog input range is 0.0-3.3V.
        /// WARNING: LPC1114 pins configured as analog inputs are NOT 5V tolerant!  Do not exceed 3.3V at any analog input!
        /// </summary>
        public static Primitive AD3
        {
            get
            {
                if (!AD3_configured)
                {
                    CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT, Pins.LPC1114_AD3, 0);
                    if (response_error != (int)errno.EOK) return 0.0F;
                    AD3_configured = true;
                }

                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_ANALOG, Pins.LPC1114_AD3, 0);
                if (response_error != (int)errno.EOK) return 0.0F;
                return response_data * 0.00322265625F;
            }
        }
        /// <summary>
        /// LPC1114 I/O Processor Expansion Board analog input AD4 (GPIO3 aka LPC1114 PIO1_3).
        /// The analog input range is 0.0-3.3V.
        /// WARNING: LPC1114 pins configured as analog inputs are NOT 5V tolerant!  Do not exceed 3.3V at any analog input!
        /// </summary>
        public static Primitive AD4
        {
            get
            {
                if (!AD4_configured)
                {
                    CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT, Pins.LPC1114_AD4, 0);
                    if (response_error != (int)errno.EOK) return 0.0F;
                    AD4_configured = true;
                }

                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_ANALOG, Pins.LPC1114_AD4, 0);
                if (response_error != (int)errno.EOK) return 0.0F;
                return response_data * 0.00322265625F;
            }
        }
        /// <summary>
        /// LPC1114 I/O Processor Expansion Board analog input AD5 (GPIO4 aka LPC1114 PIO1_4).
        /// The analog input range is 0.0-3.3V.
        /// WARNING: LPC1114 pins configured as analog inputs are NOT 5V tolerant!  Do not exceed 3.3V at any analog input!
        /// </summary>
        public static Primitive AD5
        {
            get
            {
                if (!AD5_configured)
                {
                    CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT, Pins.LPC1114_AD5, 0);
                    if (response_error != (int)errno.EOK) return 0.0F;
                    AD5_configured = true;
                }

                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_ANALOG, Pins.LPC1114_AD5, 0);
                if (response_error != (int)errno.EOK) return 0.0F;
                return response_data * 0.00322265625F;
            }
        }
    }
}
