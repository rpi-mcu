// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent Firmware Extension for Microsoft Small Basic

// General Purpose Input Output services

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.SmallBasic.Library;
using System;

namespace SPIAgent
{
    public static partial class SPIAgent
    {
        /// <summary>
        /// LPC1114 I/O Processor Expansion Board LED (aka LPC1114 PIO0_7)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive LED
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_LED, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_LED, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO0 (aka LPC1114 PIO1_0)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO0
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO0, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO0, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO1 (aka LPC1114 PIO1_1)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO1
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO1, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO1, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO2 (aka LPC1114 PIO1_2)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO2
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO2, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO2, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO3 (aka LPC1114 PIO1_3)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO3
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO3, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO3, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO4 (aka LPC1114 PIO1_4)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO4
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO4, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO4, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO5 (aka LPC1114 PIO1_5)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO5
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO5, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO5, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO6 (aka LPC1114 PIO1_8)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO6
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO6, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO6, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO7 (aka LPC1114 PIO1_9)
        /// Allowed values are 0-1 or "ON", "OFF", "TRUE", and "FALSE".
        /// </summary>
        public static Primitive GPIO7
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_GPIO, Pins.LPC1114_GPIO7, 0);
                return response_data;
            }

            set
            {
                int d = ConvertPrimitive(value, data_table, 0, 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_GPIO, Pins.LPC1114_GPIO7, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO0 (aka LPC1114 PIO1_0) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO0_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL-1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO0, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO1 (aka LPC1114 PIO1_1) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO1_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL-1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO1, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO2 (aka LPC1114 PIO1_2) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO2_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO2, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO3 (aka LPC1114 PIO1_3) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO3_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO3, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO4 (aka LPC1114 PIO1_4) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO4_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO4, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO5 (aka LPC1114 PIO1_5) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO5_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO5, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO6 (aka LPC1114 PIO1_8) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO6_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO6, d);
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board General Purpose Input Output terminal GPIO7 (aka LPC1114 PIO1_9) pin configuration
        /// Allowed values are 0-4, or "INPUT", "INPUT_PULLDOWN", "INPUT_PULLUP", "OUTPUT", and "OUTPUT_OPENDRAIN".
        /// </summary>
        public static Primitive GPIO7_config
        {
            set
            {
                int d = ConvertPrimitive(value, data_table, 0, (int)GPIO.MODE.SENTINEL - 1);
                if (response_error != (int)errno.EOK) return;

                CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_GPIO, Pins.LPC1114_GPIO7, d);
            }
        }
    }
}
