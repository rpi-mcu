// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent Firmware Extension for Microsoft Small Basic

// LEGO(R) Power Functions Remote Control services

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.SmallBasic.Library;
using System;

namespace SPIAgent
{
    public static partial class SPIAgent
    {
        /// <summary>
        /// Transmits a LEGO(R) Power Functions Remote Control command from a GPIO pin.
        /// The GPIO pin must be already configured as an output.
        /// </summary>
        /// <param name="Pin">Allowed values are "GPIO0" through "GPIO7".</param>
        /// <param name="Channel">Allowed values are 1-4.</param>
        /// <param name="Motor">Allowed values are 0-4 or "All Stop", "Motor A", "Motor B", "Combo Direct" and "Combo PWM".</param>
        /// <param name="Direction">Allowed values are 0-1 or "Reverse" and "Forward" with Motor A and Motor B, and 0 with all others.</param>
        /// <param name="Speed">Allowed values are 0-7 with Motor A and Motor B, 0-15 with Combo Direct, and 0-255 with Comobo PWM.</param>
        public static void LegoRemoteCommand(Primitive Pin, Primitive Channel, Primitive Motor, Primitive Direction, Primitive Speed)
        {
            int c = ConvertPrimitive(Channel, data_table, LEGORC.MIN_CHANNEL, LEGORC.MAX_CHANNEL);
            if (response_error != (int)errno.EOK) return;

            int m = ConvertPrimitive(Motor, data_table, 0, (int)LEGORC.MOTOR.SENTINEL-1);
            if (response_error != (int)errno.EOK) return;

            int d = ConvertPrimitive(Direction, data_table, 0, (int)LEGORC.DIRECTION.SENTINEL-1);
            if (response_error != (int)errno.EOK) return;

            int s = ConvertPrimitive(Speed, data_table, 0, LEGORC.MAX_SPEED);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_PUT_LEGORC, Pin, (c << 24) + (m << 16) + (d << 8) + s);
        }
    }
}
