// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent Firmware Extension for Microsoft Small Basic

// Timer services

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.SmallBasic.Library;
using System;

namespace SPIAgent
{
    public static partial class SPIAgent
    {
        /// <summary>
        /// Initialize LPC1114 32-bit hardware timer.
        /// </summary>
        /// <param name="TimerID">Allowed values are 0-1 or "CT32B0" and "CT32B1".</param>
        public static void TimerInitialize(Primitive TimerID)
        {
            int t = ConvertPrimitive(TimerID, pin_table, (int)Timer.ID.CT32B0, (int)Timer.ID.CT32B1);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_INIT_TIMER, t, 0);
        }

        /// <summary>
        /// Configure LPC1114 32-bit hardware timer operating mode.
        /// </summary>
        /// <param name="TimerID">Allowed values are 0-1 or "CT32B0" and "CT32B1".</param>
        /// <param name="ModeID">Allowed values are 0-5 or "DISABLED", "RESET", "PCLK", "CAP0_RISING", "CAP0_FALLING", and "CAP0_BOTH".</param>
        public static void TimerConfigureMode(Primitive TimerID, Primitive ModeID)
        {
            int t = ConvertPrimitive(TimerID, pin_table, (int)Timer.ID.CT32B0, (int)Timer.ID.CT32B1);
            if (response_error != (int)errno.EOK) return;

            int m = ConvertPrimitive(ModeID, data_table, 0, (int)Timer.MODE.SENTINEL - 1);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_TIMER_MODE, t, m);
        }

        /// <summary>
        /// Configure LPC1114 32-bit hardware timer prescaler.
        /// </summary>
        /// <param name="TimerID">Allowed values are 0-1 or "CT32B0" and "CT32B1".</param>
        /// <param name="Prescaler">Allowwed values are 1-2147483647.</param>
        public static void TimerConfigurePrescaler(Primitive TimerID, Primitive Prescaler)
        {
            int t = ConvertPrimitive(TimerID, pin_table, (int)Timer.ID.CT32B0, (int)Timer.ID.CT32B1);
            if (response_error != (int)errno.EOK) return;

            int p = ConvertPrimitive(Prescaler, null, 1, 2147483647);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_TIMER_PRESCALER, t, p);
        }

        /// <summary>
        /// Configure LPC1114 32-bit hardware timer capture mode.
        /// </summary>
        /// <param name="TimerID">Allowed values are 0-1 or "CT32B0" and "CT32B1".</param>
        /// <param name="Edge">Allowed values are 0-3 or "DISABLED", "CAP0_RISING", "CAP0_FALLING", and "CAP0_BOTH".</param>
        /// <param name="Interrupt">Allowed values are 0-1 or "DISABLED" and "ENABLED".</param>
        public static void TimerConfigureCapture(Primitive TimerID, Primitive Edge, Primitive Interrupt)
        {
            int t = ConvertPrimitive(TimerID, pin_table, (int)Timer.ID.CT32B0, (int)Timer.ID.CT32B1);
            if (response_error != (int)errno.EOK) return;

            int e = ConvertPrimitive(Edge, data_table, 0, (int)Timer.MODE.SENTINEL - 1);   // Reuse "CAP0_RISING" et al from mode enumeration
            if (response_error != (int)errno.EOK) return;
            if (e >= (int)Timer.MODE.CAP0_RISING) e -= 2;                   // Reuse "CAP0_RISING" et al from mode enumeration

            int i = ConvertPrimitive(Interrupt, data_table, 0, 1);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_TIMER_CAPTURE, t, e | (i << 4));
        }

        /// <summary>
        /// Configure LPC1114 32-bit hardware timer match register.  Eatch timer has 4 match registers.  CT32B0 match outputs are NOT available.
        /// </summary>
        /// <param name="TimerID">Allowed values are 0-1 or "CT32B0" and "CT32B1".</param>
        /// <param name="Match">Allowed values are 0-3.</param>
        /// <param name="Value">Allowed values are -2147483648-2147483647.</param>
        /// <param name="Action">Allowed values are 0-3 or "DISABLED", "CLEAR", "SET", and "TOGGLE".</param>
        /// <param name="Interrupt">Allowed values are 0-1 or "DISABLED" and "ENABLED".</param>
        /// <param name="Reset">Allowed values are 0-1 or "DISABLED" and "ENABLED".</param>
        /// <param name="Stop">Allowed values are 0-1 or "DISABLED" and "ENABLED".</param>
        public static void TimerConfigureMatch(Primitive TimerID, Primitive Match, Primitive Value,
            Primitive Action, Primitive Interrupt, Primitive Reset, Primitive Stop)
        {
            int t = ConvertPrimitive(TimerID, pin_table, (int)Timer.ID.CT32B0, (int)Timer.ID.CT32B1);
            if (response_error != (int)errno.EOK) return;

            int m = ConvertPrimitive(Match, data_table, 0, 3);
            if (response_error != (int)errno.EOK) return;

            int v = ConvertPrimitive(Value, null, -2147483648, 2147483647);
            if (response_error != (int)errno.EOK) return;

            int a = ConvertPrimitive(Action, data_table, 0, (int)Timer.MATCH_OUTPUT.SENTINEL-1);
            if (response_error != (int)errno.EOK) return;

            int i = ConvertPrimitive(Interrupt, data_table, 0, 1);
            if (response_error != (int)errno.EOK) return;

            int r = ConvertPrimitive(Reset, data_table, 0, 1);
            if (response_error != (int)errno.EOK) return;

            int s = ConvertPrimitive(Stop, data_table, 0, 1);
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0 + m, t, a | (i << 4) | (r << 5) | (s << 6));
            if (response_error != (int)errno.EOK) return;

            CommandWrapper((int)Commands.SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0_VALUE + m, t, v);
        }

        /// <summary>
        /// LPC1114 hardware 32-bit timer CT32B0 counter register
        /// </summary>
        public static Primitive Counter0
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_VALUE, (int)Timer.ID.CT32B0, 0);
                return response_data;
            }
        }

        /// <summary>
        /// LPC1114 hardware timer CT32B1 counter register
        /// </summary>
        public static Primitive Counter1
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_VALUE, (int)Timer.ID.CT32B1, 0);
                return response_data;
            }
        }

        /// <summary>
        /// LPC1114 hardware 32-bit timer CT32B0 capture register
        /// </summary>
        public static Primitive Capture0
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_CAPTURE, (int)Timer.ID.CT32B0, 0);
                return response_data;
            }
        }

        /// <summary>
        /// LPC1114 hardware 32-bit timer CT32B1 capture register
        /// </summary>
        public static Primitive Capture1
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_CAPTURE, (int)Timer.ID.CT32B1, 0);
                return response_data;
            }
        }

        /// <summary>
        /// LPC1114 hardware 32-bit timer CT32B0 capture delta value
        /// </summary>
        public static Primitive CaptureDelta0
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_CAPTURE_DELTA, (int)Timer.ID.CT32B0, 0);
                return response_data;
            }
        }

        /// <summary>
        /// LPC1114 hardware 32-bit timer CT32B1 capture delta value
        /// </summary>
        public static Primitive CaptureDelta1
        {
            get
            {
                CommandWrapper((int)Commands.SPIAGENT_CMD_GET_TIMER_CAPTURE_DELTA, (int)Timer.ID.CT32B1, 0);
                return response_data;
            }
        }
    }
}
