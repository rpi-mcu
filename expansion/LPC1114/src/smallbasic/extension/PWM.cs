// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent Firmware Extension for Microsoft Small Basic

// Pulse Width Modulated output services

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.SmallBasic.Library;
using System;

namespace SPIAgent
{
    public static partial class SPIAgent
    {
        // Private state variables
        private static int pFreq123 = 50;
        private static int pFreq4 = 50;
        private static PWM pPWM1 = null;
        private static PWM pPWM2 = null;
        private static PWM pPWM3 = null;
        private static PWM pPWM4 = null;
        private static Servo pServo1 = null;
        private static Servo pServo2 = null;
        private static Servo pServo3 = null;
        private static Servo pServo4 = null;


        private static void PWMInitialize()
        {
            pFreq123 = 50;
            pFreq4 = 50;
            pPWM1 = null;
            pPWM2 = null;
            pPWM3 = null;
            pPWM4 = null;
            pServo1 = null;
            pServo2 = null;
            pServo3 = null;
            pServo4 = null;
        }

        /// <summary>
        /// <para/>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulation frequency for PWM1 and Servo1.
        /// <para/>
        /// Allowed values are 50-50000 Hz.  Default value is 50 Hz.
        /// <para/>
        /// Small hobby motors often run best at 50 Hz; larger industrial motors often run best at higher frequencies.
        /// <para/>
        /// Standard analog servos must run at 50 Hz.  Digital servos can run as high as 400 Hz.
        /// <para/>
        /// PWM1, PWM2, PWM3, Servo1, Servo2, and Servo3 all share the same clock generator and output the same pulse rate.
        /// </summary>
        public static Primitive PWM1_Frequency
        {
            get
            {
                return pFreq123;
            }

            set
            {
                try
                {
                    pFreq123 = ConvertPrimitive(value, null, 50, 50000);
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// <para/>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulation frequency for PWM2 and Servo2.
        /// <para/>
        /// Allowed values are 50-50000 Hz.  Default value is 50 Hz.
        /// <para/>
        /// Small hobby motors often run best at 50 Hz; larger industrial motors often run best at higher frequencies.
        /// <para/>
        /// Standard analog servos must run at 50 Hz.  Digital servos can run as high as 400 Hz.
        /// <para/>
        /// PWM1, PWM2, PWM3, Servo1, Servo2, and Servo3 all share the same clock generator and will output the same pulse rate.
        /// </summary>
        public static Primitive PWM2_Frequency
        {
            get
            {
                return pFreq123;
            }

            set
            {
                try
                {
                    pFreq123 = ConvertPrimitive(value, null, 50, 50000);
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// <para/>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulation frequency for PWM3 and Servo3.
        /// <para/>
        /// Allowed values are 50-50000 Hz.  Default value is 50 Hz.
        /// <para/>
        /// Small hobby motors often run best at 50 Hz; larger industrial motors often run best at higher frequencies.
        /// <para/>
        /// Standard analog servos must run at 50 Hz.  Digital servos can run as high as 400 Hz.
        /// <para/>
        /// PWM1, PWM2, PWM3, Servo1, Servo2, and Servo3 all share the same clock generator and will output the same pulse rate.
        /// </summary>
        public static Primitive PWM3_Frequency
        {
            get
            {
                return pFreq123;
            }

            set
            {
                try
                {
                    pFreq123 = ConvertPrimitive(value, null, 50, 50000);
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// <para/>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulation frequency for PWM4 and Servo4.
        /// <para/>
        /// Allowed values are 50-50000 Hz.  Default value is 50 Hz.
        /// <para/>
        /// Small hobby motors often run best at 50 Hz; larger industrial motors often run best at higher frequencies.
        /// <para/>
        /// Standard analog servos must run at 50 Hz.  Digital servos can run as high as 400 Hz.
        /// </summary>
        public static Primitive PWM4_Frequency
        {
            get
            {
                return pFreq4;
            }

            set
            {
                try
                {
                    pFreq4 = ConvertPrimitive(value, null, 50, 50000);
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulated output PWM1 (aka GPIO1 aka LPC1114 PIO1_1).
        /// Write only.  Allowed values are 0.0-100.0 percent duty cycle.
        /// </summary>
        public static Primitive PWM1
        {
            set
            {
                try
                {
                    if (pPWM1 == null)
                    {
                        pPWM1 = new PWM(server, Pins.LPC1114_PWM1, pFreq123);
                    }

                    pPWM1.dutycycle = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulated output PWM2 (aka GPIO2 aka LPC1114 PIO1_2).
        /// Write only.  Allowed values are 0.0-100.0 percent duty cycle.
        /// </summary>
        public static Primitive PWM2
        {
            set
            {
                try
                {
                    if (pPWM2 == null)
                    {
                        pPWM2 = new PWM(server, Pins.LPC1114_PWM2, pFreq123);
                    }

                    pPWM2.dutycycle = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulated output PWM3 (aka GPIO3 aka LPC1114 PIO1_3).
        /// Write only.  Allowed values are 0.0-100.0 percent duty cycle.
        /// </summary>
        public static Primitive PWM3
        {
            set
            {
                try
                {
                    if (pPWM3 == null)
                    {
                        pPWM3 = new PWM(server, Pins.LPC1114_PWM3, pFreq123);
                    }

                    pPWM3.dutycycle = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board Pulse Width Modulated output PWM4 (aka GPIO7 aka LPC1114 PIO1_9).
        /// Write only.  Allowed values are 0.0-100.0 percent duty cycle.
        /// </summary>
        public static Primitive PWM4
        {
            set
            {
                try
                {
                    if (pPWM4 == null)
                    {
                        pPWM4 = new PWM(server, Pins.LPC1114_PWM4, pFreq4);
                    }

                    pPWM4.dutycycle = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board RC servo motor output Servo1 (aka PWM1 aka GPIO1 aka LPC1114 PIO1_1).
        /// Write only.  Allowed values are -1.0 to 1.0 normalized deflection from the null position.
        /// </summary>
        public static Primitive Servo1
        {
            set
            {
                try
                {
                    if (pServo1 == null)
                    {
                        pServo1 = new Servo(server, Pins.LPC1114_PWM1, pFreq123);
                    }

                    pServo1.position = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board RC servo motor output Servo2 (aka PWM2 aka GPIO2 aka LPC1114 PIO1_2).
        /// Write only.  Allowed values are -1.0 to 1.0 normalized deflection from the null position.
        /// </summary>
        public static Primitive Servo2
        {
            set
            {
                try
                {
                    if (pServo2 == null)
                    {
                        pServo2 = new Servo(server, Pins.LPC1114_PWM2, pFreq123);
                    }

                    pServo2.position = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board RC servo motor output Servo3 (aka PWM3 aka GPIO3 aka LPC1114 PIO1_3).
        /// Write only.  Allowed values are -1.0 to 1.0 normalized deflection from the null position.
        /// </summary>
        public static Primitive Servo3
        {
            set
            {
                try
                {
                    if (pServo3 == null)
                    {
                        pServo3 = new Servo(server, Pins.LPC1114_PWM3, pFreq123);
                    }

                    pServo3.position = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }

        /// <summary>
        /// LPC1114 I/O Processor Expansion Board RC servo motor output Servo4 (aka PWM4 aka GPIO7 aka LPC1114 PIO1_9).
        /// Write only.  Allowed values are -1.0 to 1.0 normalized deflection from the null position.
        /// </summary>
        public static Primitive Servo4
        {
            set
            {
                try
                {
                    if (pServo4 == null)
                    {
                        pServo4 = new Servo(server, Pins.LPC1114_PWM4, pFreq4);
                    }

                    pServo4.position = float.Parse(value.ToString());
                    response_error = (int)errno.EOK;
                }
                catch
                {
                    response_error = (int)errno.EINVAL;
                }
            }
        }
    }
}
