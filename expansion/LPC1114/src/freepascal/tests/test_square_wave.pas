{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ square wave output test                                               }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_square_wave(input, output);

USES
  sysutils,
  spi_agent_pins,
  spi_agent_timer,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';
  DEFAULTFREQ   = 1000;

VAR
  servername : pchar;
  freq       : integer;
  error      : integer;
  timer1     : TIMER_t;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Square Wave Output Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    BEGIN
      servername := DEFAULTSERVER;
      freq := DEFAULTFREQ;
    END
  ELSE IF system.argc = 2 THEN
    BEGIN
      servername := argv[1];
      freq := DEFAULTFREQ;
    END
  ELSE IF system.argc = 3 THEN
    BEGIN
      servername := argv[1];
      freq := StrToInt(argv[2]);
    END
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname] [frequency]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Configure the LPC1114 CT32B1 }

  timer1 := TIMER_t.create(LPC1114_CT32B1);

  timer1.ConfigurePrescaler(1);

  timer1.ConfigureMatch(LPC1114_TIMER_MATCH0, PCLK_FREQUENCY DIV freq DIV 2,
    LPC1114_TIMER_MATCH_OUTPUT_TOGGLE, LPC1114_TIMER_MATCH_INTERRUPT_DISABLE,
    LPC1114_TIMER_MATCH_RESET_ENABLE, LPC1114_TIMER_MATCH_STOP_DISABLE);

  timer1.ConfigureMode(LPC1114_TIMER_MODE_PCLK);

  { Destroy objects }

  FreeAndNil(timer1);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
