{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ sonar ranging test program                                            }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

{ LPC1114 GPIO0 -- Echo pulse input                                     }
{ LPC1114 GPIO1 -- Trigger pulse output                                 }
{                                                                       }
{ NOTE: This program is written for and tested with 4-pin ultrasonic    }
{ modules like the HC-SR04.  To use a 3-pin module like the Parallax    }
{ Ping, connect GPIO1 to GPIO0 through a Schottky signal diode like the }
{ BAT43, anode to GPIO1 and cathode to GPIO0.                           }

PROGRAM test_sonar(input, output);

USES
  CRT,
  sysutils,
  spi_agent_gpio,
  spi_agent_pins,
  spi_agent_timer,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  timer1     : TIMER_t;
  trigger    : GPIO_t;
  count      : dword;
  distance   : integer;

BEGIN
  clrscr;

  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Sonar Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Configure LPC1114 timer 1 to measure pulse width on CAP0 input }

  timer1 := TIMER_t.create(LPC1114_CT32B1);

  timer1.ConfigurePrescaler(1);

  timer1.ConfigureCapture(LPC1114_TIMER_CAPTURE_EDGE_CAP0_BOTH,
    LPC1114_TIMER_CAPTURE_INTERRUPT_DISABLE);

  timer1.ConfigureMode(LPC1114_TIMER_MODE_PCLK);

  { Configure the trigger pulse output }

  trigger := GPIO_t.create(LPC1114_GPIO1, GPIO_OUTPUT, false);

  { Perform sonar ranging }

  writeln('Press any key to exit program');
  writeln;

  REPEAT

    { Pulse trigger output }

    trigger.put(TRUE);
    trigger.put(FALSE);

    sleep(100);

    { Get echo pulse width result }

    count := timer1.GetCaptureDelta;

    { Calculate range }

    distance := round(count/282.35);

    { Display result }

    gotoxy(1, 6);
    clreol();

    IF distance < 2000 THEN
      write('Detection! Range is ', distance : 1, ' mm')
    ELSE
      write('Probing...');

    sleep(900);
  UNTIL KeyPressed;

  writeln;

  { Destroy objects }

  FreeAndNil(timer1);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
