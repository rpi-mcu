{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ LED test                                                              }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_frequency_counter(input, output);

USES
  CRT,
  sysutils,
  spi_agent_pins,
  spi_agent_timer,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  timer0     : TIMER_t;
  count      : dword;
  freq       : single;

BEGIN
  clrscr;

  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Frequency Counter Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Create the CT32B1 timer object }

  timer0 := TIMER_t.create(LPC1114_CT32B0);

  timer0.ConfigurePrescaler(1);

  timer0.ConfigureCapture(LPC1114_TIMER_CAPTURE_EDGE_CAP0_RISING,
    LPC1114_TIMER_CAPTURE_INTERRUPT_DISABLE);

  timer0.ConfigureMode(LPC1114_TIMER_MODE_PCLK);

  writeln('Press any key to exit program');
  writeln;

  REPEAT
    count := timer0.GetCaptureDelta;

    IF count < 1920 THEN
      freq := 0.0
    ELSE
      freq := PCLK_FREQUENCY/count;

    gotoxy(1, 6);
    clreol();
    write('Frequency: ', freq : 1 : 1, ' Hz');

    sleep(1000);
  UNTIL KeyPressed;

  writeln;

  { Destroy objects }

  FreeAndNil(timer0);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
