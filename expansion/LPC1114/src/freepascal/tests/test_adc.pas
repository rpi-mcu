{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Analog Input test                                                     }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_adc(input, output);

USES
  CRT,
  sysutils,
  spi_agent_analog,
  spi_agent_pins,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  adc1       : ANALOG_INPUT_t;
  adc2       : ANALOG_INPUT_t;
  adc3       : ANALOG_INPUT_t;
  adc4       : ANALOG_INPUT_t;
  adc5       : ANALOG_INPUT_t;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Analog Input Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  adc1 := ANALOG_INPUT_t.create(LPC1114_AD1);
  adc2 := ANALOG_INPUT_t.create(2);
  adc3 := ANALOG_INPUT_t.create(LPC1114_AD3);
  adc4 := ANALOG_INPUT_t.create(4);
  adc5 := ANALOG_INPUT_t.create(LPC1114_AD5);

  ClrScr;
  GotoXY(1, 1);
  write('Raspberry Pi LPC1114 I/O Processor Expansion Board Analog Input Test');

  GotoXY(1, 9);
  write('Press any key to exit program');

  REPEAT
    GotoXY(1, 3); write('Analog Input 1 is ', adc1.get : 4 : 2, 'V');
    GotoXY(1, 4); write('Analog Input 2 is ', adc2.get : 4 : 2, 'V');
    GotoXY(1, 5); write('Analog Input 3 is ', adc3.get : 4 : 2, 'V');
    GotoXY(1, 6); write('Analog Input 4 is ', adc4.get : 4 : 2, 'V');
    GotoXY(1, 7); write('Analog Input 5 is ', adc5.get : 4 : 2, 'V');
    sleep(1000);
  UNTIL KeyPressed;

  GotoXy(1, 9);
  ClrEol;

  { Destroy objects }

  FreeAndNil(adc1);
  FreeAndNil(adc2);
  FreeAndNil(adc3);
  FreeAndNil(adc4);
  FreeAndNil(adc5);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
