{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Interrupt test                                                        }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_interrupts(input, output);

USES
  sysutils,
  errno,
  spi_agent_gpio,
  spi_agent_interrupt,
  spi_agent_pins,
  spi_agent_transport;

CONST
  IntInput = LPC1114_GPIO0;

VAR
  input_pin : GPIO_t;	{ LPC1114 interrupt input pin }
  intr_pin  : integer;	{ Raspberry Pi interrupt input pin }
  error     : integer;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Interrupt Test');
  writeln;

  { Initialize the SPI Agent transport library }

  spiagent_open('ioctl://localhost', error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(1);
    END;

  { Configure an LPC1114 GPIO pin as input with pull-up, with interrupt enabled }

  input_pin := GPIO_t.Create(IntInput, GPIO_INPUT, true);
  input_pin.ConfigureInterrupt(GPIO_INTERRUPT_FALLING);

  { Enable interrupt monitoring on Raspberry Pi INT (GPIO4) }

  spiagent_interrupt_enable(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_interrupt_enable() failed, error=', error);
      writeln;
      halt(2);
    END;

  writeln('Press CONTROL-C to quit');
  writeln;

  REPEAT
    spiagent_interrupt_wait(1000, intr_pin, error);

    CASE error OF
      EOK:
        IF intr_pin <> 0 THEN
          writeln('Received interrupt on GPIO pin ', intr_pin);

      EINTR:		{ poll() interrupted by signal (CONTROL-C) }
        BREAK;
    ELSE
      writeln('ERROR: spiagent_interrupt_wait() failed, error=', error);
      writeln;
      halt(3);
    END;
  UNTIL false;

  writeln;
  writeln('Terminating.');
  writeln;

  { Return LPC1114 input pin to default configuration }

  input_pin.ConfigureMode(GPIO_MODE_INPUT_PULLUP);
  input_pin.ConfigureInterrupt(GPIO_INTERRUPT_DISABLED);

  { Disable interrupt monitoring on Raspberry Pi INT (GPIO4) }

  spiagent_interrupt_disable(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_interrupt_enable() failed, error=', error);
      writeln;
      halt(4);
    END;

  { Destroy objects }

  FreeAndNil(input_pin);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(5);
    END;
END.
