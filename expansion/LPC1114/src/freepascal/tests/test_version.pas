{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Version test                                                          }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_version(input, output);

USES
  strings,
  sysutils,
  spi_agent_commands,
  spi_agent_messages,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  cmd        : SPIAGENT_COMMAND_MSG_t;
  resp       : SPIAGENT_RESPONSE_MSG_t;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board Firmware Version Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Issue NOP command to the SPI Agent Firmware }

  cmd.command := integer(SPIAGENT_CMD_NOP);
  cmd.pin := 0;
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(4);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      writeln('ERROR: SPI Agent Firmware returned error ', resp.error);
      writeln;
      halt(5);
    END;

  { Display results }

  writeln('The SPI Agent Firmware version number is ', resp.data);

  { Issue GET_SFR command to the SPI Agent Firmware }

  cmd.command := integer(SPIAGENT_CMD_GET_SFR);
  cmd.pin := $400483F4;
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(4);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      writeln('ERROR: SPI Agent Firmware returned error ', resp.error);
      writeln;
      halt(5);
    END;

  { Display results }

  writeln('The LPC1114 device ID is $', IntToHex(resp.data, 8));
  writeln;

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(6);
    END;
END.
