{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ LED test                                                              }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_led(input, output);

USES
  CRT,
  sysutils,
  spi_agent_gpio,
  spi_agent_pins,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  LED        : GPIO_t;
  state      : boolean = false;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board LED Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  LED := GPIO_t.create(LPC1114_LED, GPIO_OUTPUT, false);

  writeln('Press any key to exit program');
  writeln;

  state := true;

  REPEAT
    IF state THEN
      writeln('Turning LED ON')
    ELSE
      writeln('Turning LED OFF');

    LED.put(state);

    sleep(1000);

    state := NOT state;
  UNTIL KeyPressed;

  { Destroy objects }

  FreeAndNil(LED);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
