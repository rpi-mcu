{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ LEGO� Power Functions Remote Control test                             }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_legorc(input, output);

USES
  sysutils,
  spi_agent_legorc,
  spi_agent_pins,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  channel    : legorc_channel_t;
  motor      : legorc_motor_t;
  direction  : legorc_direction_t;
  speed      : legorc_speed_t;
  error      : integer;
  IRED       : legorc_output_t;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board LEGO� RC Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 5 THEN
    BEGIN
      servername := DEFAULTSERVER;
      channel    := legorc_channel_t(StrToInt(argv[1]));
      motor      := legorc_motor_t(StrToInt(argv[2]));
      direction  := legorc_direction_t(StrToInt(argv[3]));
      speed      := legorc_speed_t(StrToInt(argv[4]));
    END
  ELSE IF system.argc = 6 THEN
    BEGIN
      servername := argv[1];
      channel    := legorc_channel_t(StrToInt(argv[2]));
      motor      := legorc_motor_t(StrToInt(argv[3]));
      direction  := legorc_direction_t(StrToInt(argv[4]));
      speed      := legorc_speed_t(StrToInt(argv[5]));
    END
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname] <channel:1-4> <motor:0-2> <direction:0-1> <speed:0-7>');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Configure the IRED output pin }

  IRED := legorc_output_t.create(LPC1114_GPIO7);

  { Issue the LEGO� RC command }

  IRED.put(channel, motor, direction, speed);

  { Destroy objects }

  FreeAndNil(IRED);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_close() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
