{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Special Function Register test                                        }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_sfr(input, output);

USES
  strings,
  sysutils,
  spi_agent_sfr,
  spi_agent_transport;

CONST
  DEFAULTSERVER = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  deviceid   : SFR_t;
  uart0scr   : SFR_t;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board SFR Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Create SFR objects }

  deviceid := SFR_t.create(LPC1114_DEVICE_ID);
  uart0scr := SFR_t.create(LPC1114_U0SCR);

  { Read the device ID register }

  writeln('The LPC1114 device ID is                 $', IntToHex(deviceid.get, 8));

  { Write the UART scratch pad register }

  uart0scr.put($AA);

  { Read the device ID register }

  writeln('The LPC1114 UART scratch pad register is $', IntToHex(uart0scr.get, 8));

  { Destroy objects }

  FreeAndNil(deviceid);
  FreeAndNil(uart0scr);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
