{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Pulse Width Modulated output test                                     }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM test_pwm(input, output);

USES
  sysutils,
  spi_agent_pins,
  spi_agent_pwm,
  spi_agent_transport;

CONST
  DEFAULTSERVER   = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  freq       : integer;
  PWM        : ARRAY [pwm_channel_t] OF PWM_OUTPUT_t;
  channel    : integer;
  dutycycle  : real;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board PWM Output Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Get desired PWM frequency from the user }

  REPEAT
    write('Enter PWM frequency:              ');
    readln(freq);
  UNTIL (freq >= low(PWM_FREQUENCY_t)) AND (freq <= high(PWM_FREQUENCY_t));

  { Initialize the PWM outputs }

  FOR channel := Low(pwm_channel_t) TO High(pwm_channel_t) DO
    PWM[channel] := PWM_OUTPUT_t.create(channel, freq);

  { Change PWM outputs }

  REPEAT
    REPEAT
      write('Enter PWM channel number (1-4):   ');
      readln(channel);
    UNTIL (channel = 0) OR ((channel >= 1) AND (channel <= 4));

    IF channel = 0 THEN
      BREAK;

    REPEAT
      write('Enter PWM duty cycle % (0-100.0): ');
      readln(dutycycle);
    UNTIL (dutycycle >= PWM_MIN_DUTYCYCLE) AND (dutycycle <= PWM_MAX_DUTYCYCLE);

    PWM[channel].put(dutycycle);
  UNTIL channel = 0;

  { Destroy objects }

  FOR channel := Low(pwm_channel_t) TO High(pwm_channel_t) DO
    FreeAndNil(PWM[channel]);

  { Deinitialize the SPI Agent transport library }

  spiagent_close(error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_close() failed, error=', error);
      writeln;
      halt(3);
    END;
END.
