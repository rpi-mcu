{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ H-bridge DC motor driver output test program                          }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

{ Direction output -- GPIO0          }
{ PWM output -------- GPIO1 aka PWM1 }

PROGRAM test_pwm(input, output);

USES
  sysutils,
  spi_agent_pins,
  spi_agent_pwm,
  spi_agent_transport;

CONST
  DEFAULTSERVER   = 'localhost';

VAR
  servername : pchar;
  error      : integer;
  Frequency  : integer;
  Motor      : MOTOR_OUTPUT_t;
  Speed      : real;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board DC Motor Test');
  writeln;

  { Analyze command line parameters }

  IF argc = 1 THEN
    servername := DEFAULTSERVER
  ELSE IF system.argc = 2 THEN
    servername := argv[1]
  ELSE
    BEGIN
      writeln('Usage: ', system.argv[0], ' [hostname]');
      writeln;
      halt(1);
    END;

  { Initialize the SPI Agent transport library }

  spiagent_open(servername, error);

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_open() failed, error=', error);
      writeln;
      halt(2);
    END;

  { Get desired PWM frequency from the user }

  REPEAT
    write('Enter PWM frequency:              ');
    readln(Frequency);
  UNTIL (Frequency >= low(PWM_FREQUENCY_t)) AND (Frequency <= high(PWM_FREQUENCY_t));

  { Initialize the motor driver outputs }

  Motor := MOTOR_OUTPUT_t.create(LPC1114_PWM1, LPC1114_GPIO0, Frequency);

  REPEAT
    REPEAT
      write('Enter motor speed (-1.0 to +1.0): ');
      readln(Speed);
    UNTIL (Speed >= MOTOR_MIN_SPEED) AND (Speed <= MOTOR_MAX_SPEED);

    Motor.put(Speed);
  UNTIL False;
END.
