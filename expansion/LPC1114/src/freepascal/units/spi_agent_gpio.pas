{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ General Purpose Input Output services                                 }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_gpio;

INTERFACE

  USES
    lpc11xx,
    spi_agent_transport,
    spi_agent_commands,
    spi_agent_exceptions,
    spi_agent_messages;

  TYPE
    gpio_direction_t =
     (GPIO_INPUT,
      GPIO_OUTPUT);

    gpio_mode_t =
     (GPIO_MODE_INPUT,			{ Normal high impedance input }
      GPIO_MODE_INPUT_PULLDOWN,
      GPIO_MODE_INPUT_PULLUP,
      GPIO_MODE_OUTPUT,			{ Normal push-pull output }
      GPIO_MODE_OUTPUT_OPENDRAIN);

    gpio_interrupt_config_t =
     (GPIO_INTERRUPT_DISABLED,
      GPIO_INTERRUPT_FALLING,
      GPIO_INTERRUPT_RISING,
      GPIO_INTERRUPT_BOTH);

    GPIO_t =
      CLASS
        CONSTRUCTOR Create
         (gpio_pin  : GPIO_PIN_t;
          direction : gpio_direction_t;
          state     : Boolean);

        CONSTRUCTOR Create
         (gpio_pin  : GPIO_PIN_t;
          mode      : gpio_mode_t);

        PROCEDURE ConfigureMode
         (mode      : gpio_mode_t);

        PROCEDURE ConfigureInterrupt
         (intcfg    : gpio_interrupt_config_t);

        FUNCTION get : Boolean;

        PROCEDURE put(state : Boolean);

      PRIVATE
        pin : GPIO_PIN_t;
      END;

IMPLEMENTATION

  CONSTRUCTOR GPIO_t.Create
   (gpio_pin  : GPIO_PIN_t;
    direction : gpio_direction_t;
    state     : Boolean);

  VAR
    cmd       : SPIAGENT_COMMAND_MSG_t;
    resp      : SPIAGENT_RESPONSE_MSG_t;
    error     : integer;
    errbuf    : string;

  BEGIN
    INHERITED Create;

    { Build the command structure }

    IF direction = GPIO_INPUT THEN
      BEGIN
        cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO_INPUT);
        cmd.pin := ord(gpio_pin);
        cmd.data := ord(state);
      END
    ELSE
      BEGIN
        cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO_OUTPUT);
        cmd.pin := ord(gpio_pin);
        cmd.data := ord(state);
      END;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    Self.pin := gpio_pin;
  END;

  CONSTRUCTOR GPIO_t.Create
   (gpio_pin : GPIO_PIN_t;
    mode     : gpio_mode_t);

  VAR
    cmd      : SPIAGENT_COMMAND_MSG_t;
    resp     : SPIAGENT_RESPONSE_MSG_t;
    error    : integer;
    errbuf   : string;

  BEGIN
    INHERITED Create;

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO);
    cmd.pin := ord(gpio_pin);
    cmd.data := ord(mode );

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    Self.pin := gpio_pin;
  END;

  PROCEDURE GPIO_t.ConfigureMode
   (mode   : gpio_mode_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO);
    cmd.pin := ord(Self.pin);
    cmd.data := ord(mode );

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

  PROCEDURE GPIO_t.ConfigureInterrupt
   (intcfg : gpio_interrupt_config_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO_INTERRUPT);
    cmd.pin := ord(Self.pin);
    cmd.data := ord(intcfg);

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

  FUNCTION GPIO_t.get : Boolean;

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_GPIO);
    cmd.pin := ord(Self.pin);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    get := Boolean(resp.data);
  END;

  PROCEDURE GPIO_t.put(state : Boolean);

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_PUT_GPIO);
    cmd.pin := ord(Self.pin);
    cmd.data := ord(state);

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

END.
