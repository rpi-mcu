{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ I/O pin definitions                                                   }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_pins;

INTERFACE

  USES
    lpc11xx;

  CONST

{ Interrupt and ready output (to the Raspberry Pi) pins }

    LPC1114_INT         = PIO0_3;
    LPC1114_READY       = PIO0_11;

{ Expansion board LED }

    LPC1114_LED         = PIO0_7;

{ GPIO pins on the terminal block }

    LPC1114_GPIO0       = PIO1_0;
    LPC1114_GPIO1       = PIO1_1;
    LPC1114_GPIO2       = PIO1_2;
    LPC1114_GPIO3       = PIO1_3;
    LPC1114_GPIO4       = PIO1_4;
    LPC1114_GPIO5       = PIO1_5;
    LPC1114_GPIO6       = PIO1_8;
    LPC1114_GPIO7       = PIO1_9;

{ Analog input pins }

    LPC1114_AD1         = LPC1114_GPIO0;
    LPC1114_AD2         = LPC1114_GPIO1;
    LPC1114_AD3         = LPC1114_GPIO2;
    LPC1114_AD4         = LPC1114_GPIO3;
    LPC1114_AD5         = LPC1114_GPIO4;

{ PWM output pins }

    LPC1114_PWM1        = LPC1114_GPIO1;
    LPC1114_PWM2        = LPC1114_GPIO2;
    LPC1114_PWM3        = LPC1114_GPIO3;
    LPC1114_PWM4	= LPC1114_GPIO7;

{ Timer input/output pins }

    LPC1114_CT32B1_CAP0 = LPC1114_GPIO0;
    LPC1114_CT32B1_MAT0 = LPC1114_GPIO1;
    LPC1114_CT32B1_MAT1 = LPC1114_GPIO2;
    LPC1114_CT32B1_MAT2 = LPC1114_GPIO3;
    LPC1114_CT32B1_MAT3 = LPC1114_GPIO4;
    LPC1114_CT32B0_CAP0 = LPC1114_GPIO5;

IMPLEMENTATION

END.
