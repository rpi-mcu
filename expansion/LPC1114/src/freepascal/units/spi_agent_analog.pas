{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Analog to Digital Converter input services                            }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_analog;

INTERFACE

  USES
    lpc11xx,
    spi_agent_transport,
    spi_agent_commands,
    spi_agent_exceptions,
    spi_agent_messages,
    spi_agent_pins;

  CONST
    LPC1114_ADC_SPAN     = 3.3;
    LPC1114_ADC_BITS     = 10;
    LPC1114_ADC_STEPS    = 1024;
    LPC1114_ADC_STEPSIZE = 0.00322265625;

  TYPE
    ANALOG_CHANNEL_t = 1 .. 5;

    ANALOG_VOLTAGE_t = Real;

    ANALOG_INPUT_t =
      CLASS
        CONSTRUCTOR create(adc_pin : GPIO_PIN_t);

        CONSTRUCTOR create(adc_channel : ANALOG_CHANNEL_t);

        FUNCTION get : ANALOG_VOLTAGE_t;
      PRIVATE
        pin : GPIO_PIN_t;
      END;

IMPLEMENTATION

  CONSTRUCTOR ANALOG_INPUT_t.create(adc_pin : GPIO_PIN_t);

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN
    INHERITED create;

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT);
    cmd.pin := ord(adc_pin);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
      END;

    Self.pin := adc_pin;
  END;

  CONSTRUCTOR ANALOG_INPUT_t.create(adc_channel : ANALOG_CHANNEL_t);

  VAR
    adc_pin : GPIO_PIN_t;
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN
    INHERITED create;

    adc_pin := GPIO_PIN_t(adc_channel - 1 + ord(LPC1114_AD1));

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT);
    cmd.pin := ord(adc_pin);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
      END;

    Self.pin := adc_pin;
  END;

  FUNCTION ANALOG_INPUT_t.get : ANALOG_VOLTAGE_t;

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN
    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_ANALOG);
    cmd.pin := ord(Self.pin);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
      END;

    { Return the analog measurement }

    get := resp.data*LPC1114_ADC_STEPSIZE;
  END;

END.
