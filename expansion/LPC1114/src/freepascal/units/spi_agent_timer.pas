{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ timer services                                                        }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_timer;

INTERFACE

USES
  spi_agent_transport,
  spi_agent_commands,
  spi_agent_exceptions,
  spi_agent_messages;

CONST
  PCLK_FREQUENCY = 48000000;

TYPE
  spiagent_timer_id_t =
   (LPC1114_CT32B0,
    LPC1114_CT32B1);

  spiagent_timer_mode_t =
   (LPC1114_TIMER_MODE_DISABLED,
    LPC1114_TIMER_MODE_RESET,
    LPC1114_TIMER_MODE_PCLK,
    LPC1114_TIMER_MODE_CAP0_RISING,
    LPC1114_TIMER_MODE_CAP0_FALLING,
    LPC1114_TIMER_MODE_CAP0_BOTH);

  spiagent_timer_capture_edge_t =
   (LPC1114_TIMER_CAPTURE_EDGE_DISABLED,
    LPC1114_TIMER_CAPTURE_EDGE_CAP0_RISING,
    LPC1114_TIMER_CAPTURE_EDGE_CAP0_FALLING,
    LPC1114_TIMER_CAPTURE_EDGE_CAP0_BOTH);

  spiagent_timer_capture_interrupt_t =
   (LPC1114_TIMER_CAPTURE_INTERRUPT_DISABLE,
    LPC1114_TIMER_CAPTURE_INTERRUPT_ENABLE);

  spiagent_timer_match_register_t =
   (LPC1114_TIMER_MATCH0,
    LPC1114_TIMER_MATCH1,
    LPC1114_TIMER_MATCH2,
    LPC1114_TIMER_MATCH3);

  spiagent_timer_match_output_action_t =
   (LPC1114_TIMER_MATCH_OUTPUT_DISABLED,
    LPC1114_TIMER_MATCH_OUTPUT_CLEAR,
    LPC1114_TIMER_MATCH_OUTPUT_SET,
    LPC1114_TIMER_MATCH_OUTPUT_TOGGLE);

  spiagent_timer_match_interrupt_t =
   (LPC1114_TIMER_MATCH_INTERRUPT_DISABLE,
    LPC1114_TIMER_MATCH_INTERRUPT_ENABLE);

  spiagent_timer_match_reset_t =
   (LPC1114_TIMER_MATCH_RESET_DISABLE,
    LPC1114_TIMER_MATCH_RESET_ENABLE);

  spiagent_timer_match_stop_t =
   (LPC1114_TIMER_MATCH_STOP_DISABLE,
    LPC1114_TIMER_MATCH_STOP_ENABLE);

  spiagent_timer_prescaler_t = dword;

  spiagent_timer_count_t = dword;

  TIMER_t =
    CLASS
      CONSTRUCTOR create
       (timer     : spiagent_timer_id_t);

      PROCEDURE ConfigureMode
       (mode      : spiagent_timer_mode_t);

      PROCEDURE ConfigurePrescaler
       (prescaler : spiagent_timer_prescaler_t);

      PROCEDURE ConfigureCapture
       (edge      : spiagent_timer_capture_edge_t;
        interrupt : spiagent_timer_capture_interrupt_t);

      PROCEDURE ConfigureMatch
       (reg       : spiagent_timer_match_register_t;
        value     : spiagent_timer_count_t;
        action    : spiagent_timer_match_output_action_t;
        interrupt : spiagent_timer_match_interrupt_t;
        reset     : spiagent_timer_match_reset_t;
        stop      : spiagent_timer_match_stop_t);

      FUNCTION Get : spiagent_timer_count_t;

      FUNCTION GetCapture : spiagent_timer_count_t;

      FUNCTION GetCaptureDelta : spiagent_timer_count_t;
    PRIVATE
      timer : spiagent_timer_id_t;
    END;

{*****************************************************************************}

IMPLEMENTATION

{$J-}

  CONST
    CounterAddresses : ARRAY [spiagent_timer_id_t] OF dword =
      ($40014008, $40018008);

    CaptureAddresses : ARRAY [spiagent_timer_id_t] OF dword =
      ($4001402C, $4001802C);

{*****************************************************************************}

  CONSTRUCTOR TIMER_t.create
   (timer     : spiagent_timer_id_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN
    INHERITED create;

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_INIT_TIMER);
    cmd.pin := ord(timer);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    Self.timer := timer;
  END;

{*****************************************************************************}

  PROCEDURE TIMER_t.ConfigureMode
   (mode  : spiagent_timer_mode_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_TIMER_MODE);
    cmd.pin := ord(Self.timer);
    cmd.data := ord(mode);

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

{*****************************************************************************}

  PROCEDURE TIMER_t.ConfigurePrescaler
   (prescaler : spiagent_timer_prescaler_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_TIMER_PRESCALER);
    cmd.pin := ord(Self.timer);
    cmd.data := prescaler;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

{*****************************************************************************}

  PROCEDURE TIMER_t.ConfigureCapture
   (edge      : spiagent_timer_capture_edge_t;
    interrupt : spiagent_timer_capture_interrupt_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_TIMER_CAPTURE);
    cmd.pin := ord(Self.timer);
    cmd.data := ord(edge) OR (ord(interrupt) SHL 4);

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

{*****************************************************************************}

  PROCEDURE TIMER_t.ConfigureMatch
   (reg       : spiagent_timer_match_register_t;
    value     : spiagent_timer_count_t;
    action    : spiagent_timer_match_output_action_t;
    interrupt : spiagent_timer_match_interrupt_t;
    reset     : spiagent_timer_match_reset_t;
    stop      : spiagent_timer_match_stop_t);

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0) + ord(reg);
    cmd.pin := ord(Self.timer);
    cmd.data := ord(action) OR (ord(interrupt) SHL 4) OR
      (ord(reset) SHL 5) OR (ord(stop) SHL 6);

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0_VALUE) + ord(reg);
    cmd.pin := ord(Self.timer);
    cmd.data := value;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;
  END;

{*****************************************************************************}

  FUNCTION TIMER_t.get : spiagent_timer_count_t;

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_SFR);
    cmd.pin := CounterAddresses[timer];
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    get := resp.data;
  END;

{*****************************************************************************}

  FUNCTION TIMER_t.GetCapture : spiagent_timer_count_t;

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_SFR);
    cmd.pin := CaptureAddresses[Self.timer];
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    GetCapture := resp.data;
  END;

{*****************************************************************************}

  FUNCTION TIMER_t.GetCaptureDelta : spiagent_timer_count_t;

  VAR
    cmd    : SPIAGENT_COMMAND_MSG_t;
    resp   : SPIAGENT_RESPONSE_MSG_t;
    error  : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_TIMER_CAPTURE_DELTA);
    cmd.pin := ord(Self.timer);
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' +
          errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' +
          errbuf);
      END;

    GetCaptureDelta := resp.data;
  END;
END.
