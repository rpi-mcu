{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ interrupt services                                                    }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_interrupt;

INTERFACE

USES
  initc,
  sysutils;

CONST

  { Raspberry Pi GPIO pins (on P1 expansion header) }

  RPI_GPIO4  = 4;	{ P1 Expansion Header pin 7 }
  RPI_INT    = RPI_GPIO4;

  { SPI Agent Firmware interrupt services }

  PROCEDURE spiagent_interrupt_enable(VAR error : integer); cdecl; external;

  PROCEDURE spiagent_interrupt_disable(VAR error : integer); cdecl; external;

  PROCEDURE spiagent_interrupt_wait(timeout : integer; VAR pin : integer; VAR error : integer); cdecl; external;

IMPLEMENTATION

{$linklib libspiagent}

END.
