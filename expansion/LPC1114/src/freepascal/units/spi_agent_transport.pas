{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ transport services                                                    }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_transport;

INTERFACE

  USES
    spi_agent_messages;

{$IF defined(WINDOWS) or defined(HTTPTRANSPORT)}
  { Windows has to use the HTTP services implemented below }

  PROCEDURE spiagent_open(servername:pchar; VAR error:integer);

  PROCEDURE  spiagent_command(VAR cmd:SPIAGENT_COMMAND_MSG_t;
    VAR resp:SPIAGENT_RESPONSE_MSG_t; VAR error:integer);

  PROCEDURE spiagent_close(VAR error:integer);
{$ELSE}
  { Unix clients use libspiagent.so }

  PROCEDURE spiagent_open(servername:pchar; VAR error:integer); cdecl; external;

  PROCEDURE spiagent_command(VAR cmd:SPIAGENT_COMMAND_MSG_t;
    VAR resp:SPIAGENT_RESPONSE_MSG_t; VAR error:integer); cdecl; external;

  PROCEDURE spiagent_close(VAR error:integer); cdecl; external;
{$ENDIF}

IMPLEMENTATION

{$IF defined(WINDOWS) or defined(HTTPTRANSPORT)}
  { Windows has to use the HTTP services implemented below }

  USES
    errno,
    fphttpclient,
    resolve,
    strings,
    sysutils,
    spi_agent_commands;

  VAR
    server : pchar = NIL;
    client : TFPHTTPClient = NIL;

  PROCEDURE spiagent_open(servername:pchar; VAR error:integer);

  VAR
    cmd        : SPIAGENT_COMMAND_MSG_t;
    resp       : SPIAGENT_RESPONSE_MSG_t;

  BEGIN

    { Check for already open server connection }

    IF server <> NIL THEN
      BEGIN
        error := EBUSY;
        EXIT;
      END;

    { Check for servername with HTTP scheme }

    IF strlicomp(servername, 'http://', 7) = 0 THEN
      servername := servername + 7;

    { Check for unimplemented transport scheme }

    IF strpos(servername, '://') <> NIL THEN
      BEGIN
        error := EINVAL;
        EXIT;
      END;

    { Resolve host name to IP address }

    WITH THostResolver.Create(NIL) DO
      BEGIN
        IF NOT NameLookup(servername) THEN
          BEGIN
            error := EIO;
            EXIT;
          END;

        server := StrAlloc(length(AddressAsString) + 1);
        StrPCopy(server, AddressAsString);
        Free;
      END;

    { Create the client object }

    client := TFPHTTPClient.Create(nil);

    { Ping the server }

    cmd.command := integer(SPIAGENT_CMD_NOP);
    cmd.pin := 0;
    cmd.data := 0;

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        EXIT;
      END;

    IF resp.error <> 0 THEN
      BEGIN
        error := resp.error;
        EXIT;
      END;

    error := EOK;
  END;

  PROCEDURE  spiagent_command(VAR cmd:SPIAGENT_COMMAND_MSG_t;
    VAR resp:SPIAGENT_RESPONSE_MSG_t; VAR error:integer);

  VAR
    request  : String;
    response : String;
    count    : integer;

  BEGIN

    { Check for open connection }

    IF server = NIL THEN
      BEGIN
        error := EBADF;
        EXIT;
      END;

    { Build HTTP request string }

    request := Format('http://%s:8081/SPIAGENT?%u,%u,%u',
      [server, cmd.command, cmd.pin, cmd.data]);

    { Issue request to the server }

    TRY
      response := client.Get(request);
    EXCEPT
      error := EIO;
      EXIT;
    END;

    { Decode results }

    TRY
      count := sscanf(response, '%d,%d,%d,%d,%d;',
        [@error, @resp.command, @resp.pin, @resp.data, @resp.error]);
    EXCEPT
      error := EIO;
      EXIT;
    END;

    { We should have decoded exactly 5 integers }

    IF count <> 5 THEN
      BEGIN
        error := EIO;
        EXIT;
      END;

    error := EOK;
  END;

  PROCEDURE spiagent_close(VAR error:integer);

  BEGIN

    { Check for open connection }

    IF server = NIL THEN
      BEGIN
        error := EBADF;
        EXIT;
      END;

    { Tear down the connection }

    strdispose(server);
    server := NIL;

    client.Free;
    client := NIL;

    error := EOK;
  END;
{$ELSE}
  { Unix clients use libspiagent.so }

  USES
    initc;

  {$linklib libspiagent}
{$ENDIF}
END.
