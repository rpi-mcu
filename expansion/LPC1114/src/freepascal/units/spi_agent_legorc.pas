{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ LEGO� Power Functions Remote Control output services                  }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_legorc;

INTERFACE

USES
  lpc11xx,
  spi_agent_transport,
  spi_agent_commands,
  spi_agent_exceptions,
  spi_agent_pins,
  spi_agent_messages;

TYPE
  LEGORC_CHANNEL_t   = 1 .. 4;

  LEGORC_MOTOR_t     =
   (ALLSTOP,
    MOTORA,
    MOTORB,
    COMBODIRECT,
    COMBOPWM);

  LEGORC_DIRECTION_t =
   (REVERSE,
    FORWARD);

  LEGORC_SPEED_t     = 0 .. 255;

  LEGORC_OUTPUT_t =
    CLASS
      CONSTRUCTOR create(pin : GPIO_PIN_t);

      PROCEDURE put(channel   : LEGORC_CHANNEL_t;
                    motor     : LEGORC_MOTOR_t;
                    direction : LEGORC_DIRECTION_t;
                    speed     : LEGORC_SPEED_t);

    PRIVATE
      pin : GPIO_PIN_t;
    END;

IMPLEMENTATION

CONSTRUCTOR LEGORC_OUTPUT_t.create(pin : GPIO_PIN_t);

VAR
  cmd    : SPIAGENT_COMMAND_MSG_t;
  resp   : SPIAGENT_RESPONSE_MSG_t;
  error  : integer;
  errbuf : string;

BEGIN
  INHERITED create;

  { Build the command structure }

  cmd.command := ord(SPIAGENT_CMD_CONFIGURE_GPIO_OUTPUT);
  cmd.pin := ord(pin);
  cmd.data := 0;

  { Issue command to the SPI Agent Firmware }

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      Str(error, errbuf);
      RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      Str(resp.error, errbuf);
      RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
    END;

  Self.pin := pin;
END;

PROCEDURE LEGORC_OUTPUT_t.put(channel   : LEGORC_CHANNEL_t;
                              motor     : LEGORC_MOTOR_t;
                              direction : LEGORC_DIRECTION_t;
                              speed     : LEGORC_SPEED_t);

VAR
  cmd : SPIAGENT_COMMAND_MSG_t;
  resp : SPIAGENT_RESPONSE_MSG_t;
  error : integer;
  errbuf : string;

BEGIN

  { Build the command structure }

  cmd.command := ord(SPIAGENT_CMD_PUT_LEGORC);
  cmd.pin := ord(Self.pin);
  cmd.data := speed OR (ord(direction) SHL 8) OR (ord(motor) SHL 16) OR (channel SHL 24);

  { Issue command to the SPI Agent Firmware }

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      Str(error, errbuf);
      RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      Str(resp.error, errbuf);
      RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
    END;
END;

END.
