{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ Special Function Register services                                    }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

UNIT spi_agent_sfr;

INTERFACE

USES
  spi_agent_transport,
  spi_agent_commands,
  spi_agent_exceptions,
  spi_agent_messages;

{ Define some well-known SFR's -- see the LPC11xx User Manual UM10398 }

  CONST
    LPC1114_DEVICE_ID = $400483F4;
    LPC1114_GPIO1DATA = $50010CFC;
    LPC1114_U0SCR     = $4000801C;

{ Define the SFR object class }

  TYPE
    SFR_t =
      CLASS
        CONSTRUCTOR create(addr : dword);

        FUNCTION get : dword;

        PROCEDURE put(data : dword);
      PRIVATE
        addr : dword;
      END;

IMPLEMENTATION

  CONSTRUCTOR SFR_t.create(addr : dword);

  BEGIN
    INHERITED create;

    IF (addr < $40000000) OR (addr > $501FFFFF) OR ((addr > $4007FFFF) AND (addr < $50000000)) THEN
      RAISE SpiAgentError.create('ERROR: Invalid SFR address');

    Self.addr := addr;
  END;

  FUNCTION SFR_t.get : dword;

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_GET_SFR);
    cmd.pin := Self.addr;
    cmd.data := 0;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
      END;

    { Return the SFR contents }

    get := resp.data;
  END;

  PROCEDURE SFR_t.put(data : dword);

  VAR
    cmd : SPIAGENT_COMMAND_MSG_t;
    resp : SPIAGENT_RESPONSE_MSG_t;
    error : integer;
    errbuf : string;

  BEGIN

    { Build the command structure }

    cmd.command := ord(SPIAGENT_CMD_PUT_SFR);
    cmd.pin := Self.addr;
    cmd.data := data;

    { Issue command to the SPI Agent Firmware }

    spiagent_command(cmd, resp, error);

    { Process errors }

    IF error <> 0 THEN
      BEGIN
        Str(error, errbuf);
        RAISE SpiAgentError.create('ERROR: spiagent_command() failed, error=' + errbuf);
      END;

    IF resp.error <> 0 THEN
      BEGIN
        Str(resp.error, errbuf);
        RAISE SpiAgentError.create('ERROR: SPI Agent Firmware returned error=' + errbuf);
      END;
  END;

END.
