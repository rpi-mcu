// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// loopback test program

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <curl/curl.h>

#include <spi-agent.h>

#define PROGNAME	(argv[0])
#define SERVERNAME	((argc >= 2) ? argv[1] : "localhost")
#define ITERATIONS	((argc == 3) ? atoi(argv[2]) : 15000)

static size_t min(size_t x, size_t y)
{
  if (x > y)
    return y;
  else
    return x;
}

static void strrtrim(char *s)
{
  int n = strlen(s);

  while ((n > 0) && isspace(s[n-1]))
  {
    s[n-1] = 0;
    n--;
  }
}

static char curlresponse[256];

static size_t curlcallback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
  size_t count = min(sizeof(curlresponse) - 1, size*nmemb);

  memset(curlresponse, 0, sizeof(curlresponse));
  memcpy(curlresponse, ptr, count);

  return size*nmemb;
}

static void spiagent_command_curl(CURL *ch, char *server, SPIAGENT_COMMAND_MSG_t *cmd,
  SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error)
{
  char URL[256];

  // Build URL

  memset(URL, 0, sizeof(URL));
  snprintf(URL, sizeof(URL), "%s?%u,%u,%u", server, cmd->command, cmd->pin, cmd->data);

  // Set URL

  if ((*error = curl_easy_setopt(ch, CURLOPT_URL, URL)))
  {
    fprintf(stderr, "ERROR: curl_easy_setopt() failed, %s\n", curl_easy_strerror(*error));
    *error = EIO;
    return;
  }

  // Fetch URL

  if ((*error = curl_easy_perform(ch)))
  {
    fprintf(stderr, "ERROR: curl_easy_perform() failed, %s\n", curl_easy_strerror(*error));
    *error = EIO;
    return;
  }

  // Sanitize the response

  curlresponse[sizeof(curlresponse)-1] = 0;	// Guarantee NUL terminator
  strrtrim(curlresponse);			// Strip trailing whitespace

  // Decode the response

  if (sscanf(curlresponse, "%d,%u,%u,%u,%u;", error, &resp->command,
    &resp->pin, &resp->data, &resp->error) != 5)
  {
    fprintf(stderr, "ERROR: sscanf() failed, invalid response\n");
    *error = EIO;
    return;
  }
}

int main(int argc, char *argv[])
{
  CURL *ch;
  char server[256];
  int32_t error;
  SPIAGENT_COMMAND_MSG_t command;
  SPIAGENT_RESPONSE_MSG_t response;
  time_t start, finish;
  int i;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

  if ((argc < 1) || (argc > 3))
  {
    fprintf(stderr, "Usage: %s [hostname] [iterations]\n", PROGNAME);
    exit(1);
  }

  // Build server prefix from the server name

  snprintf(server, sizeof(server), "http://%s:8081/SPIAGENT", SERVERNAME);

  // Initialize libcurl

  ch = curl_easy_init();
  if (ch == NULL)
  {
    fprintf(stderr, "ERROR: cural_easy_init() failed\n");
    exit(1);
  }

  // Set responsecallback

  if ((error = curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, curlcallback)))
  {
    fprintf(stderr, "ERROR: curl_easy_setopt() failed, %s\n", curl_easy_strerror(error));
    exit(1);
  }

  // Issue some SPI transactions

  puts("Issuing some SPI transactions...\n");

  // Build command message, with NOP command

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_NOP;
  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%d pin:%d data:%d error:%d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with LOOPBACK command

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_LOOPBACK;
  command.pin = 2;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%d pin:%d data:%d error:%d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with illegal pin number

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = 99;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%d pin:%d data:%d error:%d\n",
    response.command, response.pin, response.data, response.error);

  // Build command message, with illegal command field

  memset(&command, 0, sizeof(command));
  command.command = 99;
  command.pin = 2;
  command.data = 3;

  memset(&response, 0, sizeof(response));

  // Perform the SPI transfers

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  // Display the response from the slave MCU

  printf("Response: command:%d pin:%d data:%d error:%d\n",
    response.command, response.pin, response.data, response.error);

  // Query the LPC1114 firmware version

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_NOP;

  memset(&response, 0, sizeof(response));

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: NOP operation failed, %s\n", strerror(response.error));
    exit(1);
  }

  printf("\nThe LPC1114 firmware version is %d\n", response.data);

  // Query the LPC1114 device ID

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_SFR;
  command.pin = 0x400483F4; // LPC1114 Device ID register

  memset(&response, 0, sizeof(response));

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: SFR read from address %d failed, %s\n",
      response.pin, strerror(response.error));
    exit(1);
  }

  printf("The LPC1114 device ID is        %08X\n", response.data);

  // Query the LPC1114 LED state

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = LPC1114_LED;

  memset(&response, 0, sizeof(response));

  spiagent_command_curl(ch, server, &command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: GPIO read from pin %d failed, %s\n",
      response.pin, strerror(response.error));
    exit(1);
  }

  printf("The LED is                      %s\n\n", response.data ? "ON" : "OFF");

  // Perform SPI loopback commands as fast as possible to stress test the SPI
  // interface

  printf("Starting %d SPI agent loopback test transactions...\n\n", ITERATIONS);
  fflush(stdout);

  time(&start);

  for (i = 0; i < ITERATIONS; i++)
  {
    // Build the command message

    memset(&command, 0, sizeof(command));
    command.command = SPIAGENT_CMD_LOOPBACK;
    command.pin = i*17;
    command.data = i*19;

    memset(&response, 0, sizeof(response));

    // Perform the SPI transfers

    spiagent_command_curl(ch, server, &command, &response, &error);

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_command_curl() failed, %s\n", strerror(error));
      break;
    }

    // Display the response from the slave MCU

    if ((response.command != command.command) ||
        (response.pin != command.pin) ||
        (response.data != command.data) ||
        (response.error != 0))
    {
      printf("Iteration:%-6d Response: command:%d pin:%d data:%d error:%d\n",
        i, response.command, response.pin, response.data, response.error);
    }
  }

  time(&finish);

  // Display statistics

  double deltat = finish - start;
  double rate = i / deltat;
  double cycletime = deltat / i;

  printf("Performed %d loopback tests in %ld seconds\n", i, finish - start);
  printf("  %1.1f iterations per second\n", rate);
  printf("  %1.1f microseconds per iteration\n", cycletime*1.0E6);

  // Cleanup

  curl_easy_cleanup(ch);
  exit(0);
}
