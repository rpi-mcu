{ Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware }
{ loopback test program                                                 }

{ Copyright (C)2014-2018, Philip Munts, President, Munts AM Corp.             }
{                                                                             }
{ Redistribution and use in source and binary forms, with or without          }
{ modification, are permitted provided that the following conditions are met: }
{                                                                             }
{ * Redistributions of source code must retain the above copyright notice,    }
{   this list of conditions and the following disclaimer.                     }
{                                                                             }
{ THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" }
{ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   }
{ IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  }
{ ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   }
{ LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         }
{ CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        }
{ SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    }
{ INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     }
{ CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     }
{ ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  }
{ POSSIBILITY OF SUCH DAMAGE.                                                 }

PROGRAM spi_agent_test(input, output);

USES
  strings,
  sysutils,
  dateutils,
  fphttpclient,
  errno,
  spi_agent_commands,
  spi_agent_messages,
  spi_agent_pins;

CONST
  DEFAULTSERVER     = 'localhost';
  DEFAULTITERATIONS = 10000;

VAR
  servername : pchar;
  client     : TFPHTTPClient = NIL;
  iterations : integer;
  error      : integer;
  cmd        : SPIAGENT_COMMAND_MSG_t;
  resp       : SPIAGENT_RESPONSE_MSG_t;
  starttime  : TDateTime;
  i          : integer;
  endtime    : TDateTime;
  deltat     : double;
  rate       : double;
  cycletime  : double;

  PROCEDURE  spiagent_command(VAR cmd:SPIAGENT_COMMAND_MSG_t;
    VAR resp:SPIAGENT_RESPONSE_MSG_t; VAR error:integer);

  VAR
    request  : String;
    response : String;
    count    : integer;

  BEGIN

    { Build HTTP request string }

    request := Format('http://%s:8081/SPIAGENT?%u,%u,%u',
      [servername, cmd.command, cmd.pin, cmd.data]);

    { Issue request to the server }

    TRY
      response := client.Get(request);
    EXCEPT
      error := EIO;
      EXIT;
    END;

    { Decode results }

    TRY
      count := sscanf(response, '%d,%d,%d,%d,%d;',
        [@error, @resp.command, @resp.pin, @resp.data, @resp.error]);
    EXCEPT
      error := EIO;
      EXIT;
    END;

    { We should have decoded exactly 5 integers }

    IF count <> 5 THEN
      BEGIN
        error := EIO;
        EXIT;
      END;

    error := EOK;
  END;

BEGIN
  writeln;
  writeln('Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test');
  writeln;

  { Analyze command line parameters }

  CASE system.argc OF
    1 :
      BEGIN
        servername := DEFAULTSERVER;
        iterations := DEFAULTITERATIONS;
      END;

    2 :
      BEGIN
        servername := argv[1];
        iterations := DEFAULTITERATIONS;
      END;

    3 :
      BEGIN
        servername := argv[1];
        iterations := StrToInt(argv[2]);
      END;

  ELSE
    writeln('Usage: ', system.argv[0], ' [hostname] [iterations]');
    writeln;
    halt(1);
  END;

  client := TFPHTTPClient.Create(nil);

  writeln('Issuing some SPI transactions...');
  writeln;

  { Issue NOP command to the SPI Agent Firmware }

  cmd.command := integer(SPIAGENT_CMD_NOP);
  cmd.pin := 0;
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(3);
    END;

  { Display results }

  writeln('Response: command:', resp.command, ' pin:', resp.pin,
    ' data:', resp.data, ' error:', resp.error);

  { Issue loopback command to the SPI Agent Firmware }

  cmd.command := integer(SPIAGENT_CMD_LOOPBACK);
  cmd.pin := 2;
  cmd.data := 3;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(4);
    END;

  { Display results }

  writeln('Response: command:', resp.command, ' pin:', resp.pin,
    ' data:', resp.data, ' error:', resp.error);

  { Issue command with illegal pin number to the SPI Agent Firmware }

  cmd.command := integer(SPIAGENT_CMD_GET_GPIO);
  cmd.pin := 99;
  cmd.data := 3;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(5);
    END;

  { Display results }

  writeln('Response: command:', resp.command, ' pin:', resp.pin,
    ' data:', resp.data, ' error:', resp.error);

  { Issue illegal command to the SPI Agent Firmware }

  cmd.command := 99;
  cmd.pin := 2;
  cmd.data := 3;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(6);
    END;

  { Display results }

  writeln('Response: command:', resp.command, ' pin:', resp.pin,
    ' data:', resp.data, ' error:', resp.error);
  writeln;

  { Query the LPC1114 firmware version }

  cmd.command := integer(SPIAGENT_CMD_NOP);
  cmd.pin := 0;
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(7);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      writeln('ERROR: SPI Agent Firmware returned error ', resp.error);
      writeln;
      halt(8);
    END;

  { Display results }

  writeln('The LPC1114 firmware version is ', resp.data);

  { Query the LPC1114 device ID }

  cmd.command := integer(SPIAGENT_CMD_GET_SFR);
  cmd.pin := $400483F4;
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(9);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      writeln('ERROR: SPI Agent Firmware returned error ', resp.error);
      writeln;
      halt(10);
    END;

  { Display results }

  writeln('The LPC1114 device ID is        ', IntToHex(resp.data, 8));

  { Query the expansion board LED state }

  cmd.command := integer(SPIAGENT_CMD_GET_GPIO);
  cmd.pin := integer(LPC1114_LED);
  cmd.data := 0;

  spiagent_command(cmd, resp, error);

  { Process errors }

  IF error <> 0 THEN
    BEGIN
      writeln('ERROR: spiagent_command() failed, error=', error);
      writeln;
      halt(11);
    END;

  IF resp.error <> 0 THEN
    BEGIN
      writeln('ERROR: SPI Agent Firmware returned error ', resp.error);
      writeln;
      halt(12);
    END;

  { Display results }

  IF resp.data = 0 THEN
    writeln('The expansion board LED is      OFF')
  ELSE
    writeln('The expansion board LED is      ON');

  writeln;

  { Perform SPI loopback commands as fast as possible to stress test the SPI }
  { interface }

  writeln('Starting ', iterations, ' SPI agent loopback test transactions...');

  starttime := TimeOf(Now);

  FOR i := 1 to iterations DO
    BEGIN
      { Issue loopback command to the SPI Agent Firmware }

      cmd.command := integer(SPIAGENT_CMD_LOOPBACK);
      cmd.pin := i*17;
      cmd.data := i*19;

      spiagent_command(cmd, resp, error);

      { Process errors }

      IF error <> 0 THEN
        BEGIN
          writeln('ERROR: spiagent_command() failed, error=', error);
          writeln;
          halt(13);
        END;

      { Check for loopback error }

      IF (resp.command <> cmd.command) OR (resp.pin <> cmd.pin) OR
         (resp.data <> cmd.data) OR (resp.error <> 0) THEN
        writeln('Iteration: ', i, ' Response: command:', resp.command,
          ' pin:', resp.pin, ' data:', resp.data, ' error:', resp.error);
    END;

  endtime := TimeOf(Now);

  { Display statistics }

  deltat := SecondSpan(starttime, endtime);
  rate := i/deltat;
  cycletime := deltat/i;

  writeln;
  writeln('Performed ', iterations, ' loopback tests in ', deltat : 1 : 1, ' seconds');
  writeln('  ', rate : 1 : 1, ' iterations per second');
  writeln('  ', cycletime*1E6 : 1 : 1, ' microseconds per iteration');
  writeln;
END.
