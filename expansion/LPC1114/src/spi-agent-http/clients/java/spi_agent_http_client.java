// Java test client for the Raspberry Pi LPC1114 I/O Processor
// Expansion Board SPI agent firmware HTTP server

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

import org.apache.commons.io.*;

import static SPIAgent.commands.*;
import static SPIAgent.pins.*;

public class spi_agent_http_client
{
  static Pattern DELIMITERS = Pattern.compile("[,;]");

  public static void spiagent_command_http(String server, int cmd[], int resp[])
  {
    String request = server + "?" + Integer.toString(cmd[0]) + "," +
      Integer.toString(cmd[1]) + "," + Integer.toString(cmd[2]);

    try
    {
      String response = IOUtils.toString(new URL(request)).trim();
      Scanner s = new Scanner(response);
      s.useDelimiter(DELIMITERS);

      int error = s.nextInt();

      if (error != 0)
      {
        System.out.println("ERROR: ioctl() failed, error=" + Integer.toString(error));
        System.exit(1);
      }

      resp[0] = s.nextInt();
      resp[1] = s.nextInt();
      resp[2] = s.nextInt();
      resp[3] = s.nextInt();
      s.close();
    }
    catch (MalformedURLException e)
    {
      System.out.println("ERROR: Malformed URL: " + e.toString());
      System.exit(1);
    }
    catch (IOException e)
    {
      System.out.println("ERROR: I/O error: " + e.toString());
      System.exit(1);
    }
    catch (InputMismatchException e)
    {
      System.out.println("ERROR: invalid response");
      System.exit(1);
    }
  }

  public static void main(String[] args)
  {
    String server = new String();
    int iterations = 5000;
    int[] cmd = new int[3];
    int[] resp = new int[4];

    System.out.println("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

    // Build HTTP server URL

    switch (args.length)
    {
      case 0 :
        server = "http://localhost:8081/SPIAGENT";
        break;

      case 1 :
        server = "http://" + args[0] + ":8081/SPIAGENT";
        break;

      case 2 :
        server = "http://" + args[0] + ":8081/SPIAGENT";
        iterations = Integer.parseInt(args[1]);
        break;

      default :
        System.out.println("Usage: java -jar spi_agent_test_http.jar [hostname] [iterations]");
        System.exit(1);
        break;
    }

    System.out.println("Issuing some SPI transactions...\n");

    cmd[0] = SPIAGENT_CMD_NOP.ordinal();
    cmd[1] = 0;
    cmd[2] = 0;
    spiagent_command_http(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd[0] = SPIAGENT_CMD_LOOPBACK.ordinal();
    cmd[1] = 2;
    cmd[2] = 3;
    spiagent_command_http(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd[0] = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd[1] = 99;
    cmd[2] = 3;
    spiagent_command_http(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    cmd[0] = 99;
    cmd[1] = 2;
    cmd[2] = 3;
    spiagent_command_http(server, cmd, resp);
    System.out.print("Response: command:" + String.format("%-4d", resp[0]));
    System.out.print(" pin:" + String.format("%-4d", resp[1]));
    System.out.print(" data:" + String.format("%-4d", resp[2]));
    System.out.println(" error:" + String.format("%-4d", resp[3]));

    // Query the LPC1114 firmware firmware version

    cmd[0] = SPIAGENT_CMD_NOP.ordinal();
    cmd[1] = 0;
    cmd[2] = 0;
    spiagent_command_http(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("\nThe LPC1114 firmware version is " +
        String.format("%d", resp[2]));

    // Query the LPC1114 device I D

    cmd[0] = SPIAGENT_CMD_GET_SFR.ordinal();
    cmd[1] = 0x400483F4;
    cmd[2] = 0;
    spiagent_command_http(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("The LPC1114 device ID is        " +
        String.format("%08X", resp[2]));

    // Query the expansion board LED state

    cmd[0] = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd[1] = LPC1114_LED;
    cmd[2] = 0;
    spiagent_command_http(server, cmd, resp);
    if (resp[3] == 0)
      System.out.println("The expansion board LED is      " +
        ((resp[2] == 1) ? "ON" : "OFF"));

    // Perform loopback torture test

    System.out.println("\nStarting " + Integer.toString(iterations) +
      " SPI agent loopback test transactions...\n");

    long starttime = System.nanoTime();

    for (int i = 1; i <= iterations; i++)
    {
      cmd[0] = SPIAGENT_CMD_LOOPBACK.ordinal();
      cmd[1] = i*17;
      cmd[2] = i*19;
      spiagent_command_http(server, cmd, resp);

      if ((resp[0] != cmd[0]) || (resp[1] != cmd[1]) || (resp[2] != cmd[2]) || (resp[3] != 0))
      {
        System.out.println("Iteration:" + Integer.toString(i) + " Response: command:" +
        Integer.toString(resp[0]) + " pin:" + Integer.toString(resp[1]) + " data:" +
        Integer.toString(resp[2]) + " error:" + Integer.toString(resp[3]));
      }
    }

    long endtime = System.nanoTime();

    // Display statistics

    double deltat = (endtime - starttime)*1.0e-9;
    double rate = iterations/deltat;
    double cycletime = deltat/iterations;

    System.out.println("Performed " + Integer.toString(iterations) + " loopback tests in " +
      String.format("%1.1f", deltat) + " seconds");

    System.out.println("  " + String.format("%1.1f", rate) + " iterations per second");

    System.out.println("  " + String.format("%1.1f", cycletime*1.0E6) + " microseconds per iteration" + "\n");
  }
}
