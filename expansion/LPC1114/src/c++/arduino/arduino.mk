# Arduino makefile definitions

# Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ARDUINOEXE	?= arduino

ifeq ($(findstring CYGWIN, $(shell uname)), CYGWIN)
ARDUINOFLAGS	+= --pref sketchbook.path=`cygpath -a -d $(ARDUINOSRC)`
else
ARDUINOFLAGS	+= --pref sketchbook.path=$(realpath $(ARDUINOSRC))
endif

# Define a pattern rule to verify a sketch

%.verify: %.ino
	$(MAKE) -C $(ARDUINOSRC)/libraries
ifeq ($(findstring CYGWIN, $(shell uname)), CYGWIN)
	$(ARDUINOEXE) $(ARDUINOFLAGS) --verify `cygpath -a -d $?`
else
	$(ARDUINOEXE) $(ARDUINOFLAGS) --verify $(realpath $?)
endif

# Define a pattern rule to upload a sketch

%.upload: %.ino
	$(MAKE) -C $(ARDUINOSRC)/libraries
ifeq ($(findstring CYGWIN, $(shell uname)), CYGWIN)
	$(ARDUINOEXE) $(ARDUINOFLAGS) --upload `cygpath -a -d $?`
else
	$(ARDUINOEXE) $(ARDUINOFLAGS) --upload $(realpath $?)
endif

# Define a placeholder default target

arduino_mk_default:
	@echo ERROR: You must explicitly specify a make target!
	@exit 1

# Remove working files

arduino_mk_clean:
	$(MAKE) -C $(ARDUINOSRC)/libraries clean

arduino_mk_reallyclean: arduino_mk_clean

arduino_mk_distclean: arduino_mk_reallyclean
