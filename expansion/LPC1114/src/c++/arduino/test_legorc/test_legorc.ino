// LPC1114 I/O Processor LEGO Remote Control Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Arduino Uno R3
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C and D5
// Seeed Studios Infrared Emitter Grove Module, plugged into IOP J7

#include <LPC1114_IOP.h>

LPC1114_IOP::Transport_Class iop(LPC1114_IOP::Transport_Class::DefaultAddress, 5);
LPC1114_IOP::LEGORC IRED;

int32_t motor = LEGORC_MOTORA;
int32_t direction = LEGORC_FORWARD;
int32_t speed = 0;
int32_t step = 1;

void setup()
{
  Serial.begin(115200);
  Serial.println("LPC1114 I/O Processor LEGO Remote Control Test\n");

  IRED.Init(&iop, LPC1114_GPIO6);
}

void loop()
{
  Serial.print("Motor: ");
  Serial.print(motor, DEC);
  Serial.print(" Direction: ");
  Serial.print(direction, DEC);
  Serial.print(" Speed: ");
  Serial.println(speed, DEC);
  Serial.flush();

  IRED.Put(1, motor, direction, speed);

  speed += step;

  if (speed > 7)
  {
    speed = 7;
    step = -1;
  }

  if (speed < 0)
  {
    speed = 0;
    step = 1;

    if (direction == LEGORC_FORWARD)
    {
      direction = LEGORC_REVERSE;
    }
    else
    {
      direction = LEGORC_FORWARD;

      if (motor == LEGORC_MOTORA)
      {
        motor = LEGORC_MOTORB;
      }
      else
      {
        motor = LEGORC_MOTORA;
      }
    }
  }

  delay(1000);
}
