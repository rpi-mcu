// LPC1114 I/O Processor ADC Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Arduino Uno R3
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C
// Seeed Studios Slide Potentiometer Grove Module

#include <LPC1114_IOP.h>

LPC1114_IOP::Transport_Class iop;
LPC1114_IOP::ADC AIN1;
LPC1114_IOP::ADC AIN2;
LPC1114_IOP::ADC AIN3;
LPC1114_IOP::ADC AIN4;
LPC1114_IOP::ADC AIN5;

void setup()
{
  Serial.begin(115200);
  Serial.println("LPC1114 I/O Processor ADC Test\n");

  AIN1.Init(&iop, LPC1114_AD1);
  AIN2.Init(&iop, LPC1114_AD2);
  AIN3.Init(&iop, LPC1114_AD3);
  AIN4.Init(&iop, LPC1114_AD4);
  AIN5.Init(&iop, LPC1114_AD5);
}

void loop()
{
  // Sample analog inputs

  Serial.print("AIN1: ");
  Serial.print(double(AIN1), 2);

  Serial.print(" AIN2: ");
  Serial.print(double(AIN2), 2);

  Serial.print(" AIN3: ");
  Serial.print(double(AIN3), 2);

  Serial.print(" AIN4: ");
  Serial.print(double(AIN4), 2);

  Serial.print(" AIN5: ");
  Serial.println(double(AIN5), 2);

  delay(2000);
}
