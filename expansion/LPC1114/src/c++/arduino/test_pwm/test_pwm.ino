// LPC1114 I/O Processor PWM Output Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Arduino Uno R3
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C

#include <LPC1114_IOP.h>

LPC1114_IOP::Transport_Class iop;
LPC1114_IOP::PWM PWM1;
LPC1114_IOP::PWM PWM2;
LPC1114_IOP::PWM PWM3;
LPC1114_IOP::PWM PWM4;

double dutycycle = LPC1114_PWM_DUTY_MIN;
double step = 1.0;

void setup()
{
  Serial.begin(115200);
  Serial.print("LPC1114 I/O Processor PWM Output Test\r\n\n");

  PWM1.Init(&iop, LPC1114_PWM1, 1000);
  PWM2.Init(&iop, LPC1114_PWM2, 1000);
  PWM3.Init(&iop, LPC1114_PWM3, 1000);
  PWM4.Init(&iop, LPC1114_PWM4, 1000);
}

void loop()
{
  Serial.print("PWM duty cycle: ");
  Serial.print(dutycycle, 0);
  Serial.println(" %");

  PWM1 = dutycycle;
  PWM2 = dutycycle;
  PWM3 = dutycycle;
  PWM4 = dutycycle;

  dutycycle += step;

  if (dutycycle < LPC1114_PWM_DUTY_MIN)
  {
    dutycycle = LPC1114_PWM_DUTY_MIN;
    step = -step;
  }

  if (dutycycle > LPC1114_PWM_DUTY_MAX)
  {
    dutycycle = LPC1114_PWM_DUTY_MAX;
    step = -step;
  }

  delay(50);
}
