// LPC1114 I/O Processor Interrupt Test

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Nucleo-411RE development board
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C and D5
// Seeed Studios Button Grove Module, plugged into IOP J4 (GPIO0)

#include <mbed.h>

#include <LPC1114_IOP.h>

// Configure an event output signal for performance testing

DigitalOut EventOut(D7, false);

// LPC1114 interrupt event queue

Queue<uint32_t, sizeof(uint32_t)> queue;

// LPC1114 interrupt handler

void LPC1114_INT_handler(void)
{
  // Pulse the event out pin (Arduino D7)
  // This will usually occur about 9.6 us after the button is pressed
  // (measured with a digital oscilloscope)

  EventOut = true;
  EventOut = false;

  static uint32_t msg = 0;
  queue.put(&++msg, 0, 0);
}

int main(void)
{
  BufferedSerial console(SERIAL_TX, SERIAL_RX, 115200);

  printf("\033[H\033[2JLPC1114 I/O Processor Interrupt Test (" __DATE__ " "
    __TIME__ ")\r\n\n");
  fflush(stdout);

  // Configure the STM32 interrupt input (will be pulsed high by the LPC1114)

  InterruptIn LPC1114_INT_input(D6);

  // Attach the interrupt handler

  LPC1114_INT_input.fall(LPC1114_INT_handler);

  // Configure LPC1114 GPIO0 to interrupt on each rising edge

  LPC1114_IOP::Transport_Class iop;

  LPC1114_IOP::GPIO button(&iop, LPC1114_GPIO0,
    LPC1114_GPIO_MODE_INPUT_PULLDOWN);

  button.ConfigureInterruptEdge(LPC1114_GPIO_INTERRUPT_RISING);

  // Event loop

  for (;;)
  {
    osEvent e = queue.get(5000);

    if (e.status == osEventMessage)
    {
      uint32_t *m = (uint32_t *) e.value.p;

      // Pulse the event out pin (Arduino D7)
      // This will usually occur 20.6 us after the button is pressed
      // (measured with a digital oscilloscope)

      EventOut = true;
      EventOut = false;

      printf("PRESS %ld\r\n", *m);
      fflush(stdout);
      continue;
    }

    printf("Nothing happened!\r\n");
    fflush(stdout);
  }
}
