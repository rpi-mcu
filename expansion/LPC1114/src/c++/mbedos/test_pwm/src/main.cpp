// LPC1114 I/O Processor PWM Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Nucleo-411RE development board
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C

#include <mbed.h>

#include <LPC1114_IOP.h>

int main(void)
{
  BufferedSerial console(SERIAL_TX, SERIAL_RX, 115200);
  double step = 0.2;

  printf("\033[H\033[2JLPC1114 I/O Processor PWM Test (" __DATE__ " "
    __TIME__ ")\r\n\n");
  fflush(stdout);

  LPC1114_IOP::Transport_Class iop;

  // Create PWM output objects

  LPC1114_IOP::PWM PWM1(&iop, LPC1114_PWM1, 1000);
  LPC1114_IOP::PWM PWM2(&iop, LPC1114_PWM2, 1000);
  LPC1114_IOP::PWM PWM3(&iop, LPC1114_PWM3, 1000);
  LPC1114_IOP::PWM PWM4(&iop, LPC1114_PWM4, 1000);

  // Sweep PWM output dutycycles

  double dutycycle = LPC1114_PWM_DUTY_MIN;

  for (;;)
  {
    printf("\rPWM duty cycle: %2.1f %%  ", dutycycle);
    fflush(stdout);

    PWM1 = dutycycle;
    PWM2 = dutycycle;
    PWM3 = dutycycle;
    PWM4 = dutycycle;

    dutycycle += step;

    if (dutycycle < LPC1114_PWM_DUTY_MIN)
    {
      dutycycle = LPC1114_PWM_DUTY_MIN;
      step = -step;
    }

    if (dutycycle > LPC1114_PWM_DUTY_MAX)
    {
      dutycycle = LPC1114_PWM_DUTY_MAX;
      step = -step;
    }

    ThisThread::sleep_for(10ms);
  }
}
