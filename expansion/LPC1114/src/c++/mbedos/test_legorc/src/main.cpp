// LPC1114 I/O Processor LEGO Power Functions Remote Control Test

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Nucleo-411RE development board
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module plugged into I2C and D5
// Seeed Studios Infrared Emitter Grove Module plugged into IOP J7 (GPIO6)

#include <mbed.h>

#include <LPC1114_IOP.h>

int main(void)
{
  BufferedSerial console(SERIAL_TX, SERIAL_RX, 115200);

  printf("\033[H\033[2JLPC1114 I/O Processor LEGO Power Functions Remote "
    "Control Test (" __DATE__ " " __TIME__ ")\r\n\n");
  fflush(stdout);

  LPC1114_IOP::Transport_Class iop(new I2C(I2C_SDA, I2C_SCL),
    LPC1114_IOP::Transport_Class::DefaultAddress, D5);

  LPC1114_IOP::LEGORC IRED(&iop, LPC1114_GPIO6);

  int32_t motor;
  int32_t direction;
  int32_t speed;

  for (;;)
  {
    for (motor = LEGORC_MOTORA; motor <= LEGORC_MOTORB; motor++)
      for (direction = LEGORC_FORWARD; direction >= LEGORC_REVERSE; direction--)
      {
        for (speed = 0; speed <= 7; speed++)
        {
          printf("Motor => %ld, direction => %ld, speed => %ld\r\n",
            motor, direction, speed);
          fflush(stdout);

          IRED.Put(1, motor, direction, speed);
          ThisThread::sleep_for(1s);
        }

        ThisThread::sleep_for(1s);

        for (speed = 7; speed >= 0; speed--)
        {
          printf("Motor => %ld, direction => %ld, speed => %ld\r\n",
            motor, direction, speed);
          fflush(stdout);

          IRED.Put(1, motor, direction, speed);
          ThisThread::sleep_for(1s);
        }

        ThisThread::sleep_for(1s);
      }
  }
}
