// LPC1114 I/O Processor LED Test

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Nucleo-411RE development board
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C

#include <mbed.h>

#include <LPC1114_IOP.h>

int main(void)
{
  BufferedSerial console(SERIAL_TX, SERIAL_RX, 115200);

  printf("\033[H\033[2JLPC1114 I/O Processor LED Test (" __DATE__ " "
    __TIME__ ")\r\n\n");
  fflush(stdout);

  LPC1114_IOP::Transport_Class iop;
  LPC1114_IOP::GPIO LED(&iop, LPC1114_LED, LPC1114_GPIO_OUTPUT, false);

  for (;;)
  {
    printf("Turning LED %s\r\n", LED ? "OFF" : "ON");
    fflush(stdout);

    LED = !LED;
    ThisThread::sleep_for(1s);
  }
}
