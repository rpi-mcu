// LPC1114 I/O Processor Servo Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Hardware setup:
//
// Nucleo-411RE development board
// Seeed Studios Arduino Base Shield V2, switched to 3.3V
// Munts Technologies LPC1114 I/O Processor Grove Module, plugged into I2C
// Parallax Standard Servo

#include <mbed.h>

#include <LPC1114_IOP.h>

int main(void)
{
  BufferedSerial console(SERIAL_TX, SERIAL_RX, 115200);

  printf("\033[H\033[2JLPC1114 I/O Processor Servo Test (" __DATE__ " "
    __TIME__ ")\r\n\n");
  fflush(stdout);

  LPC1114_IOP::Transport_Class iop;

  // Create servo output objects

  LPC1114_IOP::Servo Servo1(&iop, LPC1114_PWM1);
  LPC1114_IOP::Servo Servo2(&iop, LPC1114_PWM2);
  LPC1114_IOP::Servo Servo3(&iop, LPC1114_PWM3);
  LPC1114_IOP::Servo Servo4(&iop, LPC1114_PWM4);

  // Wait for servos to stabilize at the neutral position

  ThisThread::sleep_for(200ms);

  // Sweep servos back and forth

  double position = LPC1114_SERVO_NEUTRAL;
  double step = 0.01;

  for (;;)
  {
    printf("\rServo position: %-5.2f  ", position);
    fflush(stdout);

    Servo1 = position;
    Servo2 = position;
    Servo3 = position;
    Servo4 = position;

    position += step;

    if (position > LPC1114_SERVO_MAX)
    {
      position = LPC1114_SERVO_MAX;
      step = -step;
    }

    if (position < LPC1114_SERVO_MIN)
    {
      position = LPC1114_SERVO_MIN;
      step = -step;
    }
  }
}
