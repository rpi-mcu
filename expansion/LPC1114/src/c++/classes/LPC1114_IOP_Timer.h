// LPC1114 I/O Processor 32-bit Counter/Timer services

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef _LPC1114_IOP_TIMER_H_
#define _LPC1114_IOP_TIMER_H_

#include "LPC1114_IOP_Common.h"

namespace LPC1114_IOP
{
  struct Timer
  {
    // Default parameterless constructor -- The resulting object will not be
    // usable until reconstructed or Init() has been called!
    Timer(void);

    // Construct a timer object
    Timer(Transport server, uint32_t timer);

    // Initialize a timer object
    void Init(Transport server, uint32_t timer);

    // Configure timer mode
    void ConfigureMode(uint32_t mode);

    // Configure prescaler
    void ConfigurePrescaler(uint32_t divisor);

    // Configure capture edge
    void ConfigureCapture(uint32_t edge, bool intr);

    // Configure match register
    void ConfigureMatch(uint32_t match, uint32_t value, uint32_t action,
      bool intr, bool reset, bool stop);

    // Get current timer value
    uint32_t Get(void);

    // Get latest capture value
    uint32_t GetCapture(void);

    // Get latest capture delta value
    uint32_t GetCaptureDelta(void);

    // Get current timer value
    operator uint32_t(void);

  private:

    Transport server;
    uint32_t timer;
  };
}

#endif
