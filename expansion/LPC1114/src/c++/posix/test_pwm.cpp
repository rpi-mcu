// Raspberry Pi LPC1114 I/O Processor Expansion Board PWM Output Test

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstdio>
#include <cstdlib>
#include <string>
#include <unistd.h>

#include <LPC1114_IOP.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board PWM Output Test\n");

  try
  {
    LPC1114_IOP::Transport_Class server(SERVERNAME);

    LPC1114_IOP::PWM PWM1(&server, LPC1114_PWM1);
    LPC1114_IOP::PWM PWM2(&server, LPC1114_PWM2);
    LPC1114_IOP::PWM PWM3(&server, LPC1114_PWM3);
    LPC1114_IOP::PWM PWM4(&server, LPC1114_PWM4);

    puts("Press CONTROL-C to quit\n");

    for (;;)
    {
      double d;

      for (d = LPC1114_PWM_DUTY_MIN; d <= LPC1114_PWM_DUTY_MAX; d += 1.0)
      {
        PWM1 = d;
        PWM2 = d;
        PWM3 = d;
        PWM4 = d;

        usleep(50000);
      }

      for (d = LPC1114_PWM_DUTY_MAX; d >= LPC1114_PWM_DUTY_MIN; d -= 1.0)
      {
        PWM1 = d;
        PWM2 = d;
        PWM3 = d;
        PWM4 = d;

        usleep(50000);
      }
    }
  }

  catch (std::string msg)
  {
    fprintf(stderr, "EXCEPTION: %s\n\n", msg.c_str());
    exit(1);
  }
}
