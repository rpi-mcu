// Raspberry Pi LPC1114 I/O Processor Expansion Board LED
// LEGO® Power Functions Remote Control test program

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstdio>
#include <cstdlib>
#include <string>

#include <LPC1114_IOP.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  char *servername;
  uint32_t channel;
  uint32_t motor;
  uint32_t direction;
  uint32_t speed;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board LEGO® RC Test\n");

  // Extract command line arguments

  if (argc == 5)
  {
    servername = (char *) "localhost";
    channel = atoi(argv[1]);
    motor = atoi(argv[2]);
    direction = atoi(argv[3]);
    speed = atoi(argv[4]);
  }
  else if (argc == 6)
  {
    servername = argv[1];
    channel = atoi(argv[2]);
    motor = atoi(argv[3]);
    direction = atoi(argv[4]);
    speed = atoi(argv[5]);
  }
  else
  {
    fprintf(stderr, "Usage: %s [hostname] <channel:1-4> <motor:0-2> <direction:0-1> <speed:0-7>\n\n", argv[0]);
    exit(1);
  }

  // Issue the command

  try
  {
    LPC1114_IOP::Transport_Class server(servername);
    LPC1114_IOP::LEGORC GPIO7(&server, LPC1114_GPIO7);

    GPIO7.Put(channel, motor, direction, speed);
  }

  catch (std::string msg)
  {
    fprintf(stderr, "EXCEPTION: %s\n\n", msg.c_str());
    exit(1);
  }
}
