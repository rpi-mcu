// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// version test

// Copyright (C)2017-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstdio>
#include <cstdlib>
#include <string>

#include <LPC1114_IOP.h>

#define SERVERNAME	(argc == 2 ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Firmware Version Test\n");

  try
  {
    LPC1114_IOP::Transport_Class iop(SERVERNAME);
    SPIAGENT_COMMAND_MSG_t cmd;
    SPIAGENT_RESPONSE_MSG_t resp;

    cmd.command = SPIAGENT_CMD_NOP;
    cmd.pin = 0;
    cmd.data = 0;

    iop.Transaction(&cmd, &resp);
    LPC1114_IOP::CheckError("NOP", resp.error);

    printf("The SPI Agent Firmware version number is %d\n", resp.data);

    cmd.command = SPIAGENT_CMD_GET_SFR;
    cmd.pin = 0x400483F4; // LPC1114 Device ID register
    cmd.data = 0;

    iop.Transaction(&cmd, &resp);
    LPC1114_IOP::CheckError("SFR GET", resp.error);

    printf("The LPC1114 device ID is 0x%08X\n", resp.data);
  }

  catch (std::string msg)
  {
    fprintf(stderr, "EXCEPTION: %s\n\n", msg.c_str());
    exit(1);
  }
}
