// Raspberry Pi LPC1114 I/O Processor Sonar Ranging Test

// Copyright (C)2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// LPC1114 GPIO0 -- Echo pulse input
// LPC1114 GPIO1 -- Trigger pulse output

// NOTE: This program is written for and tested with 4-pin ultrasonic
// modules like the HC-SR04.  To use a 3-pin module like the Parallax
// Ping, connect GPIO1 to GPIO0 through a Schottky signal diode like the
// BAT43, anode to GPIO1 and cathode to GPIO0.

#include <cstdio>
#include <cstdlib>
#include <string>
#include <unistd.h>

#include <LPC1114_IOP.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Sonar Ranging Test\n");

  try
  {
    LPC1114_IOP::Transport_Class server(SERVERNAME);

    // Configure GPIO0 as pulse width input

    LPC1114_IOP::Timer CT32B1(&server, LPC1114_CT32B1);
    CT32B1.ConfigurePrescaler(1);
    CT32B1.ConfigureCapture(LPC1114_TIMER_CAPTURE_EDGE_CAP0_BOTH, false);
    CT32B1.ConfigureMode(LPC1114_TIMER_MODE_PCLK);

    // Configure GPIO1 as trigger pulse output

    LPC1114_IOP::GPIO Trigger(&server, LPC1114_GPIO1, LPC1114_GPIO_OUTPUT,
      false);

    for (;;)
    {
      // Emit trigger pulse

      Trigger = true;
      Trigger = false;

      // Wait for an echo

      usleep(100000);

      // Calculate range from measured echo delay

      uint32_t distance = CT32B1.GetCaptureDelta()/282.35 + 0.5;

      // Display result

      if (distance > 2000)
        puts("Probing...");
      else
        printf("Detection! Range is %d mm\n", distance);

      fflush(stdout);
      usleep(900000);
    }
  }

  catch (std::string msg)
  {
    fprintf(stderr, "EXCEPTION: %s\n\n", msg.c_str());
    exit(1);
  }
}
