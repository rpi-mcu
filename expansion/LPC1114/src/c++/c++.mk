# Makefile to build the C++ component library

# Copyright (C)2017-2021, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifneq ($(BOARDNAME),)
# Cross-compile for MuntsOS
MUNTSOS		?= /usr/local/share/muntsos
include $(MUNTSOS)/include/$(BOARDNAME).mk
endif

AR		= $(CROSS_COMPILE)ar
CXX		= $(CROSS_COMPILE)g++
STRIP		= $(CROSS_COMPILE)strip

CXXFLAGS	+= -Wall -std=c++11
CXXFLAGS	+= -I$(RASPBERRYPI_LPC1114_SRC)/c/include
CXXFLAGS	+= -I$(RASPBERRYPI_LPC1114_SRC)/c++/classes
CXXFLAGS	+= $(DEBUGFLAGS) $(EXTRAFLAGS)

ifeq ($(BOARDNAME),)
CXXFLAGS	+= -I/usr/local/include
LDFLAGS		+= -L/usr/local/lib
ifeq ($(shell uname), Darwin)
# The MacOS native C/C++ compiler doesn't define __unix__
CXXFLAGS	+= -D__unix__
endif
endif

LDFLAGS		+= -L$(RASPBERRYPI_LPC1114_SRC)/c++/classes
LDFLAGS		+= -lspiagent++ -lspiagent

# Define a pattern rule to compile a C++ program

%: %.cpp
	$(MAKE) $(RASPBERRYPI_LPC1114_SRC)/c++/classes/libspiagent++.a
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)
	$(STRIP) $@

# Define a pattern rule to compile a C++ class

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

# Default target placeholder

cpp_mk_default: default

# Build the C++ library

$(RASPBERRYPI_LPC1114_SRC)/c++/classes/libspiagent++.a:
	for F in $(RASPBERRYPI_LPC1114_SRC)/c++/classes/*.cpp ; do $(MAKE) `dirname $$F`/`basename $$F .cpp`.o ; done
	$(AR) rcs $@ $(RASPBERRYPI_LPC1114_SRC)/c++/classes/*.o

lib: $(RASPBERRYPI_LPC1114_SRC)/c++/classes/libspiagent++.a

# Remove working files

cpp_mk_clean:
	rm -f $(RASPBERRYPI_LPC1114_SRC)/c++/classes/*.a
	rm -f $(RASPBERRYPI_LPC1114_SRC)/c++/classes/*.o
cpp_mk_reallyclean: cpp_mk_clean

cpp_mk_distclean: cpp_mk_reallyclean
