-- Raspberry Pi LPC1114 I/O Processor SPI Agent Firmware Transport Services
-- using AWS (Ada Web Server) HTTP client services

-- Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

WITH Ada.Strings;           USE Ada.Strings;
WITH Ada.Strings.Fixed;     USE Ada.Strings.Fixed;
WITH Ada.Strings.Unbounded; USE Ada.Strings.Unbounded;

WITH AWS.Client;
WITH AWS.Response;

WITH GNAT.Sockets;

PACKAGE BODY spi_agent.transport IS

  server : Unbounded_String := Null_Unbounded_String;

  -- Initialize the SPI Agent Firmware transport library

  PROCEDURE open(servername : String; error : OUT Integer) IS

  BEGIN

    -- Check for previous open

    IF server /= Null_Unbounded_String THEN
      error := EBUSY;
      RETURN;
    END IF;

    -- Check for empty server name

    IF servername = "" THEN
      error := EINVAL;
    END IF;

    -- Check for forbidden prefixes

    IF Index(servername, "ioctl://") /= 0 THEN
      error := EINVAL;
      RETURN;
    END IF;

    IF Index(servername, "oncrpc://") /= 0 THEN
      error := EINVAL;
      RETURN;
    END IF;

    IF Index(servername, "xmlrpc://") /= 0 THEN
      error := EINVAL;
      RETURN;
    END IF;

    -- Store server IP address

    server := To_Unbounded_String(GNAT.Sockets.Image(GNAT.Sockets.Addresses(GNAT.Sockets.Get_Host_By_Name(servername), 1)));

    -- Delete http:// prefix, if present

    IF Index(server, "http://") = 1 THEN
      Delete(server, 1, 7);
    END IF;

    -- Delete trailing NUL, if present

    IF Element(server, Length(server)) = ASCII.NUL THEN
      Delete(server, Length(server), Length(server));
    END IF;

    error := 0;
  END open;

  -- Deinitialize the SPI Agent Firmware transport library

  PROCEDURE close(error : OUT Integer) IS

  BEGIN

    -- Check for previous open

    IF server = Null_Unbounded_String THEN
      error := EBADF;
      RETURN;
    END IF;

    -- Flush server name

    server := Null_Unbounded_String;
    error := 0;
  END close;

  -- Dispatch an SPI Agent Firmware command to the transport library

  PROCEDURE command
   (command  : IN spi_agent.messages.SPIAGENT_COMMAND_MSG_t;
    response : OUT spi_agent.messages.SPIAGENT_RESPONSE_MSG_t;
    error    : OUT Integer) IS

    httprequest  : Unbounded_String;
    httpresponse : Unbounded_String;
    c1, c2, c3, c4, c5 : Natural;

  BEGIN

    -- Check for previous open

    IF server = Null_Unbounded_String THEN
      error := EBADF;
      RETURN;
    END IF;

    -- Build the request string

    httprequest := "http://" & server & ":8081/SPIAGENT?cmd=" &
      Trim(To_Unbounded_String(Unsigned_32'IMAGE(command.command)), Left) & "," &
      Trim(To_Unbounded_String(Unsigned_32'IMAGE(command.pin)), Left) & "," &
      Trim(To_Unbounded_String(Unsigned_32'IMAGE(command.data)), Left);

    -- Send the request string and get the response string

    httpresponse := To_Unbounded_String(AWS.Response.Message_Body(AWS.Client.Get(To_String(httprequest))));

    -- Parse the response string

    c1 := Index(httpresponse, ",", 1);
    IF c1 = 0 THEN
      error := EIO;
      RETURN;
    END IF;

    c2 := Index(httpresponse, ",", c1+1);
    IF c2 = 0 THEN
      error := EIO;
      RETURN;
    END IF;

    c3 := Index(httpresponse, ",", c2+1);
    IF c3 = 0 THEN
      error := EIO;
      RETURN;
    END IF;

    c4 := Index(httpresponse, ",", c3+1);
    IF c4 = 0 THEN
      error := EIO;
      RETURN;
    END IF;

    c5 := Index(httpresponse, ";", c4+1);
    IF c5 = 0 THEN
      error := EIO;
      RETURN;
    END IF;

    -- Convert the response string fields to variables

    error := Integer'VALUE(Slice(httpresponse, 1, c1 - 1));
    response.command := Unsigned_32'VALUE(Slice(httpresponse, c1 + 1, c2 - 1));
    response.pin := Unsigned_32'VALUE(Slice(httpresponse, c2 + 1, c3 - 1));
    response.data := Unsigned_32'VALUE(Slice(httpresponse, c3 + 1, c4 - 1));
    response.error := Unsigned_32'VALUE(Slice(httpresponse, c4 + 1, c5 - 1));
  END command;

BEGIN
  GNAT.Sockets.Initialize(False);
END spi_agent.transport;
