# Makefile definitions for building GNAT Ada application programs
# for the Raspberry Pi LPC1114 I/O Processor Expansion Board

# Copyright (C)2013-2021, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Do not remove intermediate files

.SECONDARY:

# General toolchain definitions

ADA_OBJ		?= $(shell pwd)/obj
ADA_SRC		?= $(RASPBERRYPI_LPC1114_SRC)/ada

GNATENV		+= ADA_OBJ=$(ADA_OBJ) ADA_SRC=$(ADA_SRC)

ifneq ($(BOARDNAME),)
# Definitions for MuntsOS cross-compile
MUNTSOS		?= /usr/local/share/muntsos
include $(MUNTSOS)/include/$(BOARDNAME).mk
else
# Definitions for native compile
LIBSIMPLEIO	?= /usr/local/share/libsimpleio
# Prepend $(GNAT)/bin to PATH
ifneq ($(GNAT),)
GNATENV		+= PATH=$(GNAT)/bin:$(PATH)
ifeq ($(OS), Windows_NT)
# Windows needs even more help with the path
GNATMAKE	= env $(GNATENV) $(GNAT)/bin/gnatmake
GNATSTRIP	= env $(GNATENV) $(GNAT)/bin/strip
GPRBUILD	= env $(GNATENV) $(GNAT)/bin/gprbuild
endif
endif
endif

# The default transport mechanism depends on the OS

ifeq ($(OS), Windows_NT)
TRANSPORT		?= http
else
TRANSPORT		?= libspiagent
endif

# Definitions for Microsoft Windows

ifeq ($(OS), Windows_NT)
EXESUFFIX	= .exe
GNATENV		+= PWD=$(shell pwd)
ifeq ($(TRANSPORT), remoteio_hidapi)
WINARCH		?= win64
LDFLAGS		+= -L$(LIBSIMPLEIO)/win/$(WINARCH)
endif
ifeq ($(TRANSPORT), remoteio_libusb)
WINARCH		?= win64
LDFLAGS		+= -L$(LIBSIMPLEIO)/win/$(WINARCH)
endif
endif

# Definitions for recent MacOS X and AdaCore Community Edition

ifeq ($(shell uname), Darwin)
LDFLAGS		+= -L/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib
LDFLAGS		+= -L/usr/local/lib
endif

# Definitions for strip

GNATSTRIP	?= env $(GNATENV) $(GNATPREFIX)strip

# Definitions for gprbuild

GPRBUILD	?= env $(GNATENV) gprbuild
GPRBUILDFLAGS	= -XMAIN=$@ -XTRANSPORT=$(TRANSPORT) -P$(ADA_SRC)/packages/spi_agent.gpr -p $(GPRBUILDCONFIG)

# The following transport mechanisms require components from libsimpleio

ifeq ($(TRANSPORT), remoteio_hidapi)
GPRBUILDFLAGS	+= -XLIBSIMPLEIO=$(LIBSIMPLEIO)
endif

ifeq ($(TRANSPORT), remoteio_libsimpleio)
GPRBUILDFLAGS	+= -XLIBSIMPLEIO=$(LIBSIMPLEIO)
endif

ifeq ($(TRANSPORT), remoteio_libusb)
GPRBUILDFLAGS	+= -XLIBSIMPLEIO=$(LIBSIMPLEIO)
endif

# Define a rule for compiling an Ada application program

%: %.adb
ifeq ($(OS), Windows_NT)
ifeq ($(TRANSPORT), remoteio_hidapi)
	cp $(LIBSIMPLEIO)/win/$(WINARCH)/hidapi.dll .
endif
ifeq ($(TRANSPORT), remoteio_libusb)
	cp $(LIBSIMPLEIO)/win/$(WINARCH)/libusb-1.0.dll .
endif
endif
	$(GPRBUILD) $(GPRBUILDFLAGS)
	-$(GNATSTRIP) $@$(EXESUFFIX)

# Default make target

ada_mk_default: default

# Remove working files

ada_mk_clean:
	-rm -rf $(ADA_OBJ) *.exe *.o *.ali b~* *.stackdump *.dll

ada_mk_reallyclean: ada_mk_clean

ada_mk_distclean: ada_mk_reallyclean
