// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent ONC/RPC server program

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <libsimpleio/liblinux.h>

#include "spi_agent_oncrpc.h"

#define PROGRAMNAME	"spi_agent_oncrpc_server"
#define RESPONSEDELAY	0

// ioctl() wrapper functions

extern void spiagent_open_ioctl(char *servername, int32_t *error);
extern void spiagent_command_ioctl(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);
extern void spiagent_close_ioctl(int32_t *error);

void spi_agent_oncrpc_initialize(void)
{
  int32_t error;

  openlog(PROGRAMNAME, LOG_NDELAY|LOG_PID|LOG_PERROR, LOG_DAEMON);

  syslog(LOG_INFO, "Starting LPC1114 SPI Agent Firmware ONC/RPC server");

// Open the Raspberry Pi LPC1114 I/O Processor Expansion Board SPI device

  spiagent_open_ioctl(NULL, &error);

  if (error)
  {
    syslog(LOG_ERR, "ERROR: spiagent_open_ioctl() failed, %s", strerror(error));
    exit(1);
  }

// Become a nobody

  LINUX_drop_privileges("nobody", &error);
  if (error)
  {
    syslog(LOG_ERR, "ERROR: LINUX_drop_privileges() failed, %s", strerror(error));
    exit(1);
  }

// Switch to background execution

  if (daemon(0, 0))
  {
    syslog(LOG_ERR, "ERROR: daemon() failed, %s", strerror(errno));
    exit(1);
  }
}

// RPC service function to perform an SPI transaction

bool_t spi_transaction_1_svc(SPIAGENT_COMMAND_MSG_t command, SPIAGENT_RESPONSE_MSG_t *response,
  struct svc_req *req)
{
  int32_t error;

  // Execute the requested SPI Agent command

  spiagent_command_ioctl(&command, response, &error);

  if (error)
  {
    syslog(LOG_ERR, "ERROR: spiagent_command_ioctl() failed, %s", strerror(error));
    response->error = error;
  }

  return true;
}

// Free the response structure

int spi_agent_oncrpc_1_freeresult(SVCXPRT *transp, xdrproc_t xdr_result,
  caddr_t result)
{
  xdr_free(xdr_result, result);
  return true;
}
