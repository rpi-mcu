// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI Agent ONC/RPC C client program

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "spi_agent_oncrpc.h"
#include <spi-agent.h>

#define PROGRAMNAME	argv[0]
#define SERVERNAME	((argc >= 2) ? argv[1] : "localhost")
#define ITERATIONS	((argc == 3) ? atoi(argv[2]) : 100000)

int main(int argc, char *argv[])
{
  CLIENT *clnt;
  SPIAGENT_COMMAND_MSG_t command;
  SPIAGENT_RESPONSE_MSG_t *response;
  int i;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

  if ((argc < 1) || (argc > 3))
  {
    fprintf(stderr, "Usage: %s [hostname] [iterations]\n", PROGRAMNAME);
    exit(1);
  }

 // Create client handle

  clnt = clnt_create(SERVERNAME, SPI_AGENT_ONCRPC, SPI_AGENT_ONCRPC_VERS, "udp");
  if (clnt == NULL)
  {
    clnt_pcreateerror("ERROR: clnt_create() failed");
    exit(1);
  }

// Issue some RPC calls

  puts("Issuing some SPI transactions...\n");

  command.command = SPIAGENT_CMD_NOP;
  command.pin = 0;
  command.data = 0;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
      response->command, response->pin, response->data, response->error);

  command.command = SPIAGENT_CMD_LOOPBACK;
  command.pin = 2;
  command.data = 3;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
      response->command, response->pin, response->data, response->error);

  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = 99;
  command.data = 3;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
      response->command, response->pin, response->data, response->error);

  command.command = 99;
  command.pin = 2;
  command.data = 3;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
      response->command, response->pin, response->data, response->error);

// Display the LPC1114 firmware version

  command.command = SPIAGENT_CMD_NOP;
  command.pin = 0;
  command.data = 0;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("\nThe LPC1114 firmware version is %u\n", response->data);

// Display the LPC1114 device ID

  command.command = SPIAGENT_CMD_GET_SFR;
  command.pin = 0x400483F4; // LPC1114 Device ID register
  command.data = 0;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("The LPC1114 device ID is        %08X\n", response->data);

// Display the state of the expansion board LED

  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = LPC1114_LED;
  command.data = 0;

  response = spi_transaction_1(command, clnt);
  if (response)
    printf("The expansion board LED is      %s\n\n", response->data ? "ON" : "OFF");

// Performance test

  printf("Starting %d SPI agent loopback test transactions...\n", ITERATIONS);
  fflush(stdout);

  time_t start = time(NULL);

  for (i = 0; i < ITERATIONS; i++)
  {
    command.command = SPIAGENT_CMD_LOOPBACK;
    command.pin = i*17;
    command.data = i*19;

    response = spi_transaction_1(command, clnt);

    if ((response->command != command.command) ||
        (response->pin != command.pin) ||
        (response->data != command.data) ||
        (response->error != 0))
    {
      printf("Iteration:%-6d Response: command:%u pin:%u data:%u error:%u\n",
        i, response->command, response->pin, response->data, response->error);
    }
  }

  time_t finish = time(NULL);
  double iterations = 1.0*i;
  double deltat = finish - start;
  double rate = iterations / deltat;
  double cycletime = deltat / iterations;

// Display results

  printf("\nPerformed %d loopback tests in %ld seconds\n", i, finish - start);
  printf("  %1.1f iterations per second\n", rate);
  printf("  %1.1f microseconds per RPC call\n", cycletime*1.0E6);

  exit(0);
}
