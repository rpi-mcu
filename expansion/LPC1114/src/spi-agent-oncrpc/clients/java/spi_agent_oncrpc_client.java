// Java test client for the Raspberry Pi LPC1114 I/O Processor
// Expansion Board SPI agent firmware XML-RPC server

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import java.io.*;
import java.net.*;

import org.acplt.oncrpc.*;

import static SPIAgent.commands.*;
import static SPIAgent.pins.*;

public class spi_agent_oncrpc_client
{
  public static SPIAGENT_RESPONSE_MSG_t spiagent_command_oncrpc(spi_agent_oncrpcClient server,
    SPIAGENT_COMMAND_MSG_t cmd)
  {
    try
    {
      return server.spi_transaction_1(cmd);
    }
    catch (OncRpcException e)
    {
      System.out.println("ERROR: ONC/RPC error: " + e.toString());
      System.exit(1);
    }
    catch (IOException e)
    {
      System.out.println("ERROR: I/O error: " + e.toString());
      System.exit(1);
    }

    return null;
  }

  public static void main(String[] args)
  {
    String servername = new String();
    int iterations = 50000;
    spi_agent_oncrpcClient server;
    SPIAGENT_COMMAND_MSG_t cmd = new SPIAGENT_COMMAND_MSG_t();
    SPIAGENT_RESPONSE_MSG_t resp;

    System.out.println("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

    // Build ONC-RPC server URL

    switch (args.length)
    {
      case 0 :
        servername = "localhost";
        break;

      case 1 :
        servername = args[0];
        break;

      case 2 :
        servername = args[0];
        iterations = Integer.parseInt(args[1]);
        break;

      default :
        System.out.println("Usage: java -jar spi_agent_test_oncrpc.jar [hostname] [iterations]");
        System.exit(1);
        break;
    }

    try
    {
      server = new spi_agent_oncrpcClient(InetAddress.getByName(servername),
        OncRpcProtocols.ONCRPC_UDP);
    }
    catch (OncRpcProgramNotRegisteredException e)
    {
      System.out.println("ERROR: Program not found at server " + servername);
      server = null;
      System.exit(1);
    }
    catch (OncRpcException e)
    {
      System.out.println("ERROR: ONC/RPC error: " + e.toString());
      server = null;
      System.exit(1);
    }
    catch (IOException e)
    {
      System.out.println("ERROR: I/O error: " + e.toString());
      server = null;
      System.exit(1);
    }

    System.out.println("Issuing some SPI transactions...\n");

    cmd.command = SPIAGENT_CMD_NOP.ordinal();
    cmd.pin = 0;
    cmd.data = 0;
    resp = spiagent_command_oncrpc(server, cmd);
    System.out.print("Response: command:" + String.format("%-4d", resp.command));
    System.out.print(" pin:" + String.format("%-4d", resp.pin));
    System.out.print(" data:" + String.format("%-4d", resp.data));
    System.out.println(" error:" + String.format("%-4d", resp.error));

    cmd.command = SPIAGENT_CMD_LOOPBACK.ordinal();
    cmd.pin = 2;
    cmd.data = 3;
    resp = spiagent_command_oncrpc(server, cmd);
    System.out.print("Response: command:" + String.format("%-4d", resp.command));
    System.out.print(" pin:" + String.format("%-4d", resp.pin));
    System.out.print(" data:" + String.format("%-4d", resp.data));
    System.out.println(" error:" + String.format("%-4d", resp.error));

    cmd.command = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd.pin = 99;
    cmd.data = 3;
    resp = spiagent_command_oncrpc(server, cmd);
    System.out.print("Response: command:" + String.format("%-4d", resp.command));
    System.out.print(" pin:" + String.format("%-4d", resp.pin));
    System.out.print(" data:" + String.format("%-4d", resp.data));
    System.out.println(" error:" + String.format("%-4d", resp.error));

    cmd.command = 99;
    cmd.pin = 2;
    cmd.data = 3;
    resp = spiagent_command_oncrpc(server, cmd);
    System.out.print("Response: command:" + String.format("%-4d", resp.command));
    System.out.print(" pin:" + String.format("%-4d", resp.pin));
    System.out.print(" data:" + String.format("%-4d", resp.data));
    System.out.println(" error:" + String.format("%-4d", resp.error));

    cmd.command = SPIAGENT_CMD_NOP.ordinal();
    cmd.pin = 0;
    cmd.data = 0;
    resp = spiagent_command_oncrpc(server, cmd);
    if (resp.error == 0)
      System.out.println("\nThe LPC1114 firmware version is " +
        String.format("%d", resp.data));

    cmd.command = SPIAGENT_CMD_GET_SFR.ordinal();
    cmd.pin = 0x400483F4;
    cmd.data = 0;
    resp = spiagent_command_oncrpc(server, cmd);
    if (resp.error == 0)
      System.out.println("The LPC1114 device ID is        " +
        String.format("%08X", resp.data));

    cmd.command = SPIAGENT_CMD_GET_GPIO.ordinal();
    cmd.pin = LPC1114_LED;
    cmd.data = 0;
    resp = spiagent_command_oncrpc(server, cmd);
    if (resp.error == 0)
      System.out.println("The expansion board LED is      " +
        ((resp.data == 1) ? "ON" : "OFF") + "\n");

    long starttime = System.nanoTime();

    System.out.println("Starting " + Integer.toString(iterations) +
      " SPI agent loopback test transactions...\n");

    for (int i = 1; i <= iterations; i++)
    {
      cmd.command = SPIAGENT_CMD_LOOPBACK.ordinal();
      cmd.pin = i*17;
      cmd.data = i*19;
      resp = spiagent_command_oncrpc(server, cmd);

      if ((resp.command != cmd.command) || (resp.pin != cmd.pin) ||
          (resp.data != cmd.data) || (resp.error != 0))
      {
        System.out.println("Iteration:" + Integer.toString(i) + " Response: command:" +
        Integer.toString(resp.command) + " pin:" + Integer.toString(resp.pin) + " data:" +
        Integer.toString(resp.data) + " error:" + Integer.toString(resp.error));
      }
    }

    long endtime = System.nanoTime();

    // Display statistics

    double deltat = (endtime - starttime)*1.0e-9;
    double rate = iterations/deltat;
    double cycletime = deltat/iterations;

    System.out.println("Performed " + Integer.toString(iterations) + " loopback tests in " +
      String.format("%1.1f", deltat) + " seconds");

    System.out.println("  " + String.format("%1.1f", rate) + " iterations per second");

    System.out.println("  " + String.format("%1.1f", cycletime*1.0E6) + " microseconds per iteration" + "\n");
  }
}
