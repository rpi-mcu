/* Raspberry Pi LPC1114 I/O Processor Expansion Board */
/* sonar ranging test program                         */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// LPC1114 GPIO0 -- Echo pulse input
// LPC1114 GPIO1 -- Trigger pulse output

// NOTE: This program is written for and tested with 4-pin ultrasonic
// modules like the HC-SR04.  To use a 3-pin module like the Parallax
// Ping, connect GPIO1 to GPIO0 through a Schottky signal diode like the
// BAT43, anode to GPIO1 and cathode to GPIO0.

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

static volatile sig_atomic_t breakflag = 0;

static void breakhandler(int sig)
{
  breakflag = 1;
  signal(sig, breakhandler);
}

int main(int argc, char *argv[])
{
  int32_t error;
  uint32_t count;
  uint32_t distance;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Sonar Test\n\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Install signal handlers

  if (signal(SIGINT, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGINT failed, %s\n", strerror(errno));
    exit(1);
  }

  if (signal(SIGTERM, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGTERM failed, %s\n", strerror(errno));
    exit(1);
  }

  // Configure LPC1114 timer 1 to measure pulse width on CAP0 input

  spiagent_timer_init(LPC1114_CT32B1, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_init() failed, %s\n", strerror(error));
    exit(1);
  }

  spiagent_timer_configure_prescaler(LPC1114_CT32B1, 1, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_prescaler() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_timer_configure_capture(LPC1114_CT32B1,
    LPC1114_TIMER_CAPTURE_EDGE_CAP0_BOTH, false, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_capture() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_timer_configure_mode(LPC1114_CT32B1,
    LPC1114_TIMER_MODE_PCLK, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_mode() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Configure the trigger pulse output

  spiagent_gpio_configure(LPC1114_GPIO1, LPC1114_GPIO_OUTPUT, 0, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Perform sonar ranging

  puts("Press CONTROL-C to quit\n");

  while (!breakflag)
  {

    // Pulse trigger output

    spiagent_gpio_put(LPC1114_GPIO1, 1, &error);
    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_gpio_put() failed, %s\n",
        strerror(error));
      exit(1);
    }

    spiagent_gpio_put(LPC1114_GPIO1, 0, &error);
    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_gpio_put() failed, %s\n",
        strerror(error));
      exit(1);
    }

    usleep(100000);

    // Get echo pulse width result

    spiagent_timer_get_capture_delta(LPC1114_CT32B1, &count, &error);
    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_timer_get_capture_delta() failed, %s\n",
        strerror(error));
      exit(1);
    }

    // Calculate range

    distance = count/282.35;

    // Display result

    if (distance > 2000)
      printf("Probing...                 \r");
    else
      printf("Detection! Range is %d mm\r", distance);

    fflush(stdout);
    usleep(900000);
  }

  putchar('\n');
  putchar('\n');

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
