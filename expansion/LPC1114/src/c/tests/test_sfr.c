/* Raspberry Pi LPC1114 I/O Processor Expansion Board SFR test program */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  uint32_t data;
  int32_t error;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SFR Test\n\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Read the LPC1114 device ID register

  spiagent_sfr_get(LPC1114_DEVICE_ID, &data, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_sfr_get() failed, %s\n", strerror(error));
    exit(1);
  }

  printf("The LPC1114 device ID is                 %08X\n", data);

  // Write to the LPC1114 UART scratch pad register

  spiagent_sfr_put(LPC1114_U0SCR, 0x11, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_sfr_put() failed, %s\n", strerror(error));
    exit(1);
  }

  // Read from the LPC1114 UART scratch pad register

  spiagent_sfr_get(LPC1114_U0SCR, &data, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_sfr_get() failed, %s\n", strerror(error));
    exit(1);
  }

  printf("The LPC1114 UART scratch pad register is %08X\n", data);

  // Close the SPI Agent services library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
