// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// version test

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  SPIAGENT_COMMAND_MSG_t command;
  SPIAGENT_RESPONSE_MSG_t response;
  int32_t error;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Firmware Version Test\n\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Query the LPC1114 firmware version

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_NOP;

  memset(&response, 0, sizeof(response));

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: NOP operation failed, %s\n", strerror(response.error));
    exit(1);
  }

  printf("The SPI Agent Firmware version number is %d\n", response.data);

  // Query the LPC1114 device ID

  memset(&command, 0, sizeof(command));
  command.command = SPIAGENT_CMD_GET_SFR;
  command.pin = 0x400483F4; // LPC1114 Device ID register

  memset(&response, 0, sizeof(response));

  spiagent_command(&command, &response, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_command() failed, %s\n", strerror(error));
    exit(1);
  }

  if (response.error)
  {
    fprintf(stderr, "ERROR: GET_SFR operation for register %08X failed, %s\n",
      response.pin, strerror(response.error));
    exit(1);
  }

  printf("The LPC1114 device ID is 0x%08X\n", response.data);

  // Close the SPI Agent services library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
