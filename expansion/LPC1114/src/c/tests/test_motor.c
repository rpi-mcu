/* Raspberry Pi LPC1114 I/O Processor Expansion Board */
/* H-bridge DC motor driver output test program       */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME      ((argc == 2) ? argv[1] : "localhost")

int main(int argc, char *argv[])
{
  int32_t error;
  char inbuf[256];
  uint32_t freq;
  float speed;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board DC Motor Test\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Get desired PWM frequency from the user

  printf("Enter PWM frequency:              ");
  fflush(stdout);

  fgets(inbuf, sizeof(inbuf), stdin);
  freq = atoi(inbuf);

  spiagent_pwm_set_frequency(freq, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_set_frequency() failed, %s\n", strerror(error));
    exit(1);
  }

  // Initialize the H-bridge motor driver outputs

  spiagent_motor_configure(LPC1114_PWM1, LPC1114_GPIO0, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_pwm_configure() failed, %s\n", strerror(error));
    exit(1);
  }

  for (;;)
  {
    printf("Enter motor speed (-1.0 to +1.0): ");
    fflush(stdout);

    fgets(inbuf, sizeof(inbuf), stdin);
    speed = atof(inbuf);

    spiagent_motor_put(LPC1114_PWM1, LPC1114_GPIO0, speed, &error);

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_motor_put() failed, %s\n", strerror(error));
      continue;
    }
  }

  // Close the SPI Agent Firmware transport library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
