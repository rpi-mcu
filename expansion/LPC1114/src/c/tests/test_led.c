/* Raspberry Pi LPC1114 I/O Processor Expansion Board LED test program */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

static volatile sig_atomic_t breakflag = 0;

static void breakhandler(int sig)
{
  breakflag = 1;
  signal(sig, breakhandler);
}

int main(int argc, char *argv[])
{
  int32_t error;
  bool LED = true;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board LED Test\n\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  spiagent_gpio_configure_mode(LPC1114_LED, LPC1114_GPIO_MODE_OUTPUT, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_mode() failed, %s\n", strerror(error));
    exit(1);
  }

  // Install signal handlers

  if (signal(SIGINT, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGINT failed, %s\n", strerror(errno));
    exit(1);
  }

  if (signal(SIGTERM, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGTERM failed, %s\n", strerror(errno));
    exit(1);
  }

  puts("Press CONTROL-C to quit\n");

  // Toggle the LED

  while (!breakflag)
  {
    printf("Turning LED %s\n", LED ? "ON" : "OFF");

    spiagent_gpio_put(LPC1114_LED, LED, &error);

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_gpio_put() failed, %s\n", strerror(error));
      exit(1);
    }

    LED = !LED;

    sleep(1);
  }

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
