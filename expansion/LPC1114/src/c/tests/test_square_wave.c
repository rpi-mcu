/* Raspberry Pi LPC1114 I/O Processor Expansion Board */
/* square wave output test program                    */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	argv[1]
#define FREQUENCY	atoi(argv[2])

int main(int argc, char *argv[])
{
  int32_t error;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Square Wave Output Test\n\n");

  if (argc != 3)
  {
    fprintf(stderr, "Usage: %s <hostname> <frequency>\n", argv[0]);
    exit(1);
  }

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Configure CT32B1 to generate a square wave output on MAT0 (GPIO1)

  spiagent_timer_init(LPC1114_CT32B1, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_init() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_timer_configure_prescaler(LPC1114_CT32B1, 1, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_prescaler() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_timer_configure_match(LPC1114_CT32B1, LPC1114_TIMER_MATCH0,
    PCLK_FREQUENCY/FREQUENCY/2, LPC1114_TIMER_MATCH_OUTPUT_TOGGLE,
    false, true, false, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_match() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_timer_configure_mode(LPC1114_CT32B1, LPC1114_TIMER_MODE_PCLK, &error);
  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_timer_configure_mode() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Close the transport library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
