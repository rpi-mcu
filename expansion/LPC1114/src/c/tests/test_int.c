/* Raspberry Pi LPC1114 I/O Processor Expansion Board Interrupt test */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <spi-agent.h>

int main(void)
{
  int32_t error;
  uint32_t pin;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board Interrupt Test\n");

  // Initialize the SPI Agent transport library

  spiagent_open("ioctl://localhost", &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Configure LPC1114 GPIO0 as input with pull-up, with interrupt enabled

  spiagent_gpio_configure_mode(LPC1114_GPIO0,
    LPC1114_GPIO_MODE_INPUT_PULLUP, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_mode() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_gpio_configure_interrupt(LPC1114_GPIO0,
    LPC1114_GPIO_INTERRUPT_FALLING, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_interrupt() failed, %s\n",
      strerror(error));
    exit(1);
  }

  // Enable interrupt monitoring on Raspberry Pi INT (GPIO4)

  spiagent_interrupt_enable(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_interrupt_enable() failed, %s\n",
      strerror(error));
    exit(1);
  }

  puts("Press CONTROL-C to quit\n");

  for (;;)
  {
    spiagent_interrupt_wait(1000, &pin, &error);

    if (error == EINTR)
    {
      break;
    }

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_interrupt_enable() failed, %s\n",
        strerror(error));
      break;
    }

    if (pin == 0)
    {
      continue;
    }

    printf("Received interrupt on GPIO pin %d\n", pin);
    fflush(stdout);
  }

  puts("\nTerminating.\n");

  // Return LPC1114 pin GPIO0 to default configuration

  spiagent_gpio_configure_mode(LPC1114_GPIO0,
    LPC1114_GPIO_MODE_INPUT_PULLUP, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_mode() failed, %s\n",
      strerror(error));
  }

  spiagent_gpio_configure_interrupt(LPC1114_GPIO0,
    LPC1114_GPIO_INTERRUPT_DISABLED, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_interrupt() failed, %s\n",
      strerror(error));
  }

  // Disable interrupt monitoring on Raspberry Pi INT (GPIO4)

  spiagent_interrupt_disable(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spi_agent_disable() failed, %s\n", strerror(error));
  }

  // Close the SPI Agent Firmware transport library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spi_agent_close() failed, %s\n", strerror(error));
  }

  exit(0);
}
