/* Raspberry Pi LPC1114 I/O Processor Expansion Board LED */
/* LEGO� Power Functions Remote Control test program      */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

int main(int argc, char *argv[])
{
  char *servername;
  uint32_t channel;
  uint32_t motor;
  uint32_t direction;
  uint32_t speed;
  int32_t error;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board LEGO� RC Test\n\n");

  if (argc == 5)
  {
    servername = "localhost";
    channel = atoi(argv[1]);
    motor = atoi(argv[2]);
    direction = atoi(argv[3]);
    speed = atoi(argv[4]);
  }
  else if (argc == 6)
  {
    servername = argv[1];
    channel = atoi(argv[2]);
    motor = atoi(argv[3]);
    direction = atoi(argv[4]);
    speed = atoi(argv[5]);
  }
  else
  {
    fprintf(stderr, "Usage: %s [hostname] <channel:1-4> <motor:0-2> <direction:0-1> <speed:0-7>\n", argv[0]);
    exit(1);
  }

  // Initialize the SPI Agent services library

  spiagent_open(servername, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Configure the output pin

  spiagent_gpio_configure(LPC1114_GPIO7, LPC1114_GPIO_OUTPUT, 0, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_gpio_configure_output() failed, %s\n", strerror(error));
    exit(1);
  }

  // Issue the command

  spiagent_legorc_put(LPC1114_GPIO7, channel, motor, direction, speed, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_put_legorc() failed, %s\n", strerror(error));
    exit(1);
  }

  // Deinitialize the SPI Agent services library

  spiagent_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
