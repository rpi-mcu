/* Raspberry Pi LPC1114 I/O Processor Expansion Board GPIO test program */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME	((argc == 2) ? argv[1] : "localhost")

static volatile sig_atomic_t breakflag = 0;

static void breakhandler(int sig)
{
  breakflag = 1;
  signal(sig, breakhandler);
}

int main(int argc, char *argv[])
{
  int32_t error;
  uint32_t pin;
  uint8_t bits = 0;

  printf("\nRaspberry Pi LPC1114 I/O Processor Expansion Board GPIO Test\n\n");

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    exit(1);
  }

  // Install signal handlers

  if (signal(SIGINT, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGINT failed, %s\n", strerror(errno));
    exit(1);
  }

  if (signal(SIGTERM, breakhandler) == SIG_ERR)
  {
    fprintf(stderr, "ERROR: signal() for SIGTERM failed, %s\n", strerror(errno));
    exit(1);
  }

  // Configure all GPIO pins as outputs

  for (pin = LPC1114_GPIO0; pin <= LPC1114_GPIO7; pin++)
  {
    spiagent_gpio_configure(pin, LPC1114_GPIO_OUTPUT, false, &error);

    if (error)
    {
      fprintf(stderr, "ERROR: spiagent_gpio_configure() failed, %s\n",
        strerror(error));
      exit(1);
    }

    // Skip over gap between GPIO5 and GPIO6

    if (pin == LPC1114_GPIO5) pin += 2;
  }

  puts("Press CONTROL-C to quit\n");

  // Toggle each GPIO output

  for (;;)
  {
    if (breakflag) break;

    for (pin = LPC1114_GPIO0; pin <= LPC1114_GPIO7; pin++)
    {
      spiagent_gpio_put(pin, ((bits & (1 << ((pin <= LPC1114_GPIO5) ?
        pin - LPC1114_GPIO0 : pin - LPC1114_GPIO0 - 2))) ?
        true : false), &error);

      if (error)
      {
        fprintf(stderr, "ERROR: spiagent_gpio_put() failed, %s\n",
          strerror(error));
        exit(1);
      }

      // Skip over gap between GPIO5 and GPIO6

      if (pin == LPC1114_GPIO5) pin += 2;
    }

    bits++;
  }

  spiagent_close(&error);
  exit(0);
}
