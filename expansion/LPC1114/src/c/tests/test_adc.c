/* Raspberry Pi LPC1114 I/O Processor Expansion Board A/D test program */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <curses.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <spi-agent.h>

#define SERVERNAME      ((argc == 2) ? argv[1] : "localhost")

static volatile sig_atomic_t breakflag = 0;

// Signal handler for SIGINT (CONTROL-C)

static void breakhandler(int sig)
{
  breakflag = 1;
  signal(sig, breakhandler);
}

// Error exit

static void errorexit()
{
  int32_t error;

  mvprintw(10, 0, "Press any key to exit program\n");
  refresh();
  getch();
  endwin();
  spiagent_close(&error);
  exit(1);
}

int main(int argc, char *argv[])
{
  int32_t error;
  uint32_t pin;
  float voltage;

  close(2);	// Suppress output to stderr from library functions

  // Display program banner

  initscr();
  mvprintw(0, 0, "Raspberry Pi LPC1114 I/O Processor Expansion Board Analog Input Test");
  mvprintw(8, 0, "Press CONTROL-C to quit");
  refresh();

  // Initialize the SPI Agent services library

  spiagent_open(SERVERNAME, &error);

  if (error)
  {
    mvprintw(8, 0, "ERROR: spiagent_open() failed, %s\n", strerror(error));
    errorexit();
  }

  // Install signal handlers

  if (signal(SIGINT, breakhandler) == SIG_ERR)
  {
    mvprintw(8, 0, "ERROR: signal() for SIGINT failed, %s\n", strerror(errno));
    errorexit();
  }

  if (signal(SIGTERM, breakhandler) == SIG_ERR)
  {
    mvprintw(8, 0, "ERROR: signal() for SIGTERM failed, %s\n", strerror(errno));
    errorexit();
  }

  // Configure analog inputs

  for (pin = LPC1114_AD1; pin <= LPC1114_AD5; pin++)
  {
    spiagent_analog_configure(pin, &error);

    if (error)
    {
      mvprintw(8, 0, "ERROR: spiagent_analog_configure() failed, %s\n",
        strerror(error));
      errorexit();
    }
  }

  // Read analog inputs

  while (!breakflag)
  {
    for (pin = LPC1114_AD1; pin <= LPC1114_AD5; pin++)
    {
      spiagent_analog_get(pin, &voltage, &error);

      if (error)
      {
        mvprintw(8, 0, "ERROR: spiagent_analog_get() failed, %s\n",
          strerror(error));
        errorexit();
      }

      // Display the response from the slave MCU

      mvprintw(2 + pin - LPC1114_AD1, 0, "Analog Input %d is %4.2fV", pin - LPC1114_AD1 + 1, voltage);
    }

    move(9, 0);
    refresh();
    sleep(1);
  }

  endwin();
  exit(0);
}
