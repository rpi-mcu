/* Declarations for the Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef SPI_AGENT_COMMANDS_H
#define SPI_AGENT_COMMANDS_H

_BEGIN_STD_C

// SPI Agent Firmware commands

typedef enum
{
  SPIAGENT_CMD_NOP,
  SPIAGENT_CMD_LOOPBACK,
  SPIAGENT_CMD_CONFIGURE_ANALOG_INPUT,
  SPIAGENT_CMD_CONFIGURE_GPIO_INPUT,
  SPIAGENT_CMD_CONFIGURE_GPIO_OUTPUT,
  SPIAGENT_CMD_CONFIGURE_PWM_OUTPUT,
  SPIAGENT_CMD_GET_ANALOG,
  SPIAGENT_CMD_GET_GPIO,
  SPIAGENT_CMD_PUT_GPIO,
  SPIAGENT_CMD_PUT_PWM,
  SPIAGENT_CMD_CONFIGURE_GPIO_INTERRUPT,
  SPIAGENT_CMD_CONFIGURE_GPIO,
  SPIAGENT_CMD_PUT_LEGORC,
  SPIAGENT_CMD_GET_SFR,
  SPIAGENT_CMD_PUT_SFR,
  SPIAGENT_CMD_CONFIGURE_TIMER_MODE,
  SPIAGENT_CMD_CONFIGURE_TIMER_PRESCALER,
  SPIAGENT_CMD_CONFIGURE_TIMER_CAPTURE,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH1,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH2,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH3,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH0_VALUE,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH1_VALUE,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH2_VALUE,
  SPIAGENT_CMD_CONFIGURE_TIMER_MATCH3_VALUE,
  SPIAGENT_CMD_GET_TIMER_VALUE,
  SPIAGENT_CMD_GET_TIMER_CAPTURE,
  SPIAGENT_CMD_GET_TIMER_CAPTURE_DELTA,
  SPIAGENT_CMD_INIT_TIMER,
  SPIAGENT_CMD_SENTINEL
} SPIAGENT_COMMAND_t;

_END_STD_C

#endif
