/* C Declarations for the Raspberry Pi LPC1114 I/O Processor Expansion Board */

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef EXPANSION_LPC1114_H
#define EXPANSION_LPC1114_H

// LPC1114 I/O Processor I2C configuration definitions

#define LPC1114_I2C_ADDRESS	0x44

// LPC1114 I/O Processor SPI configuration definitions

#define LPC1114_SPI_MODE	SPI_MODE_3
#define LPC1114_SPI_WORDSIZE	8
#define LPC1114_SPI_SPEED	4000000

// Configuration file name

#define CONFIGFILE		"/usr/local/etc/expansion_lpc1114.config"

// External commands

#define CMDFLASH		"/usr/local/libexec/expansion_lpc1114_flash"
#define CMDISP			"/usr/local/libexec/expansion_lpc1114_isp"
#define CMDRESET		"/usr/local/libexec/expansion_lpc1114_reset"

#endif
