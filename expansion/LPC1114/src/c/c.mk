# Common makefile definitions for compiling C test programs for the
# Raspberry Pi LPC1114 I/O Processor Expansion Board

# Copyright (C)2017-2021, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

ifneq ($(BOARDNAME),)
# Cross-compile for MuntsOS
MUNTSOS		?= /usr/local/share/muntsos
include $(MUNTSOS)/include/$(BOARDNAME).mk
endif

AR		= $(CROSS_COMPILE)ar
CC		= $(CROSS_COMPILE)gcc
STRIP		= $(CROSS_COMPILE)strip

CFLAGS		+= -Wall
CFLAGS		+= -I$(RASPBERRYPI_LPC1114_SRC)/c/include
CFLAGS		+= $(DEBUGFLAGS) $(EXTRAFLAGS)

LDFLAGS		+= -lspiagent

ifeq ($(BOARDNAME),)
CFLAGS		+= -I/usr/local/include
LDFLAGS		+= -L/usr/local/lib

# The MacOS native C/C++ compiler doesn't define __unix__
ifeq ($(shell uname), Darwin)
CFLAGS	+= -D__unix__
endif
endif

# Everything but Windows requires libncurses

ifneq ($(OS), Windows_NT)
LDFLAGS		+= -lncurses
endif

# Define a pattern rule to compile a C module

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

# Define a pattern rule to compile a C program

%: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)
	$(STRIP) $@

# Default target placeholder

c_mk_default: default

# Remove working files

c_mk_clean:

c_mk_reallyclean: c_mk_clean

c_mk_distclean: c_mk_reallyclean
