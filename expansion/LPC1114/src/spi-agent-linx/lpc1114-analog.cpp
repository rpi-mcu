// Analog input services

// Copyright (C)2016-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <spi-agent.h>

#include "linx-server/common.h"
#include "lpc1114-analog.h"

// Analog input object constructor

ADC_LPC1114::ADC_LPC1114(int32_t pin, int32_t *error)
{
  this->pin = -1;
  this->configured = false;

  if (!IS_ANALOG(pin))
  {
    *error = EINVAL;
    return;
  }

  spiagent_analog_configure(pin, error);
  if (*error) return;

  this->pin = pin;
  this->configured = true;
  *error = 0;
}

// Analog input object constructor, with deferred configuration flag

ADC_LPC1114::ADC_LPC1114(int32_t pin, int32_t deferconfig, int32_t *error)
{
  this->pin = -1;
  this->configured = false;

  if (!IS_ANALOG(pin))
  {
    *error = EINVAL;
    return;
  }

  if (deferconfig)
  {
    this->pin = pin;
    this->configured = false;
  }
  else
  {
    spiagent_analog_configure(pin, error);
    if (*error) return;

    this->pin = pin;
    this->configured = true;
  }

  *error = 0;
}

void ADC_LPC1114::read(uint32_t *sample, int32_t *error)
{
  float v;

  *sample = 0;

  // Configure the analog input pin, if necessary

  if (!this->configured)
  {
    spiagent_analog_configure(this->pin, error);
    if (*error) return;
    this->configured = true;
  }

  // Read the analog input

  spiagent_analog_get(this->pin, &v, error);
  if (*error) return;

  *sample = v*1.0E9;
  *error = 0;
}

void lpc1114_analog_init(void)
{
  int32_t error;

  // Register analog input objects

  adc_add_channel(LPC1114_AD1, new ADC_LPC1114(LPC1114_AD1, true, &error));
  adc_add_channel(LPC1114_AD2, new ADC_LPC1114(LPC1114_AD2, true, &error));
  adc_add_channel(LPC1114_AD3, new ADC_LPC1114(LPC1114_AD3, true, &error));
  adc_add_channel(LPC1114_AD4, new ADC_LPC1114(LPC1114_AD4, true, &error));
  adc_add_channel(LPC1114_AD5, new ADC_LPC1114(LPC1114_AD5, true, &error));
}
