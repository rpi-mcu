// LabView LINX Remote I/O Commands specific to the
// Raspberry Pi LPC1114 I/O Processor Expansion Board

// Copyright (C)2016-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <fcntl.h>
#include <spi-agent.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <expansion_lpc1114.h>

#include "linx-server/common.h"
#include "lpc1114.h"

//***************************************************************************

static void SystemReset(LINX_command_t *cmd, LINX_response_t *resp, int32_t *error)
{
  PREPARE_RESPONSE;
  CHECK_COMMAND_SIZE(7, 7);

  system(CMDRESET);

  resp->Status = L_OK;
  *error = 0;
}

//***************************************************************************

static void SPIAgentCommand(LINX_command_t *cmd, LINX_response_t *resp, int32_t *error)
{
  SPIAGENT_COMMAND_MSG_t cmdmsg;
  SPIAGENT_RESPONSE_MSG_t respmsg;

  PREPARE_RESPONSE;
  CHECK_COMMAND_SIZE(19, 19);

  cmdmsg.command = LINX_makeu32(cmd->Args[0], cmd->Args[1], cmd->Args[2], cmd->Args[3]);
  cmdmsg.pin     = LINX_makeu32(cmd->Args[4], cmd->Args[5], cmd->Args[6], cmd->Args[7]);;
  cmdmsg.data    = LINX_makeu32(cmd->Args[8], cmd->Args[9], cmd->Args[10], cmd->Args[11]);;

  spiagent_command(&cmdmsg, &respmsg, error);
  if (*error)
  {
    syslog(LOG_ERR, "ERROR: spiagent_command() failed, %s", strerror(*error));
    resp->Status = L_UNKNOWN_ERROR;
    return;
  }

  if (respmsg.error)
  {
    syslog(LOG_ERR, "ERROR: SPI Agent Firmware returned error, %s", strerror(respmsg.error));
    resp->Status = L_UNKNOWN_ERROR;
    *error = respmsg.error;
    return;
  }

  resp->PacketSize = 22;

  resp->Data[0]  = LINX_splitu32(respmsg.command, 0);
  resp->Data[1]  = LINX_splitu32(respmsg.command, 1);
  resp->Data[2]  = LINX_splitu32(respmsg.command, 2);
  resp->Data[3]  = LINX_splitu32(respmsg.command, 3);

  resp->Data[4]  = LINX_splitu32(respmsg.pin, 0);
  resp->Data[5]  = LINX_splitu32(respmsg.pin, 1);
  resp->Data[6]  = LINX_splitu32(respmsg.pin, 2);
  resp->Data[7]  = LINX_splitu32(respmsg.pin, 3);

  resp->Data[8]  = LINX_splitu32(respmsg.data, 0);
  resp->Data[9]  = LINX_splitu32(respmsg.data, 1);
  resp->Data[10] = LINX_splitu32(respmsg.data, 2);
  resp->Data[11] = LINX_splitu32(respmsg.data, 3);

  resp->Status = L_OK;
  *error = 0;
}

//***************************************************************************

// Register command handlers

void lpc1114_init(void)
{
  int32_t error;

  spiagent_open("ioctl://localhost", &error);
  if (error)
  {
    syslog(LOG_ERR, "ERROR: spiagent_open() failed, %s", strerror(error));
    exit(1);
  }

  AddCommand(CMD_SYSTEM_RESET, SystemReset);
  AddCommand(CMD_CUSTOM0, SPIAgentCommand);
}
