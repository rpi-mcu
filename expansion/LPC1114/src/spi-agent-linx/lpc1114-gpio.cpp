// LabView LINX Remote I/O GPIO services specific to the
// Raspberry Pi LPC1114 I/O Processor Expansion Board

// Copyright (C)2016-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <spi-agent.h>

#include "linx-server/common.h"
#include "lpc1114-gpio.h"

#define FIXDIRECTION(d) (IS_LED(pin) ? LPC1114_GPIO_OUTPUT : d)

// GPIO pin constructor

GPIO_LPC1114::GPIO_LPC1114(int32_t pin, int32_t *error)
{
  this->pin = -1;

  if (!IS_GPIO(pin) && !IS_LED(pin))
  {
    *error = ENODEV;
    return;
  }

  spiagent_gpio_configure(pin, FIXDIRECTION(LPC1114_GPIO_INPUT), 0, error);
  if (*error) return;

  this->IsOutput = false;
  this->pin = pin;
}

// GPIO pin constructor, with data direction parameter

GPIO_LPC1114::GPIO_LPC1114(int32_t pin, int32_t dir, int32_t *error)
{
  this->pin = -1;

  if (!IS_GPIO(pin) && !IS_LED(pin))
  {
    *error = ENODEV;
    return;
  }

  spiagent_gpio_configure(pin, FIXDIRECTION(dir), 0, error);
  if (*error) return;

  this->IsOutput = dir;
  this->pin = pin;
}

// GPIO pin constructor, with data direction and initial state parameters

GPIO_LPC1114::GPIO_LPC1114(int32_t pin, int32_t dir, int32_t state, int32_t *error)
{
  this->pin = -1;

  if (!IS_GPIO(pin) && !IS_LED(pin))
  {
    *error = ENODEV;
    return;
  }

  spiagent_gpio_configure(pin, FIXDIRECTION(dir), state, error);
  if (*error) return;

  this->IsOutput = dir;
  this->pin = pin;
}

// GPIO pin configuration method

void GPIO_LPC1114::configure(int32_t dir, int32_t *error)
{
  spiagent_gpio_configure(this->pin, dir, 0, error);
}

// GPIO pin read method

void GPIO_LPC1114::read(int32_t *state, int32_t *error)
{
  spiagent_gpio_get(this->pin, (uint32_t *) state, error);
}

// GPIO pin write method

void GPIO_LPC1114::write(int32_t state, int32_t *error)
{
  spiagent_gpio_put(this->pin, state, error);
}

//  Register GPIO's

void lpc1114_gpio_init(void)
{
  int32_t error;

  // Register GPIO output channel objects

  gpio_add_channel(LPC1114_GPIO0, new GPIO_LPC1114(LPC1114_GPIO0, &error));
  gpio_add_channel(LPC1114_GPIO1, new GPIO_LPC1114(LPC1114_GPIO1, &error));
  gpio_add_channel(LPC1114_GPIO2, new GPIO_LPC1114(LPC1114_GPIO2, &error));
  gpio_add_channel(LPC1114_GPIO3, new GPIO_LPC1114(LPC1114_GPIO3, &error));
  gpio_add_channel(LPC1114_GPIO4, new GPIO_LPC1114(LPC1114_GPIO4, &error));
  gpio_add_channel(LPC1114_GPIO5, new GPIO_LPC1114(LPC1114_GPIO5, &error));
  gpio_add_channel(LPC1114_GPIO6, new GPIO_LPC1114(LPC1114_GPIO6, &error));
  gpio_add_channel(LPC1114_GPIO7, new GPIO_LPC1114(LPC1114_GPIO7, &error));
  gpio_add_channel(LPC1114_LED,   new GPIO_LPC1114(LPC1114_LED,   &error));
}
