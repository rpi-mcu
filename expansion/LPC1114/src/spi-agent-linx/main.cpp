// LabView LINX Remote I/O Protocol Server Main Module

// Copyright (C)2016-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <libsimpleio/libipv4.h>
#include <libsimpleio/liblinux.h>

#include "linx-server/common.h"

#include "lpc1114.h"
#include "lpc1114-analog.h"
#include "lpc1114-gpio.h"
#include "lpc1114-pwm.h"
#include "lpc1114-servo.h"

// Device identification constants

extern const uint8_t LINX_DEVICE_FAMILY	= 255;
extern const uint8_t LINX_DEVICE_ID	= 1;
extern const char LINX_DEVICE_NAME[]	= "Raspberry Pi LPC1114 I/O Processor Expansion Board";

// More constants

#define PROGRAMNAME	"spi_agent_linx_server"
#define SERVICENAME	"spiagent-linx"

int main(void)
{
  struct servent *service;
  int32_t error;
  int fd;

  openlog(PROGRAMNAME, LOG_NDELAY|LOG_PID|LOG_PERROR, LOG_DAEMON);

  syslog(LOG_INFO, "Starting LPC1114 SPI Agent Firmware LINX server");

  // Look up service definition

  service = getservbyname(SERVICENAME, "tcp");
  if (service == nullptr)
  {
    syslog(LOG_ERR, "ERROR: getservbyname() failed, unknown service");
    exit(1);
  }

  // Register command handlers

  common_init();
  analog_init();
  gpio_init();
  pwm_init();
  servo_init();

  // Register peripheral subsystems

  lpc1114_init();
  lpc1114_analog_init();
  lpc1114_gpio_init();
  lpc1114_pwm_init();
  lpc1114_servo_init();

  // Become a nobody

  LINUX_drop_privileges("gpio", &error);
  if (error)
  {
    syslog(LOG_ERR, "ERROR: LINUX_drop_privileges() failed, %s", strerror(error));
    exit(1);
  }

  // Detach from controlling terminal

  LINUX_detach(&error);
  if (error)
  {
    syslog(LOG_ERR, "ERROR: LINUX_detach() failed, %s", strerror(error));
    exit(1);
  }

  // Start TCP server

  TCP4_server(INADDR_ANY, ntohs(service->s_port), &fd, &error);
  if (error)
  {
    syslog(LOG_ERR, "ERROR: TCP4_server() failed, %s", strerror(error));
    exit(1);
  }

  // Process commands

  executive(fd, &error);
}
