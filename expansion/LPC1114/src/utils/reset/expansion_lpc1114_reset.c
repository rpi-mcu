// Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware
// utility program to request ISP mode

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <libsimpleio/libgpio.h>
#include <spi-agent.h>

int main(void)
{
  char value[256];
  int pin_reset = -1;
  int32_t fd_reset;
  int32_t error;

  // Read pin assignment (if any) from the configuration file

  spiagent_config_open(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_config_open() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_config_get("LPC1114_RESET", value, sizeof(value), &error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_config_get() failed, %s\n",
      strerror(error));
    exit(1);
  }

  spiagent_config_close(&error);

  if (error)
  {
    fprintf(stderr, "ERROR: spiagent_config_close() failed, %s\n",
      strerror(error));
    exit(1);
  }

  pin_reset = atoi(value);

  // Assert the reset pin to reset the LPC1114 microcontroller

  GPIO_line_open(0, pin_reset, GPIOHANDLE_REQUEST_OUTPUT, 0, true, &fd_reset,
    &error);

  if (error)
  {
    fprintf(stderr, "ERROR: GPIO_line_open() failed, %s\n", strerror(error));
    exit(1);
  }

  GPIO_line_write(fd_reset, false, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: GPIO_line_write() failed, %s\n", strerror(error));
    exit(1);
  }

  usleep(1000);

  GPIO_line_write(fd_reset, true, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: GPIO_line_write() failed, %s\n", strerror(error));
    exit(1);
  }

  usleep(100000);

  GPIO_line_close(fd_reset, &error);

  if (error)
  {
    fprintf(stderr, "ERROR: GPIO_line_close() failed, %s\n", strerror(error));
    exit(1);
  }

  exit(0);
}
