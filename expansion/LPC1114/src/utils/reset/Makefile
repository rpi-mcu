# Makefile for building some Raspberry Pi LPC1114 I/O Processor
# Expansion Board utility programs.

# Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Override the following macro to build out of tree

RASPBERRYPI_LPC1114_SRC ?= $(HOME)/rpi-mcu/expansion/LPC1114/src

# Raspberry Pi toolchain definitions

HOST_CC		= gcc
HOST_CFLAGS	+= -Wall -I$(RASPBERRYPI_LPC1114_SRC)/c/include
HOST_LDFLAGS	+= -L$(RASPBERRYPI_LPC1114_SRC)/spi-agent-lib -lsimpleio -lspiagent

INSTALLDIR	= /usr/local/libexec
UTILITIES	= expansion_lpc1114_reset

# Define a pattern rule for compiling C test programs

% : %.c
	$(HOST_CC) $(HOST_CFLAGS) $< -o $@ $(HOST_LDFLAGS)

default: $(UTILITIES)

# Install utility programs

install: $(UTILITIES)
	for P in $(UTILITIES) ; do install -csm 0555 $$P $(INSTALLDIR) ; done

# Uninstall utility programs

uninstall:
	for P in $(UTILITIES) ; do rm $(INSTALLDIR)/$$P ; done

# Clean out working files

clean:
	-rm -rf $(UTILITIES)

reallyclean: clean

distclean: reallyclean
