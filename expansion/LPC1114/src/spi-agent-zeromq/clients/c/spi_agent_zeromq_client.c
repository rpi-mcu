// C test client for the Raspberry Pi LPC1114 I/O Processor
// Expansion Board SPI agent firmware ZeroMQ server

// Copyright (C)2013-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <zmq.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <spi-agent.h>

#define CLIENTNAME	"Raspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent XML-RPC C Test Client"
#define CLIENTVERSION	"1.0"

#define PROGNAME	argv[0]
#define SERVERADDR	((argc >= 2) ? argv[1] : "localhost")
#define SERVERPORT	8082
#define ITERATIONS	((argc == 3) ? atoi(argv[2]) : 150000)

// "RPC" wrapper function

void SPI_transaction_zmq(void *sock,  SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp)
{
  uint32_t cmdbuf[3], respbuf[4];

  cmdbuf[0] = htonl(cmd->command);
  cmdbuf[1] = htonl(cmd->pin);
  cmdbuf[2] = htonl(cmd->data);

  zmq_send(sock, cmdbuf, sizeof(cmdbuf), 0);
  zmq_recv(sock, respbuf, sizeof(respbuf), 0);

  resp->command = ntohl(respbuf[0]);
  resp->pin = ntohl(respbuf[1]);
  resp->data = ntohl(respbuf[2]);
  resp->error = ntohl(respbuf[3]);
}

int main(int argc, char *argv[])
{
  SPIAGENT_COMMAND_MSG_t command;
  SPIAGENT_RESPONSE_MSG_t response;
  int i;

  puts("\nRaspberry Pi LPC1114 I/O Processor Expansion Board SPI Agent Firmware Test\n");

  // Validate command line arguments

  if ((argc < 1) || (argc > 3))
  {
    fprintf(stderr, "Usage: %s [hostname] [iterations]\n", PROGNAME);
    exit(1);
  }

  // Open connection to the server

  char servername[256];
  memset(servername, 0, sizeof(servername));
  snprintf(servername, sizeof(servername), "tcp://%s:%d", SERVERADDR, SERVERPORT);

  void *context = zmq_ctx_new();
  void *serversock = zmq_socket(context, ZMQ_REQ);
  zmq_connect(serversock, servername);

  // Issue some RPC calls

  puts("Issuing some SPI transactions...\n");

  command.command = SPIAGENT_CMD_NOP;
  command.pin = 0;
  command.data = 0;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
    response.command, response.pin, response.data, response.error);

  command.command = SPIAGENT_CMD_LOOPBACK;
  command.pin = 2;
  command.data = 3;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
    response.command, response.pin, response.data, response.error);

  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = 99;
  command.data = 3;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n",
    response.command, response.pin, response.data, response.error);

  command.command = 99;
  command.pin = 2;
  command.data = 3;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("Response: command:%-4u pin:%-4u data:%-4u error:%-4u\n\n",
    response.command, response.pin, response.data, response.error);

  // Display the LPC1114 firmware version

  command.command = SPIAGENT_CMD_NOP;
  command.pin = 0;
  command.data = 0;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("The LPC1114 firmware version is %d\n", response.data);

  // Display the LPC1114 device ID

  command.command = SPIAGENT_CMD_GET_SFR;
  command.pin = 0x400483F4;
  command.data = 0;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("The LPC1114 device ID is        %08X\n", response.data);

  // Display the state of the expansion board LED

  command.command = SPIAGENT_CMD_GET_GPIO;
  command.pin = LPC1114_LED;
  command.data = 0;

  SPI_transaction_zmq(serversock, &command, &response);
  printf("The expansion board LED is      %s\n\n", response.data ? "ON" : "OFF");

  // Performance test

  printf("Starting %d SPI agent loopback test transactions...\n", ITERATIONS);
  fflush(stdout);

  time_t start = time(NULL);

  for (i = 0; i < ITERATIONS; i++)
  {
    command.command = SPIAGENT_CMD_LOOPBACK;
    command.pin = i*17;
    command.data = i*19;

    memset(&response, 0, sizeof(response));

    SPI_transaction_zmq(serversock, &command, &response);

    if ((command.command != response.command) ||
        (command.pin != response.pin) ||
        (command.data != response.data))
    {
      fprintf(stderr, "ERROR: command and response don't match\n");
      break;
    }

    if (response.error)
    {
      fprintf(stderr, "ERROR: response.error is %d\n", response.error);
      break;
    }
  }

  time_t finish = time(NULL);

  double iterations = 1.0*i;
  double deltat = finish - start;
  double rate = iterations / deltat;
  double cycletime = deltat / iterations;

  printf("\nPerformed %d loopback tests in %ld seconds\n", i, finish - start);
  printf("  %1.1f iterations per second\n", rate);
  printf("  %1.1f microseconds per iteration\n", cycletime*1.0E6);

  // Close connection to the server

  zmq_close(serversock);
  zmq_ctx_destroy(context);
  exit(0);
}
