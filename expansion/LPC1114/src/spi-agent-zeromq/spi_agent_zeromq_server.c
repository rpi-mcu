// Raspberry Pi LPC1114 I/O Processor Expansion Board
// SPI agent firmware ZeroMQ server

// Copyright (C)2015-2018, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <zmq.h>
#include <arpa/inet.h>

#include <libsimpleio/liblinux.h>

#include "spi-agent.h"

extern void spiagent_open_ioctl(char *servername, int32_t *error);
extern void spiagent_command_ioctl(SPIAGENT_COMMAND_MSG_t *cmd, SPIAGENT_RESPONSE_MSG_t *resp, int32_t *error);

#define PROGRAMNAME	"spi_agent_zeromq_server"
#define SERVICENAME	"spiagent-zeromq"

int main (void)
{
  struct servent *service;
  int32_t error;
  char service_def[256];

  openlog(PROGRAMNAME, LOG_NDELAY|LOG_PID|LOG_PERROR, LOG_DAEMON);

  syslog(LOG_INFO, "Starting LPC1114 SPI Agent Firmware ZeroMQ server");

// Look up service definition

  service = getservbyname(SERVICENAME, "tcp");
  if (service == NULL)
  {
    syslog(LOG_ERR, "ERROR: getservbyname() failed, unknown service");
    exit(1);
  }

// Open the Raspberry Pi LPC1114 I/O Processor Expansion Board SPI device

  spiagent_open_ioctl(NULL, &error);

  if (error)
  {
    syslog(LOG_ERR, "ERROR: spiagent_open_ioctl(() failed, %s", strerror(error));
    exit(1);
  }

// Become a nobody

  LINUX_drop_privileges("nobody", &error);

  if (error)
  {
    syslog(LOG_ERR, "ERROR: DropPrivileges() failed, %s", strerror(error));
    exit(1);
  }

// Switch to background

  if (daemon(0, 0))
  {
    syslog(LOG_ERR, "ERROR: daemon() failed, %s", strerror(errno));
    exit(1);
  }

//  Socket to talk to clients

  memset(service_def, 0, sizeof(service_def));
  snprintf(service_def, sizeof(service_def), "tcp://*:%d", ntohs(service->s_port));

  void *context = zmq_ctx_new();
  void *responder = zmq_socket(context, ZMQ_REP);
  if (zmq_bind(responder, service_def))
  {
    syslog(LOG_ERR, "ERROR: zmq_bind() failed, %s", strerror(errno));
    exit(1);
  }

// Message loop

  for (;;)
  {
    SPIAGENT_COMMAND_MSG_t command;
    SPIAGENT_RESPONSE_MSG_t response;
    uint32_t cmdbuf[3];
    uint32_t respbuf[4];

    if (zmq_recv(responder, cmdbuf, sizeof(cmdbuf), 0) < 0)
    {
      syslog(LOG_ERR, "ERROR: zmq_recv() failed, %s", strerror(errno));
    }

    command.command = ntohl(cmdbuf[0]);
    command.pin = ntohl(cmdbuf[1]);
    command.data = ntohl(cmdbuf[2]);

    spiagent_command_ioctl(&command, &response, &error);

    respbuf[0] = htonl(response.command);
    respbuf[1] = htonl(response.pin);
    respbuf[2] = htonl(response.data);
    respbuf[3] = htonl(response.error);

    if (zmq_send(responder, respbuf, sizeof(respbuf), 0) < 0)
    {
      syslog(LOG_ERR, "ERROR: zmq_send() failed, %s", strerror(errno));
    }
  }
}
