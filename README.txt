                     Raspberry Pi Microcontroller Framework

   This is a framework for using the [1]Raspberry Pi running [2]Raspberry
   Pi OS or another standard Linux distribution as an embedded
   microcontroller.

   See also the [3]Linux Simple I/O Library, which supports all Raspberry
   Pi models.

   See also [4]MuntsOS Embedded Linux, which supports all Raspberry Pi
   models, and may be better for embedded applications.

Git Repository

   The source code is available at: [5]http://git.munts.com/rpi-mcu

   Use the following command to clone it:

   git clone http://git.munts.com/rpi-mcu.git
   _______________________________________________________________________

   Questions or comments to Philip Munts [6]phil@munts.net

References

   1. http://www.raspberrypi.org/
   2. https://www.raspberrypi.org/software
   3. http://git.munts.com/libsimpleio
   4. http://git.munts.com/muntsos
   5. http://git.munts.com/rpi-mcu
   6. mailto:phil@munts.net
